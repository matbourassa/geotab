﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeotabExtractLib.EntityFields
{
    public class ExceptionEventField
    {
        public String id { get; }
        public String device_id { get; }
        public String driver_id { get; }
        public String rule_id { get; }
        public DateTime? activeFrom { get; }
        public DateTime? activeTo { get; }
        public float? distance { get; }
        public TimeSpan? duration { get; }
        public long? version { get; }

        public ExceptionEventField(
            String id,
            String device_id,
            String driver_id,
            String rule_id,
            DateTime? activeFrom,
            DateTime? activeTo,
            float? distance,
            TimeSpan? duration,
            long? version)
        {
            this.id = id;
            this.device_id = device_id;
            this.driver_id = driver_id;
            this.rule_id = rule_id;
            this.activeFrom = activeFrom;
            this.activeTo = activeTo;
            this.distance = distance;
            this.duration = duration;
            this.version = version;
        }
    }
}
