﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeotabExtractLib.EntityFields
{
    public class StatusDataField
    {
        public String id { get; }
        public String device_id { get; }
        public String diagnostic_id { get; }
        public DateTime? date_time { get; }
        public double? data { get; }
        public long? version { get; }

        public StatusDataField(String id, String device_id, String diagnostic_id, DateTime? date_time, double? data, long? version) {
            this.id = id;
            this.device_id = device_id;
            this.diagnostic_id = diagnostic_id;
            this.date_time = date_time;
            this.data = data;
            this.version = version;
        }
    }
}
