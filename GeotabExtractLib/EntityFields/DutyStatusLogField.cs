﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeotabExtractLib.EntityFields
{
    public class DutyStatusLogField
    {
        public String id { get; }
        public String device_id { get; }
        public String driver_id { get; }
        public String parentid { get; }
        public String state { get; }
        public String status { get; }
        public DateTime? datetime { get; }
        public DateTime? editdatetime { get; }
        public String location { get; }
        public String origin { get; }
        public DateTime? verifydatetime { get; }
        public float? version { get; }

        public DutyStatusLogField(
            String id,
            String device_id,
            String driver_id,
            String parentid,
            String state,
            String status,
            DateTime? datetime,
            DateTime? editdatetime,
            String location,
            String origin,
            DateTime? verifydatetime,
            float? version
            )
        {
            this.id = id;
            this.device_id = device_id;
            this.driver_id = driver_id;
            this.parentid = parentid;
            this.state = state;
            this.status = status;
            this.datetime = datetime;
            this.editdatetime = editdatetime;
            this.location = location;
            this.origin = origin;
            this.verifydatetime = verifydatetime;
            this.version = version;
        }
    }
}
