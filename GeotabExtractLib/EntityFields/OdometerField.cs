﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeotabExtractLib.EntityFields
{
    public class OdometerField
    {
        public double? id { get; }
        public String statusdata_id { get; }
        public String device_id { get; }
        public String vin { get; set; }
        public DateTime? date_time { get; }
        public double? value { get; }

        public OdometerField(double? id, String statusdata_id, String device_id, String vin, DateTime? date_time, double? value)
        {
            this.id = id;
            this.statusdata_id = statusdata_id;
            this.device_id = device_id;
            this.vin = vin;
            this.date_time = date_time;
            this.value = value;
        }
    }
}
