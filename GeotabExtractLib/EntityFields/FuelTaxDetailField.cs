﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeotabExtractLib.EntityFields
{
    public class FuelTaxDetailField
    {
        public String id { get; }

        public String device_id { get; }
        public String driver_id { get; }
        public String authority { get; }

        public bool? isClusterOdometer { get; }
        public String jurisdiction { get; }
        public String tollRoad { get; }

        public double? enterGPSOdometer { get; }
        public double? enterOdometer { get; }
        public double? enterLatitude { get; }
        public double? enterLongitude { get; }
        public DateTime? enterTime { get; }

        public double? exitGPSOdometer { get; }
        public double? exitOdometer { get; }
        public double? exitLatitude { get; }
        public double? exitLongitude { get; }
        public DateTime? exitTime { get; }

        /*
        array hourlygpsodometer
        array hourlylatitude
        array hourlylongitude
        array hourlyodometer
         */

        public FuelTaxDetailField(
            String id,
            String device_id,
            String driver_id,
            String authority,
            bool? isClusterOdometer,
            String jurisdiction,
            String tollRoad,
            double? enterGPSOdometer,
            double? enterOdometer,
            double? enterLatitude,
            double? enterLongitude,
            DateTime? enterTime,
            double? exitGPSOdometer,
            double? exitOdometer,
            double? exitLatitude,
            double? exitLongitude,
            DateTime? exitTime)
        {
            this.id = id;
            this.device_id = device_id;
            this.driver_id = driver_id;
            this.authority = authority;
            this.isClusterOdometer = isClusterOdometer;
            this.jurisdiction = jurisdiction;
            this.tollRoad = tollRoad;
            this.enterGPSOdometer = enterGPSOdometer;
            this.enterOdometer = enterOdometer;
            this.enterLatitude = enterLatitude;
            this.enterLongitude = enterLongitude;
            this.enterTime = enterTime;
            this.exitGPSOdometer = exitGPSOdometer;
            this.exitOdometer = exitOdometer;
            this.exitLatitude = exitLatitude;
            this.exitLongitude = exitLongitude;
            this.exitTime = exitTime;
        }
    }
}
