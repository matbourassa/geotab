﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeotabExtractLib.EntityFields
{
    public class ControllerField
    {
        public String id { get; }
        public String source_id { get; }
        public byte? code { get; }
        public String name { get; }
        public long? version { get; }

        public ControllerField(
            String id,
            String source_id,
            byte? code,
            String name,
            long? version)
        {
            this.id = id;
            this.source_id = source_id;
            this.code = code;
            this.name = name;
            this.version = version;
        }
    }
}
