﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeotabExtractLib.EntityFields
{
    public class UserField
    {
        public String id { get; }
        public int? acceptedEULA { get; }
        public String[] activeDashboardReports { get; }
        public DateTime? activeFrom { get; }
        public DateTime? activeTo { get; }
        public String[] availableDashboardReports { get; }
        public String[][] cannedResponseOptions { get; }
        public bool? changePassword { get; }
        public String user_comment { get; }
        //public List<Group> companyGroups { get; }
        public String dateFormat { get; }
        public String defaultGoogleMapStyle { get; }
        public String defaultHereMapStyle { get; }
        public String defaultMapEngine { get; }
        public String defaultOpenStreetMapStyle { get; }
        public String defaultPage { get; }
        public String designation { get; }
        public String employeeNo { get; }
        public String firstDayOfWeek { get; }
        public String firstName { get; }
        public String fuelEconomyUnit { get; }
        public String hosRuleSet { get; }
        public bool? isEULAAccepted { get; }
        public bool? isLabsEnabled { get; }
        public bool? isMetric { get; }
        public bool? isNewsEnabled { get; }
        public bool? isPersonalConveyanceEnabled { get; }
        public bool? isYardMoveEnabled { get; }
        public String user_language { get; }
        public String lastName { get; }
        //public List<MapView> mapViews { get; }
        public bool? menuCollapsedNotified { get; }
        public String name { get; }
        public String password { get; }
        //public List<Group> privateUserGroups { get; }
        //public List<Group> reportGroups { get; }
        //public List<Group> securityGroups { get; }
        public bool? showClickOnceWarning { get; }
        public String timeZoneId { get; }
        public String zoneDisplayMode { get; }

        public UserField(
            String id, 
            int? acceptedEULA, 
            String[] activeDashboardReports, 
            DateTime? activeFrom, 
            DateTime? activeTo, 
            String[] availableDashboardReports, 
            String[][] cannedResponseOptions, 
            bool? changePassword, 
            String user_comment, 
            //List<Group> companyGroups, 
            String dateFormat, 
            String defaultGoogleMapStyle, 
            String defaultHereMapStyle, 
            String defaultMapEngine, 
            String defaultOpenStreetMapStyle, 
            String defaultPage, 
            String designation, 
            String employeeNo, 
            String firstDayOfWeek, 
            String firstName, 
            String fuelEconomyUnit, 
            String hosRuleSet, 
            bool? isEULAAccepted, 
            bool? isLabsEnabled, 
            bool? isMetric, 
            bool? isNewsEnabled, 
            bool? isPersonalConveyanceEnabled, 
            bool? isYardMoveEnabled, 
            String user_language, 
            String lastName, 
            //List<MapView> mapViews, 
            bool? menuCollapsedNotified, 
            String name, 
            String password, 
            //List<Group> privateUserGroups, 
            //List<Group> reportGroups, 
            //List<Group> securityGroups, 
            bool? showClickOnceWarning, 
            String timeZoneId, 
            String zoneDisplayMode)
        {
            this.id = id;
            this.acceptedEULA = acceptedEULA;
            this.activeDashboardReports = activeDashboardReports;
            this.activeFrom = activeFrom;
            this.activeTo = activeTo;
            this.availableDashboardReports = availableDashboardReports;
            this.cannedResponseOptions = cannedResponseOptions;
            this.changePassword = changePassword;
            this.user_comment = user_comment;
            //this.companyGroups = companyGroups;
            this.dateFormat = dateFormat;
            this.defaultGoogleMapStyle = defaultGoogleMapStyle;
            this.defaultHereMapStyle = defaultHereMapStyle;
            this.defaultMapEngine = defaultMapEngine;
            this.defaultOpenStreetMapStyle = defaultOpenStreetMapStyle;
            this.defaultPage = defaultPage;
            this.designation = designation;
            this.employeeNo = employeeNo;
            this.firstDayOfWeek = firstDayOfWeek;
            this.firstName = firstName;
            this.fuelEconomyUnit = fuelEconomyUnit;
            this.hosRuleSet = hosRuleSet;
            this.isEULAAccepted = isEULAAccepted;
            this.isLabsEnabled = isLabsEnabled;
            this.isMetric = isMetric;
            this.isNewsEnabled = isNewsEnabled;
            this.isPersonalConveyanceEnabled = isPersonalConveyanceEnabled;
            this.isYardMoveEnabled = isYardMoveEnabled;
            this.user_language = user_language;
            this.lastName = lastName;
            //this.mapViews = mapViews;
            this.menuCollapsedNotified = menuCollapsedNotified;
            this.name = name;
            this.password = password;
            //this.privateUserGroups = privateUserGroups;
            //this.reportGroups = reportGroups;
            //this.securityGroups = securityGroups;
            this.showClickOnceWarning = showClickOnceWarning;
            this.timeZoneId = timeZoneId;
            this.zoneDisplayMode = zoneDisplayMode;
        }

    }
}
