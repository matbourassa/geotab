﻿using System;

namespace GeotabExtractLib.EntityFields
{
    public class Go7Field
    {
        public String id { get; }
        public String name { get; }
        public int? productId { get; }
        public String serialnumber { get; }
        public int? hardwareId { get; }
        public String worktime { get; }
        public DateTime? activeFrom { get; }
        public DateTime? activeTo { get; }
        public String vehiculeIdentificationNumber { get; }
        public String comment { get; }

        public Go7Field(
            String id,
            String name,
            int? productId,
            String serialnumber,
            int? hardwareId,
            String worktime,
            DateTime? activeFrom,
            DateTime? activeTo,
            String comment,
            String vehiculeIdentificationNumber)
        {
            this.id = id;
            this.name = name;
            this.productId = productId;
            this.serialnumber = serialnumber;
            this.hardwareId = hardwareId;
            this.worktime = worktime;
            this.activeFrom = activeFrom;
            this.activeTo = activeTo;
            this.comment = comment;
            this.vehiculeIdentificationNumber = vehiculeIdentificationNumber;
        }
    }
}
