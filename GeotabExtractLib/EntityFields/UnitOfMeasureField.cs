﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeotabExtractLib.EntityFields
{
    public class UnitOfMeasureField
    {
        public String id { get; }
        public String name { get; }

        public UnitOfMeasureField(String id, String name)
        {
            this.id = id;
            this.name = name;
        }
    }
}
