﻿using System;

namespace GeotabExtractLib.EntityFields
{
    public class FaultDataField
    {
        public String id { get; }
        public String device_id { get; }
        public String diagnostic_id { get; }
        public String controller_id { get; }
        public String failuremode_id { get; }
        public String flashcode_id { get; }
        public String dismissuser_id { get; }
        public DateTime? date_time { get; }
        public DateTime? dismissdatetime { get; }
        public int? fault_count { get; }
        public String faultstate { get; }
        public String faultlampstate { get; }
        public bool? amberwarninglamp { get; }
        public bool? malfunctionlamp { get; }
        public bool? protectwarninglamp { get; }
        public bool? redstoplamp { get; }

        public FaultDataField(
            String id,
            String device_id,
            String diagnostic_id,
            String controller_id,
            String failuremode_id,
            String flashcode_id,
            String dismissuser_id,
            DateTime? date_time,
            DateTime? dismissdatetime,
            int? fault_count,
            String faultstate,
            String faultlampstate,
            bool? amberwarninglamp,
            bool? malfunctionlamp,
            bool? protectwarninglamp,
            bool? redstoplamp)
        {
            this.id = id;
            this.device_id = device_id;
            this.diagnostic_id = diagnostic_id;
            this.controller_id = controller_id;
            this.failuremode_id = failuremode_id;
            this.flashcode_id = flashcode_id;
            this.dismissuser_id = dismissuser_id;
            this.date_time = date_time;
            this.dismissdatetime = dismissdatetime;
            this.fault_count = fault_count;
            this.faultstate = faultstate;
            this.faultlampstate = faultlampstate;
            this.amberwarninglamp = amberwarninglamp;
            this.malfunctionlamp = malfunctionlamp;
            this.protectwarninglamp = protectwarninglamp;
            this.redstoplamp = redstoplamp;
        }
    }
}
