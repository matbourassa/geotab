﻿using System;

namespace GeotabExtractLib.EntityFields
{     
    /// <summary>
    /// Store a LogRecord from geotab.
    /// </summary>
    public class LogRecordField
    {
        public String id { get; }
        public String device_id { get; }
        public DateTime? date_time { get; }
        public float? speed { get; }
        public double? latitude { get; }
        public double? longitude { get; }
        public bool? islastactive { get; }
        public long? version { get; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="device_id"></param>
        /// <param name="date_time"></param>
        /// <param name="speed"></param>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="islastactive"></param>
        /// <param name="version"></param>
        public LogRecordField(
                String id, 
                String device_id, 
                DateTime? date_time, 
                float? speed, 
                double? latitude,
                double? longitude, 
                bool? islastactive,
                long? version)
        {
            this.id = id;
            this.device_id = device_id;
            this.date_time = date_time;
            this.speed = speed;
            this.latitude = latitude;
            this.longitude = longitude;
            this.islastactive = islastactive;
            this.version = version;
        }
    }
}
