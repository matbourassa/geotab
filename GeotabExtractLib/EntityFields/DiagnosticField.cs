﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeotabExtractLib.EntityFields
{
    public class DiagnosticField
    {
        public String id { get; }
        public String controller_id { get; }
        public String enginetype_id { get; }
        public String source_id { get; }
        public String unitOfMeasure_id { get; }
        public int? code { get; }
        public String diagnosticType { get; }
        public String faultResetMode { get; }
        public String name { get; }
        public long? version { get; }

        public DiagnosticField(
            String id,
            String controller_id,
            String enginetype_id,
            String source_id,
            String unitOfMeasure_id,
            int? code,
            String diagnosticType,
            String faultResetMode,
            String name,
            long? version)
        {
            this.id = id;
            this.controller_id = controller_id;
            this.enginetype_id = enginetype_id;
            this.source_id = source_id;
            this.unitOfMeasure_id = unitOfMeasure_id;
            this.code = code;
            this.diagnosticType = this.diagnosticType;
            this.faultResetMode = faultResetMode;
            this.name = name;
            this.version = version;
        }
    }
}
