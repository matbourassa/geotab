﻿using System;

namespace GeotabExtractLib.EntityFields
{
    /// <summary>
    /// Store a Device from geotab.
    /// </summary>
    public class DeviceField
    {
        public String id { get; }
        public String name { get; }
        public int? productId { get; }
        public String serialnumber { get; }
        public int? hardwareId { get; }
        public String worktime { get; }
        public DateTime? activeFrom { get; }
        public DateTime? activeTo { get; }
        public long? version { get; }
        public String comment { get; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="productId"></param>
        /// <param name="serialnumber"></param>
        /// <param name="hardwareId"></param>
        /// <param name="worktime"></param>
        /// <param name="activeFrom"></param>
        /// <param name="activeTo"></param>
        /// <param name="version"></param>
        /// <param name="comment"></param>
        public DeviceField(
            String id, 
            String name, 
            int? productId, 
            String serialnumber,
            int? hardwareId, 
            String worktime, 
            DateTime? activeFrom, 
            DateTime? activeTo, 
            long? version, 
            String comment)
        {
            this.id = id;
            this.name = name;
            this.productId = productId;
            this.serialnumber = serialnumber;
            this.hardwareId = hardwareId;
            this.worktime = worktime;
            this.activeFrom = activeFrom;
            this.activeTo = activeTo;
            this.version = version;
            this.comment = comment;
        }
    }
}
