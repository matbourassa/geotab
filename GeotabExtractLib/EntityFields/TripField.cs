﻿using System;

namespace GeotabExtractLib.EntityFields
{
    public class TripField
    {
        public String id { get; }
        public String device_id { get; }
        public String driver_id { get; }
        public DateTime? datetime { get; }
        public float? afterhoursdistance { get; }
        public TimeSpan? afterhoursdrivingduration { get; }
        public bool? afterhoursend { get; }
        public bool? afterhoursstart { get; }
        public TimeSpan? afterhoursstopduration { get; }
        public float? averagespeed { get; }
        public TimeSpan? idlingduration { get; }
        public float? maximumspeed { get; }
        public DateTime? nexttripstart { get; }
        public int? speedrange1 { get; }
        public TimeSpan? speedrange1duration { get; }
        public int? speedrange2 { get; }
        public TimeSpan? speedrange2duration { get; }
        public int? speedrange3 { get; }
        public TimeSpan? speedrange3duration { get; }
        public DateTime? start { get; }
        public DateTime? stop { get; }
        public TimeSpan? stopduration { get; }
        public String stoppoint { get; }
        public float? workdistance { get; }
        public TimeSpan? workdrivingduration { get; }
        public TimeSpan? workstopduration { get; }
        public long? version { get; }

        public TripField(
            String id,
            String device_id,
            String driver_id,
            DateTime? datetime,
            float? afterhoursdistance,
            TimeSpan? afterhoursdrivingduration,
            bool? afterhoursend,
            bool? afterhoursstart,
            TimeSpan? afterhoursstopduration,
            float? averagespeed,
            TimeSpan? idlingduration,
            float? maximumspeed,
            DateTime? nexttripstart,
            int? speedrange1,
            TimeSpan? speedrange1duration,
            int? speedrange2,
            TimeSpan? speedrange2duration,
            int? speedrange3,
            TimeSpan? speedrange3duration,
            DateTime? start,
            DateTime? stop,
            TimeSpan? stopduration,
            String stoppoint,
            float? workdistance,
            TimeSpan? workdrivingduration,
            TimeSpan? workstopduration,
            long? version)
        {
            this.id = id;
            this.device_id = device_id;
            this.driver_id = driver_id;
            this.datetime = datetime;
            this.afterhoursdistance = afterhoursdistance;
            this.afterhoursdrivingduration = afterhoursdrivingduration;
            this.afterhoursend = afterhoursend;
            this.afterhoursstart = afterhoursstart;
            this.afterhoursstopduration = afterhoursstopduration;
            this.averagespeed = averagespeed;
            this.idlingduration = idlingduration;
            this.maximumspeed = maximumspeed;
            this.nexttripstart = nexttripstart;
            this.speedrange1 = speedrange1;
            this.speedrange1duration = speedrange1duration;
            this.speedrange2 = speedrange2;
            this.speedrange2duration = speedrange2duration;
            this.speedrange3 = speedrange3;
            this.speedrange3duration = speedrange3duration;
            this.start = start;
            this.stop = stop;
            this.stopduration = stopduration;
            this.stoppoint = stoppoint;
            this.workdistance = workdistance;
            this.workdrivingduration = workdrivingduration;
            this.workstopduration = workstopduration;
            this.version = version;
        }
    }
}
