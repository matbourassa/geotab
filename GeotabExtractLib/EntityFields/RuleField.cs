﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeotabExtractLib.EntityFields
{
    public class RuleField
    {
        public String id { get; }
        public String condition_id { get; }
        public DateTime? activeFrom { get; }
        public DateTime? activeTo { get; }
        public String baseType { get; }
        public String color { get; }
        public String comment { get; }
        //public String groups { get; }
        public String name { get; }
        public long? version { get; }
        public bool? bInactif { get; }

        public RuleField(
            String id,
            String condition_id,
            DateTime? activeFrom,
            DateTime? activeTo,
            String baseType,
            String color,
            String comment,
            //String groups,
            String name,
            long? version
            )
        {
            this.id = id;
            this.condition_id = condition_id;
            this.activeFrom = activeFrom;
            this.activeTo = activeTo;
            this.baseType = baseType;
            this.color = color;
            this.comment = comment;
            //this.groups = groups;
            this.name = name;
            this.version = version;
            this.bInactif = true;
        }

        public RuleField(
            String id,
            String condition_id,
            DateTime? activeFrom,
            DateTime? activeTo,
            String baseType,
            String color,
            String comment,
            //String groups,
            String name,
            long? version,
            bool? bInactif)
        {
            this.id = id;
            this.condition_id = condition_id;
            this.activeFrom = activeFrom;
            this.activeTo = activeTo;
            this.baseType = baseType;
            this.color = color;
            this.comment = comment;
            //this.groups = groups;
            this.name = name;
            this.version = version;
            this.bInactif = bInactif;
        }
    }
}
