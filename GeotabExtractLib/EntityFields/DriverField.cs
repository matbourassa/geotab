﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeotabExtractLib.EntityFields
{
    public class DriverField
    {
        public String id { get; }
        public String firstname { get; }
        public String lastname { get; }
        public String employeeno { get; }

        public DriverField(
            String id,
            String firstname,
            String lastname,
            String employeeno)
        {
            this.id = id;
            this.firstname = firstname;
            this.lastname = lastname;
            this.employeeno = employeeno;
        }
    }
}
