﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeotabExtractLib.EntityFields
{
    public class AlertExtractField
    {
        public int? id { get; }
        public string device_id { get; }
        public string vin { get; set; }
        public string exceptionevent_id { get; }
        public string rule_id { get; }
        public string rule_name { get; set; }
        public float? distance { get; }
        public DateTime? start_date_time { get; }
        public DateTime? end_date_time { get; }
        public string start_logrecord_id { get; set; }
        public double? start_timediff { get; set; }
        public double? start_latitude { get; set; }
        public double? start_longitude { get; set; }
        public string end_logrecord_id { get; set; }
        public double? end_timediff { get; set; }
        public double? end_latitude { get; set; }
        public double? end_longitude { get; set; }

        public AlertExtractField(
            int? id,
            string device_id,
            string vin,
            string exceptionevent_id,
            string rule_id,
            string rule_name,
            float? distance,
            DateTime? start_date_time,
            DateTime? end_date_time,
            string start_logrecord_id,
            double? start_timediff,
            double? start_latitude,
            double? start_longitude,
            string end_logrecord_id,
            double? end_timediff,
            double? end_latitude,
            double? end_longitude
            )
        {
            this.id = id;
            this.device_id = device_id;
            this.vin = vin;
            this.exceptionevent_id = exceptionevent_id;
            this.rule_id = rule_id;
            this.rule_name = rule_name;
            this.distance = distance;
            this.start_date_time = start_date_time;
            this.end_date_time = end_date_time;
            this.start_logrecord_id = start_logrecord_id;
            this.start_timediff = start_timediff;
            this.start_latitude = start_latitude;
            this.start_longitude = start_longitude;
            this.end_logrecord_id = end_logrecord_id;
            this.end_timediff = end_timediff;
            this.end_latitude = end_latitude;
            this.end_longitude = end_longitude;
        }

        public void setGPSStart(LogRecordField logrecord)
        {
            if (logrecord == null)
            {
                throw new Exception("Unable to set a GPS starting value from a null logrecord.");
            }
            this.start_logrecord_id = logrecord.id;
            if (this.start_date_time != null && logrecord.date_time != null)
            {
                this.start_timediff = ((DateTime)this.start_date_time - (DateTime)logrecord.date_time).TotalSeconds;
            }
            this.start_latitude = logrecord.latitude;
            this.start_longitude = logrecord.longitude;
        }

        public void setGPSEnd(LogRecordField logrecord)
        {
            if (logrecord == null)
            {
                throw new Exception("Unable to set a GPS ending value from a null logrecord.");
            }
            this.end_logrecord_id = logrecord.id;
            if (this.end_date_time != null && logrecord.date_time != null)
            {
                this.end_timediff = ((DateTime)this.end_date_time - (DateTime)logrecord.date_time).TotalSeconds;
            }
            this.end_latitude = logrecord.latitude;
            this.end_longitude = logrecord.longitude;
        }
    }
}
