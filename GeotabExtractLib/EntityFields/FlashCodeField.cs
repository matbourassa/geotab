﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeotabExtractLib.EntityFields
{
    public class FlashCodeField
    {
        public String id { get; }
        public String diagnostic_id { get; }
        public String failuremode_id { get; }
        public String name { get; }
        public String flashcodeindex { get; }
        public String helpurl { get; }
        public String pagereference { get; }
        public int? prioritylevel { get; }

        public FlashCodeField(
            String id,
            String diagnostic_id,
            String failuremode_id,
            String name,
            String flashcodeindex,
            String helpurl,
            String pagereference,
            int? prioritylevel)
        {
            this.id = id;
            this.diagnostic_id = diagnostic_id;
            this.failuremode_id = failuremode_id;
            this.name = name;
            this.flashcodeindex = flashcodeindex;
            this.helpurl = helpurl;
            this.pagereference = pagereference;
            this.prioritylevel = prioritylevel;
        }
    }
}
