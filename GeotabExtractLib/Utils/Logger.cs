﻿using System;

using System.Configuration;
using System.IO;

namespace GeotabExtractLib.Utils
{
    /// <summary>
    /// This class manage creation of log entries.
    /// </summary>
    public class Logger
    {
        private static Logger instance;

        private int loglevel { get; }
        private string logger_basepath;

        /// <summary>
        /// Contructor. Use getInstance() to get the singleton instance.
        /// </summary>
        /// <param name="loglevel">LogLevel required for logging</param>
        private Logger(int loglevel)
        {
            this.loglevel = loglevel;
            this.logger_basepath = ConfigurationManager.AppSettings.Get("logger_basepath");

            string loglevel_desc;
            if (loglevel == LogLevel.Critical)
            {
                loglevel_desc = "Critical";
            }
            else if (loglevel == LogLevel.Warning)
            {
                loglevel_desc = "Warning";
            }
            else if (loglevel == LogLevel.Information)
            {
                loglevel_desc = "Information";
            }
            else if (loglevel == LogLevel.Debug)
            {
                loglevel_desc = "Debug";
            }
            else if (loglevel == LogLevel.All)
            {
                loglevel_desc = "All";
            }
            else {
                loglevel_desc = loglevel.ToString();
            }

            this.write(LogLevel.Information,String.Join("", new String[] { "Logger started with debug level \"", loglevel_desc, "\"." } ));
        }

        /// <summary>
        /// Returns the instance of the singleton class.
        /// </summary>
        /// <returns></returns>
        public static Logger getInstance()
        {
            if (instance == null)
            {
                instance = new Logger(Int32.Parse(ConfigurationManager.AppSettings.Get("logger_loglevel")));
                
            }
            return instance;
        }

        /// <summary>
        /// LogLevel nested class.
        /// </summary>
        public class LogLevel {
            public static int Critical { get; } = 1;
            public static int Warning { get; } = 2;
            public static int Information { get; } = 3;
            public static int Debug { get; } = 4;
            public static int All { get; } = 9;
        }

        /// <summary>
        /// Write a log entry if loglevel <= logger_loglevel.
        /// LogLevel info and date are added to the message.
        /// </summary>
        /// <param name="loglevel">LogLevel.</param>
        /// <param name="message">Message to write into log.</param>
        public void write(int loglevel, String message) {
            if (loglevel <= this.loglevel)
            {
                //Add loglevel information
                if (loglevel == LogLevel.Information)
                {
                    message = String.Join("", new String[] { "Information - ", message });
                }
                else if (loglevel == LogLevel.Warning)
                {
                    message = String.Join("", new String[] { "Warning - ", message });
                }
                else if (loglevel == LogLevel.Critical)
                {
                    message = String.Join("", new String[] { "Critical - ", message });
                }
                else if (loglevel == LogLevel.Debug)
                {
                    message = String.Join("", new String[] { "Debug - ", message });
                }
                else
                {
                    message = String.Join("", new String[] { "Debug", loglevel.ToString(), " - ", message });
                }

                //Add time information
                message = String.Join("", new String[] { DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), " - ", message });

                //Display message in console.
                Console.WriteLine(message);
                //Console.WriteLine(String.Join("", new String[] { logger_basepath, "GeotabExtractWorker_", DateTime.Now.ToString("yyyy-MM-dd"), ".log" }));
                //Console.ReadKey();
                //Write to a logfile
                writeFile(message, String.Join("", new String[] { logger_basepath, "GeotabExtractWorker_", DateTime.Now.ToString("yyyy-MM-dd"), ".log" }));
            }
        }

        /// <summary>
        /// Manage saving the message into a file.
        /// </summary>
        /// <param name="message">Message to write into a file.</param>
        /// <param name="filepath">path and name of the log file.</param>
        public static void writeFile(string message, String filepath)
        {
            try
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(message);
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.StackTrace);
            }
        }
        
    }
}
