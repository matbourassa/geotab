# Geotab Extractor and Notifier

### Project Structure

- GeotabExtractController
-- Windows Service that synchronizes data from Geotab to a Database
- GeotabExtractLib
-- Contains Models and ViewModels used by GeotabExtractController
- GeotabExtractSetupWizard
-- Project that generates a setup wizard (installshield) `.exe` to install the service. 
- Geotab_Notify_API
-- Web API Project that receives post notifications from the Geotab service and writes to a database.

### Service Information
- Run interval: 10 seconds

### Webservice documentation
- `{hostname}/swagger`

### Dependencies
https://marketplace.visualstudio.com/items?itemName=VisualStudioProductTeam.MicrosoftVisualStudio2015InstallerProjects
for setup wizard project.


### How to build Service
- Switch configuration to Release
- Rebuild GeotabExtractSetupWizard
- Get `exe` package from `GeotabExtractor\GeotabExtractSetupWizard\Release`, copy the `msi` as well.


### Architecture Information
- Geotab_Notify_API
-- Repositories are isntanciated via DI using `SimpleInjector`
-- `AlertRepository` is setup using WinDev HFSQL Client/Server ODBC, the repository, being injected, can be replaced as long as it follows the `IAlertRepository` interface.