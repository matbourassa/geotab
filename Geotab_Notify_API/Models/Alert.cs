﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Geotab_Notify_API.Models
{
    public class Alert
    {
        public string DeviceId { get; set; }

        public DateTime Timestamp { get; set; }

        public string Name { get; set; }

        public decimal Longitude { get; set; }
        
        public decimal Latitude { get; set; }
    }
}