﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Geotab_Notify_API.ViewModels
{
    public class AlertViewModel
    {
       

        [Required]
        [JsonProperty("databaseName")]
        public string DatabaseName { get; set; }

       
        [Required]
        [JsonProperty("deviceId")]
        [MaxLength(12)]
        public string DeviceId { get; set; }

        [JsonProperty("timestamp")]
        public DateTime Timestamp { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("longitude")]
        public decimal Longitude { get; set; }

        [JsonProperty("latitude")]
        public decimal Latitude { get; set; }


    }
}