﻿using Geotab_Notify_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Geotab_Notify_API.Repositories.Interfaces
{
    public interface IAlertRepository
    {
        long Insert(Alert alert);
        bool Update(Alert alert);
        Alert Get(long id);
        List<Alert> List();
        bool Delete(int id);

        //Used for variable database inserts
        void SetConnectionStringSchemaName(string schemaName);
    }
}