﻿using Geotab_Notify_API.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using Geotab_Notify_API.Models;
using System.Data.Odbc;

namespace Geotab_Notify_API.Repositories
{
    public class AlertRepository : IAlertRepository
    {
        private readonly OdbcConnection _connection;



        public AlertRepository(string connectionString)
        {
            _connection = new OdbcConnection(connectionString);
        }

        public void SetConnectionStringSchemaName(string schemaName)
        {
            _connection.ConnectionString = _connection.ConnectionString.Replace("@database", schemaName);
        }

        public long Insert(Alert alert)
        {
            long id = 0;


            OdbcCommand command = new OdbcCommand();
            command.Connection = _connection;
            command.CommandText = "insert into T_Alerte_Directe(device_id,date_time,alert_name,latitude,longitude) values(?,?,?,?,?)";

            //If decimal paramters aren't converted to string, value inserted will be 0
            command.Parameters.Add(new OdbcParameter("@deviceid", alert.DeviceId));
            command.Parameters.Add(new OdbcParameter("@date_time", DateTime.Now.ToString("yyyyMMddHHmmss")));
            command.Parameters.Add(new OdbcParameter("@alertName", alert.Name));
            command.Parameters.Add(new OdbcParameter("@latitude", alert.Latitude.ToString()));
            command.Parameters.Add(new OdbcParameter("@longtiude", alert.Longitude.ToString()));

            _connection.Open();

            try
            {
                //Row count always 0 on return so can't rely on it, possible WINDEV driver issue
                command.ExecuteNonQuery();
                id = 0;     
            }
            finally
            {

                _connection.Close();
            }

            return id;
        }

        /// <summary>
        /// Implement but unused, not unique ID to fetch on
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Alert Get(long id)
        {
            Alert alert = new Models.Alert();
            OdbcCommand command = new OdbcCommand();
            command.Connection = _connection;
            command.CommandText = "select device_id,alert_name,latitude,longitude from Alerts where id=? LIMIT 1";

            command.Parameters.Add(new OdbcParameter("@id", id));

            _connection.Open();

            try
            {

                using (OdbcDataReader reader = command.ExecuteReader())
                {

                    while (reader.Read())
                    {
                        //alert.Id = reader.GetInt64(0);
                        alert.DeviceId = reader.GetString(1);
                        alert.Name = reader.GetString(2);
                        alert.Latitude = reader.GetDecimal(3);
                        alert.Longitude = reader.GetDecimal(4);                        
                    }
                }

            }
            finally
            {
                _connection.Close();
            }

            return alert;
        }

        public List<Alert> List()
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public bool Update(Alert alert)
        {
            throw new NotImplementedException();
        }
    }
}