﻿using Geotab_Notify_API.Models;
using Geotab_Notify_API.Repositories.Interfaces;
using Geotab_Notify_API.ViewModels;
using Mapster;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Diagnostics;

namespace Geotab_Notify_API.Controllers
{
    [RoutePrefix("alert")]
    public class AlertController : ApiController
    {
        private static IAlertRepository _alertRepo { get; set; }

        public AlertController(IAlertRepository alertRepository)
        {
            _alertRepo = alertRepository;
        }

        /// <summary>
        /// Create alert in database from geotab
        /// </summary>
        /// <param name="alertVM"></param>
        /// <returns></returns>
        [HttpGet]
        //Empty route needed or else routing will be ignored
        [Route("")]
        [SwaggerResponse(201,"Created")]
        [SwaggerResponse(400,"Invalid Data",typeof(ModelStateDictionary))]                
        public async Task<IHttpActionResult> Create(string databaseName,string deviceId,string name,decimal longitude,decimal latitude, DateTime? timestamp = null)
        {

            AlertViewModel alertVM = new AlertViewModel {
                DatabaseName = databaseName,
                DeviceId = deviceId,
                Name = name,
                Longitude = longitude,
                Latitude = latitude
            };

            if(timestamp != null)
            {
                alertVM.Timestamp = (DateTime)timestamp;
            }

            Validate<AlertViewModel>(alertVM);

            if (ModelState.IsValid)
            {
                //Dynamic database instantiation
                _alertRepo.SetConnectionStringSchemaName(alertVM.DatabaseName);

                long id = _alertRepo.Insert(alertVM.Adapt<Alert>());

                //No id to return, HFSQL ODBC doesn't return inserted count properly, can't verify insert
                return Created("", "");

            }

            return BadRequest(ModelState);

        }

    }
}
