﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GeotabExtractContoller.Connector.DB
{
    public class WrongDatabaseException : Exception
    {
        public WrongDatabaseException()
        {
        }
        public WrongDatabaseException(string message)
        : base(message)
        {
        }
    }
}
