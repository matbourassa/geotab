﻿using System;
using System.Collections.Generic;
using System.Text;

using MySql.Data.MySqlClient;

using GeotabExtractLib.EntityFields;
using GeotabExtractLib.Utils;

namespace GeotabExtractContoller.Connector.DB
{
    /// <summary>
    /// Manages connectivity to MySQL server.
    /// </summary>
    class MySQLConnector
    {
        private static MySQLConnector instance;
        private static MySqlConnection mysqlconn;

        /// <summary>
        /// Constructor. Use the getInstance() fonction to get the Singleton instance.
        /// </summary>
        /// <param name="mysql_server"></param>
        /// <param name="mysql_db"></param>
        /// <param name="mysql_username"></param>
        /// <param name="mysql_password"></param>
        private MySQLConnector(string mysql_server, string mysql_db, string mysql_username, string mysql_password, string geotab_db)
        {
            Logger log = Logger.getInstance();

            //Building and loading the connection string.
            string cs = string.Join("", new string[] { "server=", mysql_server, ";userid=", mysql_username, ";password=", mysql_password, ";database=", mysql_db });
            mysqlconn = new MySqlConnection(cs);

            try
            {
                //Test connectivity to the MySQL database and print the Server's MySQL version for the logs.
                log.write(Logger.LogLevel.Debug, string.Join("", new string[] { "New MySQL server instance. Server=\"", mysql_server, "\", DB=\"", mysql_db, "\", username=\"", mysql_username, "\"." }));
                mysqlconn.Open();
                log.write(Logger.LogLevel.Debug, string.Join("", new string[] { "MySQL Version is ", mysqlconn.ServerVersion, "." }));
            }
            catch (MySqlException mse)
            {
                log.write(Logger.LogLevel.Critical, String.Format("{0} - MySQL problem, error number is: \"{1}\". Message is: {2}", "MySQLConnector Constructor", mse.Number.ToString(), mse.StackTrace));
            }
            finally
            {
                mysqlconn.Close();
            }

            //Make sure we're connected to the right DB, compare the configured geotab_db to the one in the configuration table.
            log.write(Logger.LogLevel.Information, string.Join("", new string[] { "Checking if we're using the right DB." }));
            string curgeotab_db = GetParameterValue("geotab_db");
            if (!curgeotab_db.Equals(geotab_db))
            {
                throw new WrongDatabaseException(string.Join("", new string[] { "ERROR! It seems like we're not updating the right database. Geotab database is \"", geotab_db, "\". Database has data for \"", curgeotab_db, "\"." }));
            }
            else
            {

                log.write(Logger.LogLevel.Information, string.Join("", new string[] { "We're using the right DB: (\"", geotab_db, "\"." }));
            }

        }

        /// <summary>
        /// Get the singleton instance of the MySQLConnector class.
        /// </summary>
        /// <param name="mysql_server"></param>
        /// <param name="mysql_db"></param>
        /// <param name="mysql_username"></param>
        /// <param name="mysql_password"></param>
        /// <returns></returns>
        public static MySQLConnector GetInstance(string mysql_server, string mysql_db, string mysql_username, string mysql_password, string geotab_db)
        {
            if (instance == null)
            {
                instance = new MySQLConnector(mysql_server, mysql_db, mysql_username, mysql_password, geotab_db);
            }
            return instance;
        }

        /// <summary>
        /// Delete AlertExtract older than the provided Datetime.
        /// </summary>
        /// <param name="date_time">Oldest AlertExtract allowed.</param>
        /// <returns>Returns true if no SQL Exception is returned.</returns>
        public bool DeleteAlertExtractOlderThan(DateTime date_time)
        {
            Logger log = Logger.getInstance();
            bool isSqlException = false;

            try
            {
                mysqlconn.Open();

                log.write(Logger.LogLevel.Information, string.Join("", new string[] { "Deleting AlertExtract older than \"", date_time.ToString(), "\"" }));
                MySqlCommand feedVersionUpdateCmd = new MySqlCommand();
                feedVersionUpdateCmd.Connection = mysqlconn;
                feedVersionUpdateCmd.CommandText = "DELETE FROM ge_alertextract WHERE end_date_time<@date_time";
                feedVersionUpdateCmd.Prepare();
                feedVersionUpdateCmd.Parameters.AddWithValue("@date_time", date_time);
                feedVersionUpdateCmd.ExecuteNonQuery();
                log.write(Logger.LogLevel.Information, string.Join("", new string[] { "AlertExtract cleaned." }));
            }
            catch (MySqlException mse)
            {
                log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
                isSqlException = true;
            }
            finally
            {
                mysqlconn.Close();
            }

            return !isSqlException;
        }

        /// <summary>
        /// Delete logrecords older than the provided Datetime.
        /// </summary>
        /// <param name="date_time">Oldest logrecord allowed.</param>
        /// <returns>Returns true if no SQL Exception is returned.</returns>
        public bool DeleteLogRecordsOlderThan(DateTime date_time)
        {
            Logger log = Logger.getInstance();
            bool isSqlException = false;

            try
            {
                mysqlconn.Open();

                log.write(Logger.LogLevel.Information, string.Join("", new string[] { "Deleting LogRecord older than \"", date_time.ToString(), "\"" }));
                MySqlCommand feedVersionUpdateCmd = new MySqlCommand();
                feedVersionUpdateCmd.Connection = mysqlconn;
                feedVersionUpdateCmd.CommandText = "DELETE FROM gt_logrecord WHERE date_time<@date_time";
                feedVersionUpdateCmd.Prepare();
                feedVersionUpdateCmd.Parameters.AddWithValue("@date_time", date_time);
                feedVersionUpdateCmd.ExecuteNonQuery();
                log.write(Logger.LogLevel.Information, string.Join("", new string[] { "LogRecord cleaned." }));
            }
            catch (MySqlException mse)
            {
                log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
                isSqlException = true;
            }
            finally
            {
                mysqlconn.Close();
            }

            return !isSqlException;
        }

        /// <summary>
        /// Delete Odometer values older than the provided Datetime.
        /// </summary>
        /// <param name="date_time">Oldest Odometer values allowed.</param>
        /// <returns>Returns true if no SQL Exception is returned.</returns>
        public bool DeleteOdometersOlderThan(DateTime date_time)
        {
            Logger log = Logger.getInstance();
            bool isSqlException = false;

            try
            {
                mysqlconn.Open();

                log.write(Logger.LogLevel.Information, string.Join("", new string[] { "Deleting Odometer values older than \"", date_time.ToString(), "\"" }));
                MySqlCommand feedVersionUpdateCmd = new MySqlCommand();
                feedVersionUpdateCmd.Connection = mysqlconn;
                feedVersionUpdateCmd.CommandText = "DELETE FROM ge_odometer WHERE date_time<@date_time";
                feedVersionUpdateCmd.Prepare();
                feedVersionUpdateCmd.Parameters.AddWithValue("@date_time", date_time);
                feedVersionUpdateCmd.ExecuteNonQuery();
                log.write(Logger.LogLevel.Information, string.Join("", new string[] { "Odometer values cleaned." }));
            }
            catch (MySqlException mse)
            {
                log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
                isSqlException = true;
            }
            finally
            {
                mysqlconn.Close();
            }

            return !isSqlException;
        }

        /// <summary>
        /// Get a value from a setting from the configuration table.
        /// </summary>
        /// <returns>Returns the ExceptionEventVersion.</returns>
        public string GetParameterValue(string parameter)
        {
            Logger log = Logger.getInstance();
            string value = null;

            try
            {
                mysqlconn.Open();

                log.write(Logger.LogLevel.Debug, string.Join("", new string[] { "Fetching the \"", parameter, "\" from the configuration table." }));
                MySqlCommand getConfigurationCmd = new MySqlCommand();
                getConfigurationCmd.Connection = mysqlconn;
                getConfigurationCmd.CommandText = "SELECT value FROM ge_configuration WHERE setting=@setting";
                getConfigurationCmd.Prepare();
                getConfigurationCmd.Parameters.AddWithValue("@setting", parameter);
                value = getConfigurationCmd.ExecuteScalar().ToString();
                log.write(Logger.LogLevel.Debug, string.Join("", new string[] { "Value found for \"", parameter, "\" found is \"", value, "\"." }));
            }
            catch (MySqlException mse)
            {
                log.write(Logger.LogLevel.Critical, String.Format("{0} - {1} - MySQL problem, error number is: \"{2}\". Message is: {3}", "GetParameterValue", parameter, mse.Number.ToString(), mse.StackTrace));
            }
            finally
            {
                mysqlconn.Close();
            }
            return value;
        }

        /// <summary>
        /// Get the latest fetched FeedVersion from the DB. Used to resume the service where it stopped.
        /// </summary>
        /// <returns>Returns the feedVersion for the feedname.</returns>
        public long GetFeedVersion(string feedname)
        {
            Logger log = Logger.getInstance();
            long curFeedVersion = -1;
            try
            {
                mysqlconn.Open();

                log.write(Logger.LogLevel.Debug, String.Format("Getting latest \"{0}\" version from feedversion table..", feedname));
                MySqlCommand feedVersionSelectCmd = new MySqlCommand();
                feedVersionSelectCmd.Connection = mysqlconn;
                feedVersionSelectCmd.CommandText = "SELECT version FROM ge_feedversion WHERE feedname=@feedname";
                feedVersionSelectCmd.Prepare();
                feedVersionSelectCmd.Parameters.AddWithValue("@feedname", feedname);
                curFeedVersion = long.Parse(feedVersionSelectCmd.ExecuteScalar().ToString());
                log.write(Logger.LogLevel.Information, string.Join("", new string[] { "Latest \"", feedname, "\" version found is ", curFeedVersion.ToString(), "." }));
            }
            catch (MySqlException mse)
            {
                log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
            }
            finally
            {
                mysqlconn.Close();
            }
            return curFeedVersion;
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public String GetVIN(string device_id)
        {
            Logger log = Logger.getInstance();
            String vin = null;
            try
            {
                mysqlconn.Open();

                log.write(9, String.Format("Getting the Vehicle Identification number for device id \"{0}\".", device_id));
                MySqlCommand feedVersionSelectCmd = new MySqlCommand();
                feedVersionSelectCmd.Connection = mysqlconn;
                feedVersionSelectCmd.CommandText = "SELECT vehiculeidentificationnumber FROM gt_go7 WHERE id=@id";
                feedVersionSelectCmd.Prepare();
                feedVersionSelectCmd.Parameters.AddWithValue("@id", device_id);
                vin = feedVersionSelectCmd.ExecuteScalar().ToString();
                log.write(9, String.Format("VIN for device \"{0}\" is \"{1}\"", device_id, vin));
            }
            catch (MySqlException mse)
            {
                log.write(Logger.LogLevel.Critical, string.Join("", String.Format("MySQL problem, error number is:{0}. Message is:{1}", mse.Number.ToString(), mse.StackTrace)));
            }
            catch (Exception e)
            {
                log.write(Logger.LogLevel.Critical, string.Join("", String.Format("Exception found. Message is:{0}", e.StackTrace)));
            }
            finally
            {
                mysqlconn.Close();
            }
            return vin;
        }

        /// <summary>
        /// Find the closest LogRecordField by date_time for a given device_id.
        /// </summary>
        /// <param name="device_id">ID of the device.</param>
        /// <param name="date_time">Datetime we want a LogRecord for.</param>
        /// <returns>LogRecordField closest to the date_time provided for the provided device_id.</returns>
        public LogRecordField FindLogRecordFieldByDateTime(string device_id, DateTime date_time)
        {
            Logger log = Logger.getInstance();
            LogRecordField logRecord = null;
            bool foundMatchingLogRecord = false;

            try
            {
                mysqlconn.Open();

                log.write(9, string.Join("", new string[] { "Getting the LogRecord the closest to the specified DateTime ", date_time.ToString(), " for the following device_id=\"", device_id, "\"." }));
                MySqlCommand logrecordSelectCmd = new MySqlCommand();
                logrecordSelectCmd.Connection = mysqlconn;
                //Get a single LogRecord for a given device_id with an absolute differential value of its date_time value minus the provided date_time value.
                logrecordSelectCmd.CommandText =
@"SELECT 
    id,
    device_id,
    date_time,
    speed,
    latitude,
    longitude,
    islastactive,
    version
FROM gt_logrecord
WHERE device_id=@device_id
AND date_time > @date_time_min
AND date_time < @date_time_max
ORDER BY ABS(TIMESTAMPDIFF(second, date_time, @date_time))
limit 1;";
                logrecordSelectCmd.Prepare();
                logrecordSelectCmd.Parameters.AddWithValue("@device_id", device_id);
                logrecordSelectCmd.Parameters.AddWithValue("@date_time", date_time);
                logrecordSelectCmd.Parameters.AddWithValue("@date_time_min", formatDateMySQL(date_time.Subtract(TimeSpan.FromMinutes(1))));
                logrecordSelectCmd.Parameters.AddWithValue("@date_time_max", formatDateMySQL(date_time.Add(TimeSpan.FromMinutes(1))));
                MySqlDataReader logrecordResult = logrecordSelectCmd.ExecuteReader();

                if (logrecordResult.Read())
                {
                    LogRecordField tmpLogRecord = new LogRecordField(
                                                        logrecordResult.GetString("id"),
                                                        logrecordResult.GetString("device_id"),
                                                        logrecordResult.IsDBNull(2) ? null : new DateTime?(logrecordResult.GetDateTime("date_time")),
                                                        logrecordResult.IsDBNull(3) ? null : new float?(logrecordResult.GetFloat("speed")),
                                                        logrecordResult.IsDBNull(4) ? null : new double?(logrecordResult.GetDouble("latitude")),
                                                        logrecordResult.IsDBNull(5) ? null : new double?(logrecordResult.GetDouble("longitude")),
                                                        logrecordResult.IsDBNull(6) ? null : new bool?(logrecordResult.GetBoolean("islastactive")),
                                                        logrecordResult.IsDBNull(7) ? null : new long?(logrecordResult.GetInt64("version"))
                                                        );

                    if (tmpLogRecord.id != null && !tmpLogRecord.id.Equals("") &&
                        tmpLogRecord.device_id != null && !tmpLogRecord.device_id.Equals(""))
                    {
                        logRecord = tmpLogRecord;
                        foundMatchingLogRecord = true;
                        log.write(9, string.Join("", new string[] { "Found closest LogRecord with ID= \"", logRecord.id, "\" and device_id=\"", device_id, "\". Date is ", date_time.ToString(), "." }));
                    }
                    else
                    {
                        log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "Invalid LogRecord returned. Id=\"", logRecord.id, "\", Device_id=\"", logRecord.device_id, "\"" }));
                    }
                }
            }
            catch (MySqlException mse)
            {
                log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
            }
            finally
            {
                mysqlconn.Close();
            }
            if (!foundMatchingLogRecord)
            {
                log.write(Logger.LogLevel.Debug, string.Join("", new string[] { "No LogRecord found for device_id=\"", device_id, "\" and date_time=", date_time.ToString(), " within 1 minutes. Retrying without time limit." }));
                return FindLogRecordFieldByDateTime_notimelimit(device_id, date_time);
            }
            return logRecord;
        }

        /// <summary>
        /// Find the closest LogRecordField by date_time for a given device_id.
        /// </summary>
        /// <param name="device_id">ID of the device.</param>
        /// <param name="date_time">Datetime we want a LogRecord for.</param>
        /// <returns>LogRecordField closest to the date_time provided for the provided device_id.</returns>
        private LogRecordField FindLogRecordFieldByDateTime_notimelimit(string device_id, DateTime date_time)
        {
            Logger log = Logger.getInstance();
            LogRecordField logRecord = null;

            try
            {
                mysqlconn.Open();

                log.write(9, string.Join("", new string[] { "Getting the LogRecord the closest to the specified DateTime ", date_time.ToString(), " for the following device_id=\"", device_id, "\"." }));
                MySqlCommand logrecordSelectCmd = new MySqlCommand();
                logrecordSelectCmd.Connection = mysqlconn;
                //Get a single LogRecord for a given device_id with an absolute differential value of its date_time value minus the provided date_time value.
                logrecordSelectCmd.CommandText =
@"SELECT 
    id,
    device_id,
    date_time,
    speed,
    latitude,
    longitude,
    islastactive,
    version
FROM gt_logrecord
WHERE device_id=@device_id
ORDER BY ABS(TIMESTAMPDIFF(second, date_time, @date_time))
limit 1;";
                logrecordSelectCmd.Prepare();
                logrecordSelectCmd.Parameters.AddWithValue("@device_id", device_id);
                logrecordSelectCmd.Parameters.AddWithValue("@date_time", date_time);
                MySqlDataReader logrecordResult = logrecordSelectCmd.ExecuteReader();

                if (logrecordResult.Read())
                {
                    LogRecordField tmpLogRecord = new LogRecordField(
                                                    logrecordResult.GetString("id"),
                                                    logrecordResult.GetString("device_id"),
                                                    logrecordResult.IsDBNull(2) ? null : new DateTime?(logrecordResult.GetDateTime("date_time")),
                                                    logrecordResult.IsDBNull(3) ? null : new float?(logrecordResult.GetFloat("speed")),
                                                    logrecordResult.IsDBNull(4) ? null : new double?(logrecordResult.GetDouble("latitude")),
                                                    logrecordResult.IsDBNull(5) ? null : new double?(logrecordResult.GetDouble("longitude")),
                                                    logrecordResult.IsDBNull(6) ? null : new bool?(logrecordResult.GetBoolean("islastactive")),
                                                    logrecordResult.IsDBNull(7) ? null : new long?(logrecordResult.GetInt64("version"))
                                                    );

                    if (tmpLogRecord.id != null && !tmpLogRecord.id.Equals("") &&
                        tmpLogRecord.device_id != null && !tmpLogRecord.device_id.Equals(""))
                    {
                        logRecord = tmpLogRecord;
                        log.write(9, string.Join("", new string[] { "Found closest LogRecord with ID= \"", logRecord.id, "\" and device_id=\"", device_id, "\". Date is ", date_time.ToString(), "." }));
                    }
                    else
                    {
                        log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "Invalid LogRecord returned. Id=\"", logRecord.id, "\", Device_id=\"", logRecord.device_id, "\"" }));
                    }
                }
                else
                {
                    log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "No LogRecord found for device_id=\"", device_id, "\" and date_time=", date_time.ToString() }));
                    return null;
                }
            }
            catch (MySqlException mse)
            {
                log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
            }
            finally
            {
                mysqlconn.Close();
            }
            return logRecord;
        }

        /// <summary>
        /// Returns the Exception Rule name from the extracted Rule list.
        /// </summary>
        /// <param name="rule_id">Rule_id</param>
        /// <returns>Name of the Rule.</returns>
        public string GetRuleName(string rule_id)
        {
            Logger log = Logger.getInstance();
            string rule_name = null;
            try
            {
                mysqlconn.Open();

                log.write(9, string.Join("", new string[] { "Getting rule_name for id=\"", rule_id, "\"." }));
                MySqlCommand ruleSelectCmd = new MySqlCommand();
                ruleSelectCmd.Connection = mysqlconn;
                ruleSelectCmd.CommandText = "SELECT name FROM gt_rule WHERE id=@rule_id";
                ruleSelectCmd.Prepare();
                ruleSelectCmd.Parameters.AddWithValue("@rule_id", rule_id);
                Object tmpResult = ruleSelectCmd.ExecuteScalar();
                if (tmpResult != null)
                {
                    rule_name = tmpResult.ToString();
                }
                else
                {
                    rule_name = "NoRuleName";
                    log.write(Logger.LogLevel.Warning, string.Join("", new string[] { "rule_id = \"", rule_id, "\" isn't defined." }));
                }
                log.write(9, string.Join("", new string[] { "rule_name for id = \"", rule_id, "\" is ", rule_name, "." }));
            }
            catch (MySqlException mse)
            {
                log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
            }
            finally
            {
                mysqlconn.Close();
            }
            return rule_name;
        }

        /// <summary>
        /// AlertExtract are updated in two phases, this function is to get the list of AlertExtract that need updating for the second phase.
        /// </summary>
        /// <returns>List of non-updated AlertExtract.</returns>
        public List<AlertExtractField> GetUncompleteAlertExtract()
        {
            Logger log = Logger.getInstance();

            List<AlertExtractField> lstAlertExtract = new List<AlertExtractField>();

            try
            {
                mysqlconn.Open();

                log.write(Logger.LogLevel.Debug, "Getting the list of AlertExtract that doesn't have all their parameters set.");
                MySqlCommand alertExtractSelectCmd = new MySqlCommand();
                alertExtractSelectCmd.Connection = mysqlconn;
                alertExtractSelectCmd.CommandText =
@"SELECT
    id,
    device_id,
    vin,
    exceptionevent_id,
    rule_id,
    rule_name,
    distance,
    start_date_time,
    end_date_time,
    start_logrecord_id,
    start_timediff,
    start_latitude,
    start_longitude,
    end_logrecord_id,
    end_timediff,
    end_latitude,
    end_longitude
FROM ge_alertextract
WHERE 
    rule_name IS NULL OR
    start_logrecord_id IS NULL OR
    end_logrecord_id IS NULL;";
                alertExtractSelectCmd.Prepare();
                MySqlDataReader alertExtractResult = alertExtractSelectCmd.ExecuteReader();

                while (alertExtractResult.Read())
                {
                    AlertExtractField tmpAlertExtract = new AlertExtractField(
                                                    alertExtractResult.IsDBNull(0) ? null : new int?(alertExtractResult.GetInt32("id")),
                                                    alertExtractResult.GetString("device_id"),
                                                    alertExtractResult.IsDBNull(2) ? null : alertExtractResult.GetString("vin"),
                                                    alertExtractResult.GetString("exceptionevent_id"),
                                                    alertExtractResult.GetString("rule_id"),
                                                    alertExtractResult.IsDBNull(5) ? null : alertExtractResult.GetString("rule_name"),
                                                    alertExtractResult.IsDBNull(6) ? null : new float?(alertExtractResult.GetFloat("distance")),
                                                    alertExtractResult.IsDBNull(7) ? null : new DateTime?(alertExtractResult.GetDateTime("start_date_time")),
                                                    alertExtractResult.IsDBNull(8) ? null : new DateTime?(alertExtractResult.GetDateTime("end_date_time")),
                                                    alertExtractResult.IsDBNull(9) ? null : alertExtractResult.GetString("start_logrecord_id"),
                                                    alertExtractResult.IsDBNull(10) ? null : new double?(alertExtractResult.GetDouble("start_timediff")),
                                                    alertExtractResult.IsDBNull(11) ? null : new double?(alertExtractResult.GetDouble("start_latitude")),
                                                    alertExtractResult.IsDBNull(12) ? null : new double?(alertExtractResult.GetDouble("start_longitude")),
                                                    alertExtractResult.IsDBNull(13) ? null : alertExtractResult.GetString("end_logrecord_id"),
                                                    alertExtractResult.IsDBNull(14) ? null : new double?(alertExtractResult.GetDouble("end_timediff")),
                                                    alertExtractResult.IsDBNull(15) ? null : new double?(alertExtractResult.GetDouble("end_latitude")),
                                                    alertExtractResult.IsDBNull(16) ? null : new double?(alertExtractResult.GetDouble("end_longitude"))
                                                    );
                    if (tmpAlertExtract.id != null &&
                        tmpAlertExtract.exceptionevent_id != null && !tmpAlertExtract.exceptionevent_id.Equals("") &&
                        tmpAlertExtract.device_id != null && !tmpAlertExtract.device_id.Equals("") &&
                        tmpAlertExtract.rule_id != null && !tmpAlertExtract.rule_id.Equals("") &&
                        tmpAlertExtract.distance != null &&
                        tmpAlertExtract.start_date_time != null &&
                        tmpAlertExtract.end_date_time != null)
                    {
                        lstAlertExtract.Add(tmpAlertExtract);
                    }
                    else
                    {
                        log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "Invalid AlertExtract found. Please cleanup the ge_alertextract table." }));
                    }
                }
            }
            catch (MySqlException mse)
            {
                log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
            }
            finally
            {
                mysqlconn.Close();
            }
            log.write(Logger.LogLevel.Information, string.Join("", new string[] { lstAlertExtract.Count.ToString(), " AlertExtract extracted." }));
            return lstAlertExtract;
        }

        /// <summary>
        /// AlertExtract are updated in two phases, this function is to insert the AlertExtract in the DB with the information we have from the ExceptionEvent (first phase).
        /// </summary>
        /// <param name="lstAlertExtract">List of AlertExtract to insert in the DB.</param>
        /// <returns>Returns true if no SQL Exception is returned.</returns>
        public bool InsertAlertExtract(List<AlertExtractField> lstAlertExtract)
        {
            Logger log = Logger.getInstance();
            bool isSqlException = false;

            if (lstAlertExtract != null)
            {
                log.write(Logger.LogLevel.Information, string.Join("", new string[] { "Inserting AlertExtract in MySQL DB." }));
                try
                {
                    mysqlconn.Open();
                    int itemcount = 0;
                    foreach (AlertExtractField alertExtract in lstAlertExtract)
                    {
                        /*//TODO: remove
                        DateTime tempDT = new DateTime(2017, 10, 17, 12, 0, 0);
                        DateTime tempDT2 = new DateTime(2017, 10, 18, 0, 0, 0);
                        if (alertExtract.start_date_time < tempDT || alertExtract.start_date_time > tempDT2) {
                            continue;
                        }*/
                        if (++itemcount % 5000 == 0)
                        {
                            log.write(Logger.LogLevel.Debug, String.Format("{0} items processed.", itemcount.ToString()));
                        }
                        try
                        {
                            //Insert the AlertExtract in the DB
                            MySqlCommand alertExtractInsertCmd = new MySqlCommand();
                            alertExtractInsertCmd.Connection = mysqlconn;
                            alertExtractInsertCmd.CommandText =
 @"INSERT INTO ge_alertextract (
    device_id,
    exceptionevent_id,
    rule_id,
    distance,
    start_date_time,
    end_date_time,
    start_logrecord_id,
    start_timediff,
    start_latitude,
    start_longitude,
    end_logrecord_id,
    end_timediff,
    end_latitude,
    end_longitude
)
Values (
    @device_id,
    @exceptionevent_id,
    @rule_id,
    @distance,
    @start_date_time,
    @end_date_time,
    NULL,
    0,
    0,
    0,
    NULL,
    0,
    0,
    0
) 
ON DUPLICATE KEY UPDATE
    distance=@distance,
    end_date_time=@end_date_time,
    end_logrecord_id=NULL,
    end_timediff=0,
    end_latitude=0,
    end_longitude=0; ";
                            alertExtractInsertCmd.Prepare();
                            alertExtractInsertCmd.Parameters.AddWithValue("@device_id", alertExtract.device_id);
                            alertExtractInsertCmd.Parameters.AddWithValue("@exceptionevent_id", alertExtract.exceptionevent_id);
                            alertExtractInsertCmd.Parameters.AddWithValue("@rule_id", alertExtract.rule_id);
                            alertExtractInsertCmd.Parameters.AddWithValue("@distance", alertExtract.distance);
                            alertExtractInsertCmd.Parameters.AddWithValue("@start_date_time", formatDateMySQL(alertExtract.start_date_time));
                            alertExtractInsertCmd.Parameters.AddWithValue("@end_date_time", formatDateMySQL(alertExtract.end_date_time));

                            alertExtractInsertCmd.ExecuteNonQuery();
                        }
                        catch (MySqlException mse)
                        {
                            if (mse.Number == 1062)
                            {
                                log.write(Logger.LogLevel.Warning, String.Format("Duplicate value found when inserting the AlertExtractField for exceptionevent_id={0}.", alertExtract.exceptionevent_id));
                            }
                            else
                            {
                                log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
                                isSqlException = true;
                            }
                        }
                    }
                    log.write(Logger.LogLevel.Information, "Done inserting AlertExtract.");
                }
                catch (MySqlException mse)
                {
                    log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
                    isSqlException = true;
                }
                finally
                {
                    mysqlconn.Close();
                }
            }
            else
            {
                log.write(Logger.LogLevel.Warning, "No AlertExtract to insert.");
            }

            return !isSqlException;
        }

        /// <summary>
        /// AlertExtract are updated in two phases, this function is to update the AlertExtract with all the missing informations (second phase).
        /// </summary>
        /// <param name="alertExtract">List of AlertExtract to update in the DB</param>
        /// <returns>Returns true if no SQL Exception is returned.</returns>
        public bool UpdateAlertExtractRuleNameAndGPS(AlertExtractField alertExtract)
        {
            List<AlertExtractField> lstAlertExtract = new List<AlertExtractField>();
            lstAlertExtract.Add(alertExtract);
            return UpdateAlertExtractRuleNameAndGPS(lstAlertExtract);
        }

        /// <summary>
        /// AlertExtract are updated in two phases, this function is to update the AlertExtract with all the missing informations (second phase).
        /// </summary>
        /// <param name="lstAlertExtract">List of AlertExtract to update in the DB</param>
        /// <returns>Returns true if no SQL Exception is returned.</returns>
        public bool UpdateAlertExtractRuleNameAndGPS(List<AlertExtractField> lstAlertExtract)
        {
            Logger log = Logger.getInstance();
            bool isSqlException = false;

            if (lstAlertExtract != null)
            {
                log.write(9, string.Join("", new string[] { "Updating AlertExtract Rule name and GPS values in MySQL DB." }));
                try
                {
                    mysqlconn.Open();

                    int itemcount = 0;
                    foreach (AlertExtractField alertExtract in lstAlertExtract)
                    {
                        if (++itemcount % 500 == 0)
                        {
                            log.write(Logger.LogLevel.Debug, String.Format("{0} items processed.", itemcount.ToString()));
                        }
                        //Insert/update the EngineType in the DB
                        MySqlCommand alertExtractUpdateCmd = new MySqlCommand();
                        alertExtractUpdateCmd.Connection = mysqlconn;
                        alertExtractUpdateCmd.CommandText =
@"UPDATE ge_alertextract SET 
    rule_name=@rule_name,
    start_logrecord_id=@start_logrecord_id,
    start_timediff=@start_timediff,
    start_latitude=@start_latitude,
    start_longitude=@start_longitude,
    end_logrecord_id=@end_logrecord_id,
    end_timediff=@end_timediff,
    end_latitude=@end_latitude,
    end_longitude=@end_longitude
WHERE id=@id;";
                        /*
                        log.write(Logger.LogLevel.Debug, "id=" + alertExtract.id);
                        log.write(Logger.LogLevel.Debug, "rule_name=" + alertExtract.rule_name);
                        log.write(Logger.LogLevel.Debug, "start_logrecord_id=" + alertExtract.start_logrecord_id);
                        log.write(Logger.LogLevel.Debug, "start_timediff=" + alertExtract.start_timediff);
                        log.write(Logger.LogLevel.Debug, "start_latitude=" + alertExtract.start_latitude);
                        log.write(Logger.LogLevel.Debug, "start_longitude=" + alertExtract.start_longitude);
                        log.write(Logger.LogLevel.Debug, "end_logrecord_id=" + alertExtract.end_logrecord_id);
                        log.write(Logger.LogLevel.Debug, "end_timediff=" + alertExtract.end_timediff);
                        log.write(Logger.LogLevel.Debug, "end_latitude=" + alertExtract.end_latitude);
                        log.write(Logger.LogLevel.Debug, "end_longitude=" + alertExtract.end_longitude);
                        */
                        alertExtractUpdateCmd.Prepare();
                        alertExtractUpdateCmd.Parameters.AddWithValue("@rule_name", ((Object)alertExtract.rule_name) ?? DBNull.Value);
                        alertExtractUpdateCmd.Parameters.AddWithValue("@start_logrecord_id", ((Object)alertExtract.start_logrecord_id) ?? DBNull.Value);
                        alertExtractUpdateCmd.Parameters.AddWithValue("@start_timediff", ((Object)alertExtract.start_timediff) ?? DBNull.Value);
                        alertExtractUpdateCmd.Parameters.AddWithValue("@start_latitude", ((Object)alertExtract.start_latitude) ?? DBNull.Value);
                        alertExtractUpdateCmd.Parameters.AddWithValue("@start_longitude", ((Object)alertExtract.start_longitude) ?? DBNull.Value);
                        alertExtractUpdateCmd.Parameters.AddWithValue("@end_logrecord_id", ((Object)alertExtract.end_logrecord_id) ?? DBNull.Value);
                        alertExtractUpdateCmd.Parameters.AddWithValue("@end_timediff", ((Object)alertExtract.end_timediff) ?? DBNull.Value);
                        alertExtractUpdateCmd.Parameters.AddWithValue("@end_latitude", ((Object)alertExtract.end_latitude) ?? DBNull.Value);
                        alertExtractUpdateCmd.Parameters.AddWithValue("@end_longitude", ((Object)alertExtract.end_longitude) ?? DBNull.Value);
                        alertExtractUpdateCmd.Parameters.AddWithValue("@id", alertExtract.id);
                        alertExtractUpdateCmd.ExecuteNonQuery();
                    }
                    log.write(9, "Done updating Rule name and GPS values for AlertExtract.");
                }
                catch (MySqlException mse)
                {
                    log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
                    isSqlException = true;
                }
                finally
                {
                    mysqlconn.Close();
                }
            }
            else
            {
                log.write(Logger.LogLevel.Warning, "No Source to update.");
            }
            return !isSqlException;
        }

        /// <summary>
        /// Updates the feed version in the DB.
        /// </summary>
        /// <param name="feedname">feed to update in the DB.</param>
        /// <returns>Returns feed version or -1 if an error occured.</returns>
        public bool UpdateFeedVersion(string feedname, long feedversion)
        {
            Logger log = Logger.getInstance();
            bool isSqlException = false;

            try
            {
                mysqlconn.Open();

                log.write(Logger.LogLevel.Information, string.Join("", new string[] { "Updating \"", feedname, "\" version to ", feedversion.ToString(), "." }));
                MySqlCommand feedVersionUpdateCmd = new MySqlCommand();
                feedVersionUpdateCmd.Connection = mysqlconn;
                feedVersionUpdateCmd.CommandText = "UPDATE ge_feedversion SET version=@version WHERE feedname=@feedname";
                feedVersionUpdateCmd.Prepare();
                feedVersionUpdateCmd.Parameters.AddWithValue("@feedname", feedname);
                feedVersionUpdateCmd.Parameters.AddWithValue("@version", feedversion);
                feedVersionUpdateCmd.ExecuteNonQuery();
                log.write(Logger.LogLevel.Information, string.Join("", new string[] { "\"", feedname, "\" version updated." }));
            }
            catch (MySqlException mse)
            {
                log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
                isSqlException = true;
            }
            finally
            {
                mysqlconn.Close();
            }

            return !isSqlException;
        }

        /*
        /// <summary>
        /// 
        /// </summary>
        /// <param name="controllers"></param>
        /// <returns></returns>
        public bool updateControllers(List<ControllerField> controllers)
        {
            Logger log = Logger.getInstance();
            bool isSqlException = false;

            if (controllers != null)
            {
                log.write(Logger.LogLevel.Information, string.Join("", new string[] { "Updating Controller in MySQL DB." }));
                try
                {
                    mysqlconn.Open();

                    foreach (ControllerField controller in controllers)
                    {
                        //Insert/update the EngineType in the DB
                        MySqlCommand controllerInsertCmd = new MySqlCommand();
                        controllerInsertCmd.Connection = mysqlconn;
                        controllerInsertCmd.CommandText =
@"INSERT INTO gt_controller (
    id,
    source_id,
    code,
    name,
    version
)
Values (
    @id,
    @source_id,
    @code,
    @name,
    @version
)
ON DUPLICATE KEY UPDATE
    source_id=@source_id,
    code=@code,
    name=@name,
    version=@version;";
                        controllerInsertCmd.Prepare();
                        controllerInsertCmd.Parameters.AddWithValue("@id", controller.id);
                        controllerInsertCmd.Parameters.AddWithValue("@source_id", controller.source_id);
                        controllerInsertCmd.Parameters.AddWithValue("@code", controller.code);
                        controllerInsertCmd.Parameters.AddWithValue("@name", controller.name);
                        controllerInsertCmd.Parameters.AddWithValue("@version", controller.version);
                        controllerInsertCmd.ExecuteNonQuery();
                    }
                    log.write(Logger.LogLevel.Information, "Done updating Controllers.");
                }
                catch (MySqlException mse)
                {
                    log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
                    isSqlException = true;
                }
                finally
                {
                    mysqlconn.Close();
                }
            }
            else
            {
                log.write(Logger.LogLevel.Warning, "No Controller to update.");
            }
            return !isSqlException;
        }*/

        /// <summary>
        /// Insert/Update devices in the MySQL server based on device list provided.
        /// </summary>
        /// <param name="devices"></param>
        /// <returns>Returns true if success.</returns>
        public bool UpdateDevices(List<DeviceField> devices)
        {
            Logger log = Logger.getInstance();
            bool isSqlException = false;

            if (devices != null)
            {
                log.write(Logger.LogLevel.Information, string.Join("", new string[] { "Updating Devices in MySQL DB." }));
                try
                {
                    mysqlconn.Open();

                    int itemcount = 0;
                    foreach (DeviceField device in devices)
                    {
                        if (++itemcount % 100 == 0)
                        {
                            log.write(Logger.LogLevel.Debug, String.Format("{0} items processed.", itemcount.ToString()));
                        }
                        //log.write(Logger.LogLevel.Debug, string.Join("", new string[] { "Inserting or updating \"", device.id, "\"..." }));

                        //Insert/update the device in the DB
                        MySqlCommand deviceInsertCmd = new MySqlCommand();
                        deviceInsertCmd.Connection = mysqlconn;
                        deviceInsertCmd.CommandText =
@"INSERT INTO gt_device (
    id,
    name,
    productid,
    serialnumber,
    hardwareid,
    worktime,
    activefrom,
    activeto,
    version,
    comment
)
Values (
    @id,
    @name,
    @productid,
    @serialnumber,
    @hardwareid,
    @worktime,
    @activefrom,
    @activeto,
    @version,
    @comment
)
ON DUPLICATE KEY UPDATE
    name=@name,
    productid=@productid,
    serialnumber=@serialnumber,
    hardwareid=@hardwareid,
    worktime=@worktime,
    activefrom=@activefrom,
    activeto=@activeto,
    version=@version,
    comment=@comment;";
                        deviceInsertCmd.Prepare();
                        deviceInsertCmd.Parameters.AddWithValue("@id", device.id);
                        deviceInsertCmd.Parameters.AddWithValue("@name", device.name);
                        deviceInsertCmd.Parameters.AddWithValue("@productid", device.productId);
                        deviceInsertCmd.Parameters.AddWithValue("@serialnumber", device.serialnumber);
                        deviceInsertCmd.Parameters.AddWithValue("@hardwareid", device.hardwareId);
                        deviceInsertCmd.Parameters.AddWithValue("@worktime", device.worktime);
                        deviceInsertCmd.Parameters.AddWithValue("@activefrom", device.activeFrom);
                        deviceInsertCmd.Parameters.AddWithValue("@activeto", device.activeTo);
                        deviceInsertCmd.Parameters.AddWithValue("@version", device.version);
                        deviceInsertCmd.Parameters.AddWithValue("@comment", device.comment);
                        deviceInsertCmd.ExecuteNonQuery();

                        //log.write(Logger.LogLevel.Debug, string.Join("", new string[] { "Device \"", device.id, "\" added." }));
                    }
                    log.write(Logger.LogLevel.Information, "Done updating Devices.");
                }
                catch (MySqlException mse)
                {
                    log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
                    isSqlException = true;
                }
                finally
                {
                    mysqlconn.Close();
                }
            }
            else
            {
                log.write(Logger.LogLevel.Warning, "No devices to update.");
            }
            return !isSqlException;
        }
        /// <summary>
        /// Insert/Update Go7 devices in the MySQL server based on device list provided.
        /// </summary>
        /// <param name="go7devices"></param>
        /// <returns>Returns true if success.</returns>
        public bool UpdateGo7(List<Go7Field> go7devices)
        {
            Logger log = Logger.getInstance();
            bool isSqlException = false;

            if (go7devices != null)
            {
                log.write(Logger.LogLevel.Information, string.Join("", new string[] { "Updating Go7 devices in MySQL DB." }));
                try
                {
                    mysqlconn.Open();

                    int itemcount = 0;
                    foreach (Go7Field go7device in go7devices)
                    {
                        if (++itemcount % 100 == 0)
                        {
                            log.write(Logger.LogLevel.Debug, String.Format("{0} items processed.", itemcount.ToString()));
                        }
                        //log.write(Logger.LogLevel.Debug, string.Join("", new string[] { "Inserting or updating \"", device.id, "\"..." }));

                        //Insert/update the device in the DB
                        MySqlCommand go7DdeviceInsertCmd = new MySqlCommand();
                        go7DdeviceInsertCmd.Connection = mysqlconn;
                        go7DdeviceInsertCmd.CommandText =
@"INSERT INTO gt_go7 (
    id,
    name,
    productid,
    serialnumber,
    hardwareid,
    worktime,
    activefrom,
    activeto,
    comment,
    vehiculeIdentificationNumber
)
Values (
    @id,
    @name,
    @productid,
    @serialnumber,
    @hardwareid,
    @worktime,
    @activefrom,
    @activeto,
    @comment,
    @vehiculeIdentificationNumber
)
ON DUPLICATE KEY UPDATE
    name=@name,
    productid=@productid,
    serialnumber=@serialnumber,
    hardwareid=@hardwareid,
    worktime=@worktime,
    activefrom=@activefrom,
    activeto=@activeto,
    comment=@comment,
    vehiculeIdentificationNumber=@vehiculeIdentificationNumber;";
                        go7DdeviceInsertCmd.Prepare();
                        go7DdeviceInsertCmd.Parameters.AddWithValue("@id", go7device.id);
                        go7DdeviceInsertCmd.Parameters.AddWithValue("@name", go7device.name);
                        go7DdeviceInsertCmd.Parameters.AddWithValue("@productid", go7device.productId);
                        go7DdeviceInsertCmd.Parameters.AddWithValue("@serialnumber", go7device.serialnumber);
                        go7DdeviceInsertCmd.Parameters.AddWithValue("@hardwareid", go7device.hardwareId);
                        go7DdeviceInsertCmd.Parameters.AddWithValue("@worktime", go7device.worktime);
                        go7DdeviceInsertCmd.Parameters.AddWithValue("@activefrom", go7device.activeFrom);
                        go7DdeviceInsertCmd.Parameters.AddWithValue("@activeto", go7device.activeTo);
                        go7DdeviceInsertCmd.Parameters.AddWithValue("@comment", go7device.comment);
                        go7DdeviceInsertCmd.Parameters.AddWithValue("@vehiculeIdentificationNumber", go7device.vehiculeIdentificationNumber);
                        go7DdeviceInsertCmd.ExecuteNonQuery();

                        //log.write(Logger.LogLevel.Debug, string.Join("", new string[] { "Device \"", device.id, "\" added." }));
                    }
                    log.write(Logger.LogLevel.Information, "Done updating Go7 devices.");
                }
                catch (MySqlException mse)
                {
                    log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
                    isSqlException = true;
                }
                finally
                {
                    mysqlconn.Close();
                }
            }
            else
            {
                log.write(Logger.LogLevel.Warning, "No Go7 devices to update.");
            }
            return !isSqlException;
        }

        public bool UpdateDiagnostics(List<DiagnosticField> diagnostics)
        {
            Logger log = Logger.getInstance();
            bool isSqlException = false;

            if (diagnostics != null)
            {
                log.write(Logger.LogLevel.Information, string.Join("", new string[] { "Updating Diagnostics in MySQL DB." }));
                try
                {
                    mysqlconn.Open();

                    int itemcount = 0;
                    foreach (DiagnosticField diagnostic in diagnostics)
                    {
                        if (++itemcount % 1000 == 0)
                        {
                            log.write(Logger.LogLevel.Debug, String.Format("{0} items processed.", itemcount.ToString()));
                        }
                        //Insert/update the EngineType in the DB
                        MySqlCommand diagnosticInsertCmd = new MySqlCommand();
                        diagnosticInsertCmd.Connection = mysqlconn;
                        diagnosticInsertCmd.CommandText =
@"INSERT INTO gt_diagnostic (
    id,
    controller_id,
    enginetype_id,
    source_id,
    unitOfMeasure_id,
    code,
    diagnosticType,
    faultResetMode,
    name,
    version
)
Values (
    @id,
    @controller_id,
    @enginetype_id,
    @source_id,
    @unitOfMeasure_id,
    @code,
    @diagnosticType,
    @faultResetMode,
    @name,
    @version
)
ON DUPLICATE KEY UPDATE
    controller_id=@controller_id,
    enginetype_id=@enginetype_id,
    source_id=@source_id,
    unitOfMeasure_id=@unitOfMeasure_id,
    code=@code,
    diagnosticType=@diagnosticType,
    faultResetMode=@faultResetMode,
    name=@name,
    version=@version;";
                        diagnosticInsertCmd.Prepare();
                        diagnosticInsertCmd.Parameters.AddWithValue("@id", diagnostic.id);
                        diagnosticInsertCmd.Parameters.AddWithValue("@controller_id", diagnostic.controller_id);
                        diagnosticInsertCmd.Parameters.AddWithValue("@enginetype_id", diagnostic.enginetype_id);
                        diagnosticInsertCmd.Parameters.AddWithValue("@source_id", diagnostic.source_id);
                        diagnosticInsertCmd.Parameters.AddWithValue("@unitOfMeasure_id", diagnostic.unitOfMeasure_id);
                        diagnosticInsertCmd.Parameters.AddWithValue("@code", diagnostic.code);
                        diagnosticInsertCmd.Parameters.AddWithValue("@diagnosticType", diagnostic.diagnosticType);
                        diagnosticInsertCmd.Parameters.AddWithValue("@faultResetMode", diagnostic.faultResetMode);
                        diagnosticInsertCmd.Parameters.AddWithValue("@name", diagnostic.name);
                        diagnosticInsertCmd.Parameters.AddWithValue("@version", diagnostic.version);
                        /*
                         * log.write(9, "id= " + diagnostic.id);
                        log.write(9, "controller_id= " + diagnostic.controller_id);
                        if (diagnostic.controller_id == null)
                            log.write(9, "controller_id is null");
                        log.write(9, "enginetype_id= " + diagnostic.enginetype_id);
                        if (diagnostic.enginetype_id == null)
                            log.write(9, "enginetype_id is null");
                        log.write(9, "source_id= " + diagnostic.source_id);
                        if (diagnostic.source_id == null)
                            log.write(9, "source_id is null");
                        log.write(9, "unitOfMeasure_id= " + diagnostic.unitOfMeasure_id);
                        if (diagnostic.unitOfMeasure_id == null)
                            log.write(9, "unitOfMeasure_id is null");
                        log.write(9, "code= " + diagnostic.code.ToString());
                        log.write(9, "diagnosticType= " + diagnostic.diagnosticType);
                        log.write(9, "faultResetMode= " + diagnostic.faultResetMode);
                        log.write(9, "name= " + diagnostic.name);
                        log.write(9, "version= " + diagnostic.version.ToString());
                        */
                        diagnosticInsertCmd.ExecuteNonQuery();
                    }
                    log.write(Logger.LogLevel.Information, "Done updating Diagnostics.");
                }
                catch (MySqlException mse)
                {
                    log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
                    isSqlException = true;
                }
                finally
                {
                    mysqlconn.Close();
                }
            }
            else
            {
                log.write(Logger.LogLevel.Warning, "No Controller to update.");
            }
            return !isSqlException;
        }

        /*
        public bool updateEngineTypes(List<EngineTypeField> engineTypes)
        {
            Logger log = Logger.getInstance();
            bool isSqlException = false;

            if (engineTypes != null)
            {
                log.write(Logger.LogLevel.Information, string.Join("", new string[] { "Updating EngineType in MySQL DB." }));
                try
                {
                    mysqlconn.Open();

                    foreach (EngineTypeField engineType in engineTypes)
                    {
                        //Insert/update the EngineType in the DB
                        MySqlCommand engineTypeInsertCmd = new MySqlCommand();
                        engineTypeInsertCmd.Connection = mysqlconn;
                        engineTypeInsertCmd.CommandText =
@"INSERT INTO gt_enginetype (
    id,
    name
)
Values (
    @id,
    @name
)
ON DUPLICATE KEY UPDATE
    name=@name;";
                        engineTypeInsertCmd.Prepare();
                        engineTypeInsertCmd.Parameters.AddWithValue("@id", engineType.id);
                        engineTypeInsertCmd.Parameters.AddWithValue("@name", engineType.name);
                        engineTypeInsertCmd.ExecuteNonQuery();
                    }
                    log.write(Logger.LogLevel.Information, "Done updating EngineTypes.");
                }
                catch (MySqlException mse)
                {
                    log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
                    isSqlException = true;
                }
                finally
                {
                    mysqlconn.Close();
                }
            }
            else
            {
                log.write(Logger.LogLevel.Warning, "No EngineType to update.");
            }
            return !isSqlException;
        }*/

        /// <summary>
        /// Insert DutyStatusLogs found in the DB (does not update it to possibly improve performance).
        /// </summary>
        /// <param name="dutystatuslogs">List of DutyStatusLogs to insert in the DB</param>
        /// <returns>Returns true if no MySQL error is returned.</returns>
        public bool UpdateDutyStatusLogs(List<DutyStatusLogField> dutystatuslogs)
        {
            Logger log = Logger.getInstance();
            bool isSqlException = false;

            if (dutystatuslogs != null)
            {
                log.write(Logger.LogLevel.Debug, string.Join("", new string[] { "Updating DutyStatusLogs in MySQL DB." }));
                try
                {
                    mysqlconn.Open();

                    int itemcount = 0;
                    foreach (DutyStatusLogField dutystatuslog in dutystatuslogs)
                    {
                        if (++itemcount % 1000 == 0)
                        {
                            log.write(Logger.LogLevel.Debug, String.Format("{0} items processed.", itemcount.ToString()));
                        }
                        //Insert the device in the DB
                        MySqlCommand dutystatuslogInsertCmd = new MySqlCommand();
                        dutystatuslogInsertCmd.Connection = mysqlconn;
                        dutystatuslogInsertCmd.CommandText =
@"INSERT INTO gt_dutystatuslog (
    dutystatuslog_id,
    device_id,
    driver_id,
    parentid,
    state,
    status,
    datetime,
    editdatetime,
    location,
    origin,
    verifydatetime,
    version
)
Values (
    @dutystatuslog_id,
    @device_id,
    @driver_id,
    @parentid,
    @state,
    @status,
    @datetime,
    @editdatetime,
    @location,
    @origin,
    @verifydatetime,
    @version
);";
                        dutystatuslogInsertCmd.Prepare();
                        dutystatuslogInsertCmd.Parameters.AddWithValue("@dutystatuslog_id", dutystatuslog.id);
                        dutystatuslogInsertCmd.Parameters.AddWithValue("@device_id", dutystatuslog.device_id);
                        dutystatuslogInsertCmd.Parameters.AddWithValue("@driver_id", dutystatuslog.driver_id);
                        dutystatuslogInsertCmd.Parameters.AddWithValue("@parentid", dutystatuslog.parentid);
                        dutystatuslogInsertCmd.Parameters.AddWithValue("@state", dutystatuslog.state);
                        dutystatuslogInsertCmd.Parameters.AddWithValue("@status", dutystatuslog.status);
                        dutystatuslogInsertCmd.Parameters.AddWithValue("@datetime", formatDateMySQL(dutystatuslog.datetime));
                        dutystatuslogInsertCmd.Parameters.AddWithValue("@editdatetime", formatDateMySQL(dutystatuslog.editdatetime));
                        dutystatuslogInsertCmd.Parameters.AddWithValue("@location", dutystatuslog.location);
                        dutystatuslogInsertCmd.Parameters.AddWithValue("@origin", dutystatuslog.origin);
                        dutystatuslogInsertCmd.Parameters.AddWithValue("@verifydatetime", formatDateMySQL(dutystatuslog.verifydatetime));
                        dutystatuslogInsertCmd.Parameters.AddWithValue("@version", dutystatuslog.version);
                        dutystatuslogInsertCmd.ExecuteNonQuery();
                    }
                    log.write(Logger.LogLevel.Debug, "Done updating DutyStatusLogs");
                }
                catch (MySqlException mse)
                {
                    log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
                    isSqlException = true;
                }
                finally
                {
                    mysqlconn.Close();
                }
            }
            else
            {
                log.write(Logger.LogLevel.Debug, "No DutyStatusLog to update.");
            }
            return !isSqlException;
        }

        /// <summary>
        /// Insert ExceptionEvent found in the DB (does not update it to possibly improve performance).
        /// </summary>
        /// <param name="exceptionEvents">List of ExceptionEvent to insert in the DB</param>
        /// <returns>Returns true if no MySQL error is returned.</returns>
        public bool UpdateExceptionEvent(List<ExceptionEventField> exceptionEvents)
        {
            Logger log = Logger.getInstance();
            bool isSqlException = false;

            if (exceptionEvents != null)
            {
                log.write(Logger.LogLevel.Debug, string.Join("", new string[] { "Updating ExceptionEvent in MySQL DB." }));
                try
                {
                    mysqlconn.Open();

                    int itemcount = 0;
                    foreach (ExceptionEventField exceptionEvent in exceptionEvents)
                    {
                        if (++itemcount % 1000 == 0)
                        {
                            log.write(Logger.LogLevel.Debug, String.Format("{0} items processed.", itemcount.ToString()));
                        }
                        //Insert the device in the DB
                        MySqlCommand exceptionEventInsertCmd = new MySqlCommand();
                        exceptionEventInsertCmd.Connection = mysqlconn;
                        exceptionEventInsertCmd.CommandText =
@"INSERT IGNORE INTO gt_exceptionevent (
    exceptionevent_id,
    device_id,
    driver_id,
    rule_id,
    activeFrom,
    activeTo,
    distance,
    duration,
    version
)
Values (
    @exceptionevent_id,
    @device_id,
    @driver_id,
    @rule_id,
    @activeFrom,
    @activeTo,
    @distance,
    @duration,
    @version
)";
                        /*ON DUPLICATE KEY UPDATE*/
                        exceptionEventInsertCmd.Prepare();
                        exceptionEventInsertCmd.Parameters.AddWithValue("@exceptionevent_id", exceptionEvent.id);
                        exceptionEventInsertCmd.Parameters.AddWithValue("@device_id", exceptionEvent.device_id);
                        exceptionEventInsertCmd.Parameters.AddWithValue("@driver_id", exceptionEvent.driver_id);
                        exceptionEventInsertCmd.Parameters.AddWithValue("@rule_id", exceptionEvent.rule_id);
                        exceptionEventInsertCmd.Parameters.AddWithValue("@activeFrom", formatDateMySQL(exceptionEvent.activeFrom));
                        exceptionEventInsertCmd.Parameters.AddWithValue("@activeTo", formatDateMySQL(exceptionEvent.activeTo));
                        exceptionEventInsertCmd.Parameters.AddWithValue("@distance", exceptionEvent.distance);
                        exceptionEventInsertCmd.Parameters.AddWithValue("@duration", exceptionEvent.duration);
                        exceptionEventInsertCmd.Parameters.AddWithValue("@version", exceptionEvent.version);

                        //Console.WriteLine("id=" + exceptionEvent.id);
                        //Console.WriteLine("device_id=" + exceptionEvent.device_id);
                        //Console.WriteLine("driver_id=" + exceptionEvent.driver_id);
                        //Console.WriteLine("rule_id=" + exceptionEvent.rule_id);
                        //Console.WriteLine("activeFrom=" + exceptionEvent.activeFrom);
                        //Console.WriteLine("activeTo=" + exceptionEvent.activeTo);
                        //Console.WriteLine("distance=" + exceptionEvent.distance);
                        //Console.WriteLine("duration=" + exceptionEvent.duration);
                        //Console.WriteLine("version=" + exceptionEvent.version);
                        //Console.ReadKey();
                        exceptionEventInsertCmd.ExecuteNonQuery();
                    }
                    log.write(Logger.LogLevel.Debug, "Done updating ExceptionEvent");
                }
                catch (MySqlException mse)
                {
                    log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
                    isSqlException = true;
                }
                catch (Exception e)
                {
                    log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", e.ToString(), ". Message is: ", e.StackTrace }));
                    isSqlException = true;
                }
                finally
                {
                    mysqlconn.Close();
                }
            }
            else
            {
                log.write(Logger.LogLevel.Debug, "No ExceptionEvent to update.");
            }
            return !isSqlException;
        }

        /*
        public bool updateFailureModes(List<FailureModeField> failureModes)
        {
            Logger log = Logger.getInstance();
            bool isSqlException = false;

            if (failureModes != null)
            {
                log.write(Logger.LogLevel.Information, string.Join("", new string[] { "Updating FailureMode in MySQL DB." }));
                try
                {
                    mysqlconn.Open();

                    foreach (FailureModeField failureMode in failureModes)
                    {
                        //Insert/update the FailureMode in the DB
                        MySqlCommand failureModeInsertCmd = new MySqlCommand();
                        failureModeInsertCmd.Connection = mysqlconn;
                        failureModeInsertCmd.CommandText =
@"INSERT INTO gt_failuremode (
    id,
    source_id,
    code,
    name
)
Values (
    @id,
    @source_id,
    @code,
    @name
)
ON DUPLICATE KEY UPDATE
    name=@name,
    source_id=@source_id,
    code=@code,
    name=@name;";
                        failureModeInsertCmd.Prepare();
                        failureModeInsertCmd.Parameters.AddWithValue("@id", failureMode.id);
                        failureModeInsertCmd.Parameters.AddWithValue("@source_id", failureMode.source_id);
                        failureModeInsertCmd.Parameters.AddWithValue("@code", failureMode.code);
                        failureModeInsertCmd.Parameters.AddWithValue("@name", failureMode.name);
                        failureModeInsertCmd.ExecuteNonQuery();
                    }
                    log.write(Logger.LogLevel.Information, "Done updating FailureMode.");
                }
                catch (MySqlException mse)
                {
                    log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
                    isSqlException = true;
                }
                finally
                {
                    mysqlconn.Close();
                }
            }
            else
            {
                log.write(Logger.LogLevel.Warning, "No FailureMode to update.");
            }
            return !isSqlException;
        }
        */

        /*
        /// <summary>
        /// Insert Faultdata found in the DB (does not update it to possibly improve performance).
        /// </summary>
        /// <param name="faultdatalist">List of FaultData to insert in the DB</param>
        /// <returns>Returns true if no MySQL error is returned.</returns>
        public bool updateFaultData(List<FaultDataField> faultdatalist)
        {
            Logger log = Logger.getInstance();
            bool isSqlException = false;

            if (faultdatalist != null)
            {
                log.write(Logger.LogLevel.Debug, string.Join("", new string[] { "Updating FaultData in MySQL DB." }));
                try
                {
                    mysqlconn.Open();

                    foreach (FaultDataField faultdata in faultdatalist)
                    {
                        //Insert the device in the DB
                        MySqlCommand faultdataInsertCmd = new MySqlCommand();
                        faultdataInsertCmd.Connection = mysqlconn;
                        faultdataInsertCmd.CommandText =
@"INSERT IGNORE INTO gt_faultdata (
    id,
    device_id,
    diagnostic_id,
    controller_id,
    failuremode_id,
    flashcode_id,
    dismissuser_id,
    date_time,
    dismissdatetime,
    fault_count,
    faultstate,
    faultlampstate,
    amberwarninglamp,
    malfunctionlamp,
    redstoplamp
)
Values (
    @id,
    @device_id,
    @diagnostic_id,
    @controller_id,
    @failuremode_id,
    @flashcode_id,
    @dismissuser_id,
    @date_time,
    @dismissdatetime,
    @fault_count,
    @faultstate,
    @faultlampstate,
    @amberwarninglamp,
    @malfunctionlamp,
    @redstoplamp
)";
                        faultdataInsertCmd.Prepare();
                        faultdataInsertCmd.Parameters.AddWithValue("@id", faultdata.id);
                        faultdataInsertCmd.Parameters.AddWithValue("@device_id", faultdata.device_id);
                        faultdataInsertCmd.Parameters.AddWithValue("@diagnostic_id", faultdata.diagnostic_id);
                        faultdataInsertCmd.Parameters.AddWithValue("@controller_id", faultdata.controller_id);
                        faultdataInsertCmd.Parameters.AddWithValue("@failuremode_id", faultdata.failuremode_id);
                        faultdataInsertCmd.Parameters.AddWithValue("@flashcode_id", faultdata.flashcode_id);
                        faultdataInsertCmd.Parameters.AddWithValue("@dismissuser_id", faultdata.dismissuser_id);
                        faultdataInsertCmd.Parameters.AddWithValue("@date_time", faultdata.date_time);
                        faultdataInsertCmd.Parameters.AddWithValue("@dismissdatetime", faultdata.dismissdatetime);
                        faultdataInsertCmd.Parameters.AddWithValue("@fault_count", faultdata.fault_count);
                        faultdataInsertCmd.Parameters.AddWithValue("@faultstate", faultdata.faultstate);
                        faultdataInsertCmd.Parameters.AddWithValue("@faultlampstate", faultdata.faultlampstate);
                        faultdataInsertCmd.Parameters.AddWithValue("@amberwarninglamp", faultdata.amberwarninglamp);
                        faultdataInsertCmd.Parameters.AddWithValue("@malfunctionlamp", faultdata.malfunctionlamp);
                        faultdataInsertCmd.Parameters.AddWithValue("@redstoplamp", faultdata.redstoplamp);

                        //Console.WriteLine("faultdata.id=" + faultdata.id);
                        //Console.WriteLine("faultdata.device_id=" + faultdata.device_id);
                        //Console.WriteLine("faultdata.diagnostic_id=" + faultdata.diagnostic_id);
                        //Console.WriteLine("faultdata.controller_id=" + faultdata.controller_id);
                        //Console.WriteLine("faultdata.failuremode_id=" + faultdata.failuremode_id);
                        //Console.WriteLine("faultdata.flashcode_id=" + faultdata.flashcode_id);
                        //Console.WriteLine("faultdata.dismissuser_id=" + faultdata.dismissuser_id);
                        //Console.ReadKey();
                        faultdataInsertCmd.ExecuteNonQuery();
                    }
                    log.write(Logger.LogLevel.Debug, "Done updating FaultData");
                }
                catch (MySqlException mse)
                {
                    log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
                    isSqlException = true;
                }
                catch (Exception e)
                {
                    log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", e.ToString(), ". Message is: ", e.StackTrace }));
                    isSqlException = true;
                }
                finally
                {
                    mysqlconn.Close();
                }
            }
            else
            {
                log.write(Logger.LogLevel.Debug, "No FaultData to update.");
            }
            return !isSqlException;
        }
        */

        /*
        public bool updateFlashCodes(List<FlashCodeField> flashCodes)
        {
            Logger log = Logger.getInstance();
            bool isSqlException = false;

            if (flashCodes != null)
            {
                log.write(Logger.LogLevel.Information, string.Join("", new string[] { "Updating FlashCode in MySQL DB." }));
                try
                {
                    mysqlconn.Open();

                    foreach (FlashCodeField flashCode in flashCodes)
                    {
                        //Insert/update the FlashCode in the DB
                        MySqlCommand flashCodeInsertCmd = new MySqlCommand();
                        flashCodeInsertCmd.Connection = mysqlconn;
                        flashCodeInsertCmd.CommandText =
@"INSERT INTO gt_flashcode (
    id,
    diagnostic_id,
    failuremode_id,
    name,
    flashcodeindex,
    helpurl,
    pagereference,
    prioritylevel
)
Values (
    @id,
    @diagnostic_id,
    @failuremode_id,
    @name,
    @flashcodeindex,
    @helpurl,
    @pagereference,
    @prioritylevel
)
ON DUPLICATE KEY UPDATE
    diagnostic_id=@diagnostic_id
    failuremode_id=@failuremode_id
    name=@name
    flashcodeindex=@flashcodeindex
    helpurl=@helpurl
    pagereference=@pagereference
    prioritylevel=@prioritylevel;";
                        flashCodeInsertCmd.Prepare();
                        flashCodeInsertCmd.Parameters.AddWithValue("@id", flashCode.id);
                        flashCodeInsertCmd.Parameters.AddWithValue("@diagnostic_id", flashCode.diagnostic_id);
                        flashCodeInsertCmd.Parameters.AddWithValue("@failuremode_id", flashCode.failuremode_id);
                        flashCodeInsertCmd.Parameters.AddWithValue("@name", flashCode.name);
                        flashCodeInsertCmd.Parameters.AddWithValue("@flashcodeindex", flashCode.flashcodeindex);
                        flashCodeInsertCmd.Parameters.AddWithValue("@helpurl", flashCode.helpurl);
                        flashCodeInsertCmd.Parameters.AddWithValue("@pagereference", flashCode.pagereference);
                        flashCodeInsertCmd.Parameters.AddWithValue("@prioritylevel", flashCode.prioritylevel);
                        flashCodeInsertCmd.ExecuteNonQuery();
                    }
                    log.write(Logger.LogLevel.Information, "Done updating FlashCodes.");
                }
                catch (MySqlException mse)
                {
                    log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
                    isSqlException = true;
                }
                finally
                {
                    mysqlconn.Close();
                }
            }
            else
            {
                log.write(Logger.LogLevel.Warning, "No FlashCode to update.");
            }
            return !isSqlException;
        }*/

        /// <summary>
        /// Insert LogRecords found in the DB (does not update it to possibly improve performance).
        /// </summary>
        /// <param name="logRecords">List of LogRecords to insert in the DB</param>
        /// <returns>Returns true if no MySQL error is returned.</returns>
        public bool UpdateLogRecords(List<LogRecordField> logRecords)
        {
            Logger log = Logger.getInstance();
            bool isSqlException = false;

            if (logRecords != null)
            {
                log.write(Logger.LogLevel.Debug, string.Join("", new string[] { "Updating LogRecords in MySQL DB." }));
                try
                {
                    mysqlconn.Open();

                    int logrecordCount = 0;
                    foreach (LogRecordField logRecord in logRecords)
                    {

                        if (++logrecordCount % 5000 == 0)
                        {
                            log.write(Logger.LogLevel.Debug, String.Format("{0} items processed.", logrecordCount.ToString()));
                        }
                        //Insert the device in the DB
                        MySqlCommand logRecordInsertCmd = new MySqlCommand();
                        logRecordInsertCmd.Connection = mysqlconn;
                        logRecordInsertCmd.CommandText =
@"INSERT IGNORE INTO gt_logrecord (
    logrecord_id,
    device_id,
    date_time,
    speed,
    latitude,
    longitude,
    islastactive,
    version
)
Values (
    @logrecord_id,
    @device_id,
    @date_time,
    @speed,
    @latitude,
    @longitude,
    @islastactive,
    @version
)";
                        logRecordInsertCmd.Prepare();
                        logRecordInsertCmd.Parameters.AddWithValue("@logrecord_id", logRecord.id);
                        logRecordInsertCmd.Parameters.AddWithValue("@device_id", logRecord.device_id);
                        logRecordInsertCmd.Parameters.AddWithValue("@date_time", formatDateMySQL(logRecord.date_time));
                        logRecordInsertCmd.Parameters.AddWithValue("@speed", logRecord.speed);
                        logRecordInsertCmd.Parameters.AddWithValue("@latitude", logRecord.latitude);
                        logRecordInsertCmd.Parameters.AddWithValue("@longitude", logRecord.longitude);
                        logRecordInsertCmd.Parameters.AddWithValue("@islastactive", logRecord.islastactive);
                        logRecordInsertCmd.Parameters.AddWithValue("@version", logRecord.version);
                        logRecordInsertCmd.ExecuteNonQuery();
                    }
                    log.write(Logger.LogLevel.Debug, "Done updating LogRecords");
                }
                catch (MySqlException mse)
                {
                    log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
                    isSqlException = true;
                }
                finally
                {
                    mysqlconn.Close();
                }
            }
            else
            {
                log.write(Logger.LogLevel.Debug, "No devices to update.");
            }
            return !isSqlException;
        }

        public void UpdateOdometerVINs(ref List<OdometerField> lstOdometer)
        {
            Logger log = Logger.getInstance();

            foreach (OdometerField odometer in lstOdometer)
            {
                if (odometer.device_id != null && odometer.vin == null)
                {
                    odometer.vin = GetVIN(odometer.device_id);
                }
            }
        }

        /// <summary>
        /// Insert Trips found in the DB (does not update it to possibly improve performance).
        /// </summary>
        /// <param name="trips">List of Trips to insert in the DB</param>
        /// <returns>Returns true if no MySQL error is returned.</returns>
        public bool UpdateTrips(List<TripField> trips)
        {
            Logger log = Logger.getInstance();
            bool isSqlException = false;

            if (trips != null)
            {
                log.write(Logger.LogLevel.Debug, string.Join("", new string[] { "Updating Trips in MySQL DB." }));
                try
                {
                    mysqlconn.Open();

                    int itemcount = 0;
                    foreach (TripField trip in trips)
                    {
                        if (++itemcount % 1000 == 0)
                        {
                            log.write(Logger.LogLevel.Debug, String.Format("{0} items processed.", itemcount.ToString()));
                        }
                        //Insert the device in the DB
                        MySqlCommand tripInsertCmd = new MySqlCommand();
                        tripInsertCmd.Connection = mysqlconn;
                        tripInsertCmd.CommandText =
@"INSERT IGNORE INTO gt_trip (
    id,
    device_id,
    driver_id,
    datetime,
    afterhoursdistance,
    afterhoursdrivingduration,
    afterhoursend,
    afterhoursstart,
    afterhoursstopduration,
    averagespeed,
    idlingduration,
    maximumspeed,
    nexttripstart,
    speedrange1,
    speedrange1duration,
    speedrange2,
    speedrange2duration,
    speedrange3,
    speedrange3duration,
    start,
    stop,
    stopduration,
    stoppoint,
    workdistance,
    workdrivingduration,
    workstopduration,
    version
)
Values (
    @id,
    @device_id,
    @driver_id,
    @datetime,
    @afterhoursdistance,
    @afterhoursdrivingduration,
    @afterhoursend,
    @afterhoursstart,
    @afterhoursstopduration,
    @averagespeed,
    @idlingduration,
    @maximumspeed,
    @nexttripstart,
    @speedrange1,
    @speedrange1duration,
    @speedrange2,
    @speedrange2duration,
    @speedrange3,
    @speedrange3duration,
    @start,
    @stop,
    @stopduration,
    @stoppoint,
    @workdistance,
    @workdrivingduration,
    @workstopduration,
    @version
)";
                        tripInsertCmd.Prepare();
                        tripInsertCmd.Parameters.AddWithValue("@id", trip.id);
                        tripInsertCmd.Parameters.AddWithValue("@device_id", trip.device_id);
                        tripInsertCmd.Parameters.AddWithValue("@driver_id", trip.driver_id);
                        tripInsertCmd.Parameters.AddWithValue("@datetime", formatDateMySQL(trip.datetime));
                        tripInsertCmd.Parameters.AddWithValue("@afterhoursdistance", trip.afterhoursdistance);
                        tripInsertCmd.Parameters.AddWithValue("@afterhoursdrivingduration", trip.afterhoursdrivingduration);
                        tripInsertCmd.Parameters.AddWithValue("@afterhoursend", trip.afterhoursend);
                        tripInsertCmd.Parameters.AddWithValue("@afterhoursstart", trip.afterhoursstart);
                        tripInsertCmd.Parameters.AddWithValue("@afterhoursstopduration", trip.afterhoursstopduration);
                        tripInsertCmd.Parameters.AddWithValue("@averagespeed", trip.averagespeed);
                        tripInsertCmd.Parameters.AddWithValue("@idlingduration", trip.idlingduration);
                        tripInsertCmd.Parameters.AddWithValue("@maximumspeed", trip.maximumspeed);
                        tripInsertCmd.Parameters.AddWithValue("@nexttripstart", trip.nexttripstart);
                        tripInsertCmd.Parameters.AddWithValue("@speedrange1", trip.speedrange1);
                        tripInsertCmd.Parameters.AddWithValue("@speedrange1duration", trip.speedrange1duration);
                        tripInsertCmd.Parameters.AddWithValue("@speedrange2", trip.speedrange2);
                        tripInsertCmd.Parameters.AddWithValue("@speedrange2duration", trip.speedrange2duration);
                        tripInsertCmd.Parameters.AddWithValue("@speedrange3", trip.speedrange3);
                        tripInsertCmd.Parameters.AddWithValue("@speedrange3duration", trip.speedrange3duration);
                        tripInsertCmd.Parameters.AddWithValue("@start", formatDateMySQL(trip.start));
                        tripInsertCmd.Parameters.AddWithValue("@stop", formatDateMySQL(trip.stop));
                        tripInsertCmd.Parameters.AddWithValue("@stopduration", trip.stopduration);
                        tripInsertCmd.Parameters.AddWithValue("@stoppoint", trip.stoppoint);
                        tripInsertCmd.Parameters.AddWithValue("@workdistance", trip.workdistance);
                        tripInsertCmd.Parameters.AddWithValue("@workdrivingduration", trip.workdrivingduration);
                        tripInsertCmd.Parameters.AddWithValue("@workstopduration", trip.workstopduration);
                        tripInsertCmd.Parameters.AddWithValue("@version", trip.version);
                        tripInsertCmd.ExecuteNonQuery();
                    }
                    log.write(Logger.LogLevel.Debug, "Done updating Trips");
                }
                catch (MySqlException mse)
                {
                    log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
                    isSqlException = true;
                }
                finally
                {
                    mysqlconn.Close();
                }
            }
            else
            {
                log.write(Logger.LogLevel.Debug, "No trip to update.");
            }
            return !isSqlException;
        }

        /// <summary>
        /// Insert Odometer values from the provided list. 
        /// </summary>
        /// <param name="lstOdometer">List of Odometer values.</param>
        /// <returns>Returns true if no MySQL error is returned.</returns>
        public bool InsertOdometer(List<OdometerField> lstOdometer)
        {
            Logger log = Logger.getInstance();
            bool isSqlException = false;

            if (lstOdometer != null)
            {
                log.write(Logger.LogLevel.Debug, string.Join("", new string[] { "Inserting Odometer values in MySQL DB." }));
                try
                {
                    mysqlconn.Open();

                    int itemcount = 0;
                    foreach (OdometerField odometer in lstOdometer)
                    {
                        if (++itemcount % 1000 == 0)
                        {
                            log.write(Logger.LogLevel.Debug, String.Format("{0} items processed.", itemcount.ToString()));
                        }
                        //Insert the device in the DB
                        MySqlCommand odometerInsertCmd = new MySqlCommand();
                        odometerInsertCmd.Connection = mysqlconn;
                        odometerInsertCmd.CommandText =
@"INSERT IGNORE INTO ge_odometer (
    statusdata_id,
    device_id,
    vin,
    date_time,
    value
)
Values (
    @statusdata_id,
    @device_id,
    @vin,
    @date_time,
    @value
)";
                        odometerInsertCmd.Prepare();
                        odometerInsertCmd.Parameters.AddWithValue("@statusdata_id", odometer.statusdata_id);
                        odometerInsertCmd.Parameters.AddWithValue("@device_id", odometer.device_id);
                        odometerInsertCmd.Parameters.AddWithValue("@vin", odometer.vin);
                        odometerInsertCmd.Parameters.AddWithValue("@date_time", formatDateMySQL(odometer.date_time));
                        odometerInsertCmd.Parameters.AddWithValue("@value", odometer.value);
                        /*Console.WriteLine("odometer.id=" + odometer.id);
                        Console.WriteLine("odometer.device_id=" + odometer.device_id);
                        Console.WriteLine("odometer.date_time=" + odometer.date_time);
                        Console.WriteLine("odometer.data=" + odometer.value);
                        Console.WriteLine("=======================");
                        Console.ReadKey();*/
                        odometerInsertCmd.ExecuteNonQuery();
                    }
                    log.write(Logger.LogLevel.Debug, "Done inserting odometer values");
                }
                catch (MySqlException mse)
                {
                    log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
                    isSqlException = true;
                }
                finally
                {
                    mysqlconn.Close();
                }
            }
            else
            {
                log.write(Logger.LogLevel.Debug, "No odometer to update.");
            }
            return !isSqlException;
        }

        /// <summary>
        /// Insert/Update Rule in the MySQL server based on device list provided.
        /// </summary>
        /// <param name="rules"></param>
        /// <returns>Returns true if success.</returns>
        public bool UpdateRules(List<RuleField> rules)
        {
            Logger log = Logger.getInstance();
            bool isSqlException = false;

            if (rules != null)
            {
                log.write(Logger.LogLevel.Information, string.Join("", new string[] { "Updating Rules in MySQL DB." }));
                try
                {
                    mysqlconn.Open();

                    int itemcount = 0;
                    foreach (RuleField rule in rules)
                    {
                        if (++itemcount % 100 == 0)
                        {
                            log.write(Logger.LogLevel.Debug, String.Format("{0} items processed.", itemcount.ToString()));
                        }
                        //Insert/update the device in the DB
                        MySqlCommand ruleInsertCmd = new MySqlCommand();
                        ruleInsertCmd.Connection = mysqlconn;
                        ruleInsertCmd.CommandText =
@"INSERT INTO gt_rule (
    id,
    condition_id,
    activeFrom,
    activeTo,
    baseType,
    color,
    comment,
    name,
    version
)
Values (
    @id,
    @condition_id,
    @activeFrom,
    @activeTo,
    @baseType,
    @color,
    @comment,
    @name,
    @version
)
ON DUPLICATE KEY UPDATE
    condition_id=@condition_id,
    activeFrom=@activeFrom,
    activeTo=@activeTo,
    baseType=@baseType,
    color=@color,
    comment=@comment,
    name=@name,
    version=@version;";
                        ruleInsertCmd.Prepare();
                        ruleInsertCmd.Parameters.AddWithValue("@id", rule.id);
                        ruleInsertCmd.Parameters.AddWithValue("@condition_id", rule.condition_id);
                        ruleInsertCmd.Parameters.AddWithValue("@activeFrom", formatDateMySQL(rule.activeFrom));
                        ruleInsertCmd.Parameters.AddWithValue("@activeTo", formatDateMySQL(rule.activeTo));
                        ruleInsertCmd.Parameters.AddWithValue("@baseType", rule.baseType);
                        ruleInsertCmd.Parameters.AddWithValue("@color", rule.color);
                        ruleInsertCmd.Parameters.AddWithValue("@comment", rule.comment);
                        ruleInsertCmd.Parameters.AddWithValue("@name", rule.name);
                        ruleInsertCmd.Parameters.AddWithValue("@version", rule.version);
                        ruleInsertCmd.ExecuteNonQuery();
                    }
                    log.write(Logger.LogLevel.Information, "Done updating Rules.");
                }
                catch (MySqlException mse)
                {
                    log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
                    isSqlException = true;
                }
                finally
                {
                    mysqlconn.Close();
                }
            }
            else
            {
                log.write(Logger.LogLevel.Warning, "No Rules to update.");
            }
            return !isSqlException;
        }

        /*
        public bool updateSources(List<SourceField> sources)
        {
            Logger log = Logger.getInstance();
            bool isSqlException = false;

            if (sources != null)
            {
                log.write(Logger.LogLevel.Information, string.Join("", new string[] { "Updating Source in MySQL DB." }));
                try
                {
                    mysqlconn.Open();

                    foreach (SourceField source in sources)
                    {
                        //Insert/update the EngineType in the DB
                        MySqlCommand sourceInsertCmd = new MySqlCommand();
                        sourceInsertCmd.Connection = mysqlconn;
                        sourceInsertCmd.CommandText =
@"INSERT INTO gt_source (
    id,
    name
)
Values (
    @id,
    @name
)
ON DUPLICATE KEY UPDATE
    name=@name;";
                        sourceInsertCmd.Prepare();
                        sourceInsertCmd.Parameters.AddWithValue("@id", source.id);
                        sourceInsertCmd.Parameters.AddWithValue("@name", source.name);
                        sourceInsertCmd.ExecuteNonQuery();
                    }
                    log.write(Logger.LogLevel.Information, "Done updating Sources.");
                }
                catch (MySqlException mse)
                {
                    log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
                    isSqlException = true;
                }
                finally
                {
                    mysqlconn.Close();
                }
            }
            else
            {
                log.write(Logger.LogLevel.Warning, "No Source to update.");
            }
            return !isSqlException;
        }
        */


        /// <summary>
        /// Insert StatusData found in the DB.
        /// </summary>
        /// <param name="statusdatalist">List of StatusData to insert in the DB</param>
        /// <returns>Returns true if no MySQL error is returned.</returns>
        public bool updateOdometerFromStatusData(List<StatusDataField> statusdatalist)
        {
            Logger log = Logger.getInstance();
            bool isSqlException = false;

            if (statusdatalist != null)
            {
                log.write(Logger.LogLevel.Debug, string.Join("", new string[] { "Updating StatusData in MySQL DB." }));
                try
                {
                    mysqlconn.Open();

                    foreach (StatusDataField statusdata in statusdatalist)
                    {

                        if (statusdata.diagnostic_id.Equals("DiagnosticRawOdometerId"))
                        {

                            //Insert the device in the DB
                            MySqlCommand statusdataInsertCmd = new MySqlCommand();
                            statusdataInsertCmd.Connection = mysqlconn;
                            statusdataInsertCmd.CommandText =
    @"INSERT IGNORE INTO gt_statusdata (
    statusdata_id,
    device_id,
    diagnostic_id,
    date_time,
    data,
    version
)
Values (
    @statusdata_id,
    @device_id,
    @diagnostic_id,
    @date_time,
    @data,
    @version
)";
                            statusdataInsertCmd.Prepare();
                            statusdataInsertCmd.Parameters.AddWithValue("@statusdata_id", statusdata.id);
                            statusdataInsertCmd.Parameters.AddWithValue("@device_id", statusdata.device_id);
                            statusdataInsertCmd.Parameters.AddWithValue("@diagnostic_id", statusdata.diagnostic_id);
                            statusdataInsertCmd.Parameters.AddWithValue("@date_time", statusdata.date_time);
                            statusdataInsertCmd.Parameters.AddWithValue("@data", statusdata.data);
                            statusdataInsertCmd.Parameters.AddWithValue("@version", statusdata.version);
                            statusdataInsertCmd.ExecuteNonQuery();
                        }
                    }
                    log.write(Logger.LogLevel.Debug, "Done updating StatusData");
                }
                catch (MySqlException mse)
                {
                    log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
                    isSqlException = true;
                }
                finally
                {
                    mysqlconn.Close();
                }
            }
            else
            {
                log.write(Logger.LogLevel.Debug, "No statusdata to update.");
            }
            return !isSqlException;
        }


        /*
                public bool updateUnitOfMeasures(List<UnitOfMeasureField> unitOfMeasures)
                {
                    Logger log = Logger.getInstance();
                    bool isSqlException = false;

                    if (unitOfMeasures != null)
                    {
                        log.write(Logger.LogLevel.Information, string.Join("", new string[] { "Updating UnitOfMeasure in MySQL DB." }));
                        try
                        {
                            mysqlconn.Open();

                            foreach (UnitOfMeasureField unitOfMeasure in unitOfMeasures)
                            {
                                //Insert/update the UnitOfMeasure in the DB
                                MySqlCommand unitOfMeasureInsertCmd = new MySqlCommand();
                                unitOfMeasureInsertCmd.Connection = mysqlconn;
                                unitOfMeasureInsertCmd.CommandText =
        @"INSERT INTO gt_unitofmeasure (
            id,
            name
        )
        Values (
            @id,
            @name
        )
        ON DUPLICATE KEY UPDATE
            name=@name;";
                                unitOfMeasureInsertCmd.Prepare();
                                unitOfMeasureInsertCmd.Parameters.AddWithValue("@id", unitOfMeasure.id);
                                unitOfMeasureInsertCmd.Parameters.AddWithValue("@name", unitOfMeasure.name);
                                unitOfMeasureInsertCmd.ExecuteNonQuery();
                            }
                            log.write(Logger.LogLevel.Information, "Done updating UnitOfMeasures.");
                        }
                        catch (MySqlException mse)
                        {
                            log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
                            isSqlException = true;
                        }
                        finally
                        {
                            mysqlconn.Close();
                        }
                    }
                    else
                    {
                        log.write(Logger.LogLevel.Warning, "No unitOfMeasure to update.");
                    }
                    return !isSqlException;
                }
                */
        /*
        public bool updateUsers(List<UserField> users)
        {
            Logger log = Logger.getInstance();
            bool isSqlException = false;

            if (users != null)
            {
                log.write(Logger.LogLevel.Information, string.Join("", new string[] { "Updating Users in MySQL DB." }));
                try
                {
                    mysqlconn.Open();

                    foreach (UserField user in users)
                    {
                        //Insert/update the User in the DB
                        MySqlCommand userInsertCmd = new MySqlCommand();
                        userInsertCmd.Connection = mysqlconn;
                        userInsertCmd.CommandText =
@"INSERT INTO gt_user (
    id,
    acceptedEULA,
    activeFrom,
    activeTo,
    changePassword,
    user_comment,
    dateFormat,
    defaultGoogleMapStyle,
    defaultHereMapStyle,
    defaultMapEngine,
    defaultOpenStreetMapStyle,
    defaultPage,
    designation,
    employeeNo,
    firstDayOfWeek,
    firstName,
    fuelEconomyUnit,
    hosRuleSet,
    isEULAAccepted,
    isLabsEnabled,
    isMetric,
    isNewsEnabled,
    isPersonalConveyanceEnabled,
    isYardMoveEnabled,
    user_language,
    lastName,
    menuCollapsedNotified,
    name,
    password,
    showClickOnceWarning,
    timeZoneId,
    zoneDisplayMode
)
Values (
    @id,
    @acceptedEULA,
    @activeFrom,
    @activeTo,
    @changePassword,
    @user_comment,
    @dateFormat,
    @defaultGoogleMapStyle,
    @defaultHereMapStyle,
    @defaultMapEngine,
    @defaultOpenStreetMapStyle,
    @defaultPage,
    @designation,
    @employeeNo,
    @firstDayOfWeek,
    @firstName,
    @fuelEconomyUnit,
    @hosRuleSet,
    @isEULAAccepted,
    @isLabsEnabled,
    @isMetric,
    @isNewsEnabled,
    @isPersonalConveyanceEnabled,
    @isYardMoveEnabled,
    @user_language,
    @lastName,
    @menuCollapsedNotified,
    @name,
    @password,
    @showClickOnceWarning,
    @timeZoneId,
    @zoneDisplayMode
)
ON DUPLICATE KEY UPDATE
    acceptedEULA=@acceptedEULA,
    activeFrom=@activeFrom,
    activeTo=@activeTo,
    changePassword=@changePassword,
    user_comment=@user_comment,
    dateFormat=@dateFormat,
    defaultGoogleMapStyle=@defaultGoogleMapStyle,
    defaultHereMapStyle=@defaultHereMapStyle,
    defaultMapEngine=@defaultMapEngine,
    defaultOpenStreetMapStyle=@defaultOpenStreetMapStyle,
    defaultPage=@defaultPage,
    designation=@designation,
    employeeNo=@employeeNo,
    firstDayOfWeek=@firstDayOfWeek,
    firstName=@firstName,
    fuelEconomyUnit=@fuelEconomyUnit,
    hosRuleSet=@hosRuleSet,
    isEULAAccepted=@isEULAAccepted,
    isLabsEnabled=@isLabsEnabled,
    isMetric=@isMetric,
    isNewsEnabled=@isNewsEnabled,
    isPersonalConveyanceEnabled=@isPersonalConveyanceEnabled,
    isYardMoveEnabled=@isYardMoveEnabled,
    user_language=@user_language,
    lastName=@lastName,
    menuCollapsedNotified=@menuCollapsedNotified,
    name=@name,
    password=@password,
    showClickOnceWarning=@showClickOnceWarning,
    timeZoneId=@timeZoneId,
    zoneDisplayMode=@zoneDisplayMode;";
                        userInsertCmd.Prepare();
                        userInsertCmd.Parameters.AddWithValue("@id", user.id);
                        userInsertCmd.Parameters.AddWithValue("@acceptedEULA", user.acceptedEULA);
                        userInsertCmd.Parameters.AddWithValue("@activeFrom", user.activeFrom);
                        userInsertCmd.Parameters.AddWithValue("@activeTo", user.activeTo);
                        userInsertCmd.Parameters.AddWithValue("@changePassword", user.changePassword);
                        userInsertCmd.Parameters.AddWithValue("@user_comment", user.user_comment);
                        userInsertCmd.Parameters.AddWithValue("@dateFormat", user.dateFormat);
                        userInsertCmd.Parameters.AddWithValue("@defaultGoogleMapStyle", user.defaultGoogleMapStyle);
                        userInsertCmd.Parameters.AddWithValue("@defaultHereMapStyle", user.defaultHereMapStyle);
                        userInsertCmd.Parameters.AddWithValue("@defaultMapEngine", user.defaultMapEngine);
                        userInsertCmd.Parameters.AddWithValue("@defaultOpenStreetMapStyle", user.defaultOpenStreetMapStyle);
                        userInsertCmd.Parameters.AddWithValue("@defaultPage", user.defaultPage);
                        userInsertCmd.Parameters.AddWithValue("@designation", user.designation);
                        userInsertCmd.Parameters.AddWithValue("@employeeNo", user.employeeNo);
                        userInsertCmd.Parameters.AddWithValue("@firstDayOfWeek", user.firstDayOfWeek);
                        userInsertCmd.Parameters.AddWithValue("@firstName", user.firstName);
                        userInsertCmd.Parameters.AddWithValue("@fuelEconomyUnit", user.fuelEconomyUnit);
                        userInsertCmd.Parameters.AddWithValue("@hosRuleSet", user.hosRuleSet);
                        userInsertCmd.Parameters.AddWithValue("@isEULAAccepted", user.isEULAAccepted);
                        userInsertCmd.Parameters.AddWithValue("@isLabsEnabled", user.isLabsEnabled);
                        userInsertCmd.Parameters.AddWithValue("@isMetric", user.isMetric);
                        userInsertCmd.Parameters.AddWithValue("@isNewsEnabled", user.isNewsEnabled);
                        userInsertCmd.Parameters.AddWithValue("@isPersonalConveyanceEnabled", user.isPersonalConveyanceEnabled);
                        userInsertCmd.Parameters.AddWithValue("@isYardMoveEnabled", user.isYardMoveEnabled);
                        userInsertCmd.Parameters.AddWithValue("@user_language", user.user_language);
                        userInsertCmd.Parameters.AddWithValue("@lastName", user.lastName);
                        userInsertCmd.Parameters.AddWithValue("@menuCollapsedNotified", user.menuCollapsedNotified);
                        userInsertCmd.Parameters.AddWithValue("@name", user.name);
                        userInsertCmd.Parameters.AddWithValue("@password", user.password);
                        userInsertCmd.Parameters.AddWithValue("@showClickOnceWarning", user.showClickOnceWarning);
                        userInsertCmd.Parameters.AddWithValue("@timeZoneId", user.timeZoneId);
                        userInsertCmd.Parameters.AddWithValue("@zoneDisplayMode", user.zoneDisplayMode);
                        userInsertCmd.ExecuteNonQuery();
                    }
                    log.write(Logger.LogLevel.Information, "Done updating Users.");
                }
                catch (MySqlException mse)
                {
                    log.write(Logger.LogLevel.Critical, string.Join("", new string[] { "MySQL problem, error number is: ", mse.Number.ToString(), ". Message is: ", mse.StackTrace }));
                    isSqlException = true;
                }
                finally
                {
                    mysqlconn.Close();
                }
            }
            else
            {
                log.write(Logger.LogLevel.Warning, "No User to update.");
            }
            return !isSqlException;
        }
*/
        private string formatDateMySQL(DateTime? date)
        {
            if (date != null)
            {
                return String.Format("{0}-{1}-{2} {3}:{4}:{5}", date.Value.Year, date.Value.Month, date.Value.Day, date.Value.Hour, date.Value.Minute, date.Value.Second);
            }
            return null;
        }
    }
}
