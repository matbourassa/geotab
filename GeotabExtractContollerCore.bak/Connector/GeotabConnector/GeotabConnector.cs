﻿using System;
using System.Collections.Generic;
using System.Text;

using Geotab.Checkmate;
using Geotab.Checkmate.ObjectModel;
using Geotab.Checkmate.ObjectModel.Engine;

using ExceptionEvent = Geotab.Checkmate.ObjectModel.Exceptions.ExceptionEvent;
using Rule = Geotab.Checkmate.ObjectModel.Exceptions.Rule;

using GeotabExtractLib.EntityFields;
using GeotabExtractLib.Utils;

namespace GeotabExtractContoller.Connector.GeotabConnector
{
    /// <summary>
    /// Manage connectivity to Geotab.
    /// </summary>
    class GeotabConnector
    {
        private static GeotabConnector instance;
        private API api;

        /// <summary>
        /// Consructor. Use getInstance() to get the singleton instance.
        /// </summary>
        /// <param name="geotab_server"></param>
        /// <param name="geotab_db"></param>
        /// <param name="geotab_username"></param>
        /// <param name="geotab_password"></param>
        private GeotabConnector(String geotab_server, String geotab_db, String geotab_username, String geotab_password)
        {
            Logger log = Logger.getInstance();

            log.write(Logger.LogLevel.Information, "Starting API session with Geotab servers...");
            api = new API(geotab_username, geotab_password, null, geotab_db, geotab_server);
            api.Authenticate();
            log.write(Logger.LogLevel.Information, "API Session authenticated!");
        }

        /// <summary>
        /// Returns the instance of the singleton class.
        /// </summary>
        /// <param name="geotab_server"></param>
        /// <param name="geotab_db"></param>
        /// <param name="geotab_username"></param>
        /// <param name="geotab_password"></param>
        /// <returns></returns>
        public static GeotabConnector getInstance(String geotab_server, String geotab_db, String geotab_username, String geotab_password)
        {
            if (instance == null)
            {
                instance = new GeotabConnector(geotab_server, geotab_db, geotab_username, geotab_password);
            }
            return instance;
        }
        /*
        /// <summary>
        /// Get all Controller from Geotab server/database.
        /// </summary>
        /// <returns>Returns the complete list as ControllerField.</returns>
        public List<ControllerField> GetControllers()
        {
            Logger log = Logger.getInstance();

            //Fetching Controller...
            log.write(Logger.LogLevel.Debug, String.Join("", new string[] { "Fetching Entity type \"Controller\"..." }));
            List<Controller> geotabControllerTypes = api.Call<List<Controller>>("Get", typeof(Controller));
            List<ControllerField> extractedControllerFields = new List<ControllerField>();

            //Controller fetched. Reading  each Controller and add it to the extractedControllerFields list.
            log.write(Logger.LogLevel.Debug, String.Join("", new string[] { "\"Controller\" fetched, Reading list..." }));
            foreach (Controller geotabControllerType in geotabControllerTypes)
            {
                /*Console.WriteLine(
                            "ID=" + geotabControllerType.Id.ToString() +
                            "\tSource.Id=" + geotabControllerType.Source.Id.ToString() +
                            "\tCode=" + geotabControllerType.Code.ToString() +
                            "\tName=" + geotabControllerType.Name +
                            "\tVersion=" + geotabControllerType.Version.ToString()
                    );*/
        /*
        if (geotabControllerType.Id != null)
        {
            if (geotabControllerType.Id.ToString() != "")
            {
                extractedControllerFields.Add(new ControllerField(
                    geotabControllerType.Id.ToString(),
                    geotabControllerType.Source.Id.ToString(),
                    geotabControllerType.Code,
                    geotabControllerType.Name,
                    geotabControllerType.Version
                    ));
            }
            else
            {
                log.write(Logger.LogLevel.Warning, "geotabController.Id is empty. We won't be able to export it. Skipping...");
            }
        }
        else
        {
            log.write(Logger.LogLevel.Warning, "geotabController.Id is null. We won't be able to export it. Skipping...");
        }
    }
    log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "Reading done. Number of Controller read = ", geotabControllerTypes.Count.ToString(), ". Number of valid Controller = ", extractedControllerFields.Count.ToString() }));
    //Returning the list of "valid" Controller found.
    return extractedControllerFields;
}*/

        /// <summary>
        /// Get all Devices from Geotab server/database.
        /// </summary>
        /// <returns>Returns the complete list as DeviceField.</returns>
        public List<DeviceField> GetDevices()
        {
            Logger log = Logger.getInstance();

            //Fetching devices...
            log.write(Logger.LogLevel.Debug, "Fetching devices...");
            List<Device> geotabdevices = api.Call<List<Device>>("Get", typeof(Device));

            //Devices fetched. Reading  each device and add it to the extractedDeviceFields list.
            log.write(Logger.LogLevel.Debug, "Devices fetched. Reading devices...");
            List<DeviceField> extractedDeviceFields = new List<DeviceField>();
            foreach (Device geotabdevice in geotabdevices)
            {
                /*Console.WriteLine(
                    "ID=" + geotabdevice.Id.ToString() +
                    "\tName=" + geotabdevice.Name +
                    "\tProductId=" + geotabdevice.Id +
                    "\tSerialNumber=" + geotabdevice.SerialNumber +
                    "\tHardwareId=" + geotabdevice.HardwareId +
                    "\tActiveFrom=" + geotabdevice.ActiveFrom.ToString() +
                    "\tActiveTo=" + geotabdevice.ActiveTo.ToString() +
                    "\tVersion=" + geotabdevice.Version.ToString() +
                    "\tComment=" + geotabdevice.Comment
                    );*/
                //Test the ID to make sure we dont have null or empty value.
                if (geotabdevice.Id != null)
                {
                    if (geotabdevice.Id.ToString() != "")
                    {
                        //we probably have a valid id. Add the device to the list.
                        extractedDeviceFields.Add(new DeviceField(
                            geotabdevice.Id.ToString(),
                            geotabdevice.Name,
                            geotabdevice.ProductId,
                            geotabdevice.SerialNumber,
                            geotabdevice.HardwareId,
                            geotabdevice.WorkTime.ToString(),
                            geotabdevice.ActiveFrom,
                            geotabdevice.ActiveTo,
                            geotabdevice.Version,
                            geotabdevice.Comment));
                    }
                    else
                    {
                        log.write(Logger.LogLevel.Warning, "geotabdevice.Id is empty. We won't be able to export it. Skipping...");
                    }
                }
                else
                {
                    log.write(Logger.LogLevel.Warning, "geotabdevice.Id is null. We won't be able to export it. Skipping...");
                }
            }
            log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "Reading done. Number of Devices read = ", extractedDeviceFields.Count.ToString(), ". Number of valid Device = ", extractedDeviceFields.Count.ToString() }));
            //Returning the list of "valid" Devices found.
            return extractedDeviceFields;
        }

        /// <summary>
        /// Fetch Trips based on DutyStatusLog provided.
        /// </summary>
        /// <param name="dutyStatusLogVersion"></param>
        /// <returns>Returns a list of DutyStatusLogs object.</returns>
        public List<DutyStatusLogField> GetDutyStatusLogs(ref long dutyStatusLogVersion)
        {
            Logger log = Logger.getInstance();
            List<DutyStatusLogField> dutystatuslogs = new List<DutyStatusLogField>();

            //Fetching LogRecords
            log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "Fetching DutyStatusLogs from version ", dutyStatusLogVersion.ToString(), "." }));
            FeedResult<DutyStatusLog> feedDutyStatusLogData = null;
            try
            {
                feedDutyStatusLogData = MakeFeedCall<DutyStatusLog>(dutyStatusLogVersion);
                log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "DutyStatusLogs fetched up to version = ", feedDutyStatusLogData.ToVersion.ToString(), ". Reading DutyStatusLog..." }));
            }
            catch (Exception e)
            {
                log.write(Logger.LogLevel.Critical, String.Join("", new String[] { "Error with MakeFeedCall fonction.", e.StackTrace }));
            }
            //Add the Trip to the Trip list
            if (feedDutyStatusLogData != null)
            {
                foreach (DutyStatusLog dutystatuslog in feedDutyStatusLogData.Data)
                {
                    if (dutystatuslog.Id != null)
                    {
                        if (dutystatuslog.Device != null)
                        {
                            if (dutystatuslog.Device.Id != null)
                            {
                                dutystatuslogs.Add(new DutyStatusLogField(
                                    dutystatuslog.Id.ToString(),
                                    dutystatuslog.Device != null ? dutystatuslog.Device.Id.ToString() : null,
                                    dutystatuslog.Driver != null ? dutystatuslog.Driver.Id.ToString() : null,
                                    dutystatuslog.ParentId != null ? dutystatuslog.ParentId.ToString() : null,
                                    dutystatuslog.State != null ? dutystatuslog.State.Value.ToString() : null,
                                    dutystatuslog.Status != null ? dutystatuslog.Status.Value.ToString() : null,
                                    dutystatuslog.DateTime,
                                    dutystatuslog.EditDateTime,
                                    dutystatuslog.Location != null ? dutystatuslog.Location.ToString() : null,
                                    dutystatuslog.Origin != null ? dutystatuslog.Origin.Value.ToString() : null,
                                    dutystatuslog.VerifyDateTime,
                                    dutystatuslog.Version));
                                /*Console.WriteLine(
                                    "ID= " + dutystatuslog.Id +
                                    "\tDeviceId= " + dutystatuslog.Device.Id +
                                    "\tDateTime= " + dutystatuslog.DateTime +
                                    "\tVersion= " + dutystatuslog.Version);

                                Console.ReadKey();*/
                            }
                            else
                            {
                                log.write(Logger.LogLevel.Warning, "dutystatuslog.Device.Id is null. We won't be able to export it. Skipping...");
                            }
                        }
                        else
                        {
                            log.write(Logger.LogLevel.Warning, "dutystatuslog.Device is null. We won't be able to export it. Skipping...");
                        }
                    }
                    else
                    {
                        log.write(Logger.LogLevel.Warning, "dutystatuslog.Id is null. We won't be able to export it. Skipping...");
                    }
                }
                log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "Reading done. Number of DutyStatusLogs read = ", feedDutyStatusLogData.Data.Count.ToString(), ". Number of DutyStatusLogs Extracted = ", dutystatuslogs.Count.ToString(), "." }));

                //ref Version update
                dutyStatusLogVersion = feedDutyStatusLogData.ToVersion == null ? 0 : feedDutyStatusLogData.ToVersion.Value;
            }
            //Returning the list
            return dutystatuslogs;
        }

        /// <summary>
        /// Get all Devices from Geotab server/database.
        /// </summary>
        /// <returns>Returns the complete list as DeviceField.</returns>
        public List<Go7Field> GetGo7()
        {
            Logger log = Logger.getInstance();

            //Fetching Go7 devices...
            log.write(Logger.LogLevel.Debug, "Fetching Go7 devices...");
            //To get list of Go7 devices, we must get list of Devices and then cast into Go7 (or wichever device type we want)
            //Ref: https://helpdesk.geotab.com/hc/en-us/community/posts/255597486-API-call-example-for-GO7-Entity
            List<Go7> geotabGo7devices = api.Call<List<Go7>>("Get", typeof(Device));

            //Go7 devices fetched. Reading  each Go7 device and add it to the extractedGo7Fields list.
            log.write(Logger.LogLevel.Debug, "Go7 devices fetched. Reading Go7 devices...");
            List<Go7Field> extractedGo7Fields = new List<Go7Field>();
            foreach (Go7 geotabGo7device in geotabGo7devices)
            {
                //Test the ID to make sure we dont have null or empty value.
                if (geotabGo7device.Id != null)
                {
                    if (geotabGo7device.Id.ToString() != "")
                    {
                        //we probably have a valid id. Add the Go7 device to the list.
                        extractedGo7Fields.Add(new Go7Field(
                            geotabGo7device.Id.ToString(),
                            geotabGo7device.Name,
                            geotabGo7device.ProductId,
                            geotabGo7device.SerialNumber,
                            geotabGo7device.HardwareId,
                            geotabGo7device.WorkTime.ToString(),
                            geotabGo7device.ActiveFrom,
                            geotabGo7device.ActiveTo,
                            geotabGo7device.Comment,
                            geotabGo7device.VehicleIdentificationNumber));
                    }
                    else
                    {
                        log.write(Logger.LogLevel.Warning, "geotabGo7device.Id is empty. We won't be able to export it. Skipping...");
                    }
                }
                else
                {
                    log.write(Logger.LogLevel.Warning, "geotabGo7device.Id is null. We won't be able to export it. Skipping...");
                }
            }
            log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "Reading done. Number of Go7 devices read = ", extractedGo7Fields.Count.ToString(), ". Number of valid Go7 device = ", extractedGo7Fields.Count.ToString() }));
            //Returning the list of "valid" Devices found.
            return extractedGo7Fields;
        }

        /// <summary>
        /// Get all Diagnostic from Geotab server/database.
        /// </summary>
        /// <returns>Returns the complete list as DiagnosticField.</returns>
        public List<DiagnosticField> GetDiagnostics()
        {
            Logger log = Logger.getInstance();

            //Fetching Diagnostic...
            log.write(Logger.LogLevel.Debug, String.Join("", new string[] { "Fetching Entity type \"Diagnostic\"..." }));
            List<Diagnostic> geotabDiagnostics = api.Call<List<Diagnostic>>("Get", typeof(Diagnostic));
            List<DiagnosticField> extractedDiagnosticFields = new List<DiagnosticField>();

            //Diagnostic fetched. Reading  each device and add it to the extractedDiagnosticFields list.
            log.write(Logger.LogLevel.Debug, String.Join("", new string[] { "\"Diagnostic\" fetched, Reading list..." }));
            foreach (Diagnostic geotabDiagnostic in geotabDiagnostics)
            {
                /*Console.WriteLine(
                    "\tID=" + geotabDiagnostic.Id.ToString() +
                    "\tController.Id=" + geotabDiagnostic.Controller.Id.ToString() +
                    "\tEngineType.Id=" + geotabDiagnostic.EngineType.Id.ToString() +
                    "\tSource.Id=" + geotabDiagnostic.Source.Id.ToString() +
                    "\tUnitOfMeasure.Ide=" + geotabDiagnostic.UnitOfMeasure.Id.ToString() +
                    "\tCode=" + geotabDiagnostic.Code.ToString() +
                    "\tDiagnosticType.Value=" + geotabDiagnostic.DiagnosticType.Value.ToString() +
                    "\tFaultResetMode.Value=" + geotabDiagnostic.FaultResetMode.Value.ToString() +
                    "\tName=" + geotabDiagnostic.Name +
                    "\tVersion=" + geotabDiagnostic.Version.ToString()
                    );*/
                if (geotabDiagnostic.Id != null)
                {
                    if (geotabDiagnostic.Id.ToString() != "")
                    {
                        extractedDiagnosticFields.Add(new DiagnosticField(
                            geotabDiagnostic.Id.ToString(),
                            geotabDiagnostic.Controller.Id.ToString(),
                            geotabDiagnostic.EngineType.Id.ToString(),
                            geotabDiagnostic.Source.Id.ToString(),
                            geotabDiagnostic.UnitOfMeasure.Id.ToString(),
                            geotabDiagnostic.Code,
                            geotabDiagnostic.DiagnosticType.Value.ToString(),
                            geotabDiagnostic.FaultResetMode.Value.ToString(),
                            geotabDiagnostic.Name,
                            geotabDiagnostic.Version
                            ));
                    }
                    else
                    {
                        log.write(Logger.LogLevel.Warning, "geotabDiagnostic.Id is empty. We won't be able to export it. Skipping...");
                    }
                }
                else
                {
                    log.write(Logger.LogLevel.Warning, "geotabDiagnostic.Id is null. We won't be able to export it. Skipping...");
                }
            }
            log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "Reading done. Number of Diagnostic read = ", geotabDiagnostics.Count.ToString(), ". Number of valid Diagnostic = ", extractedDiagnosticFields.Count.ToString() }));
            //Returning the list of "valid" EngineType found.
            return extractedDiagnosticFields;
        }

        /// <summary>
        /// Get all EngineType from Geotab server/database.
        /// </summary>
        /// <returns>Returns the complete list as EngineTypeField.</returns>
        public List<EngineTypeField> GetEngineTypes()
        {
            Logger log = Logger.getInstance();

            //Fetching EngineType...
            log.write(Logger.LogLevel.Debug, String.Join("", new string[] { "Fetching Entity type \"EngineType\"..." }));
            List<EngineType> geotabEngineTypes = api.Call<List<EngineType>>("Get", typeof(EngineType));
            List<EngineTypeField> extractedEngineTypeFields = new List<EngineTypeField>();

            //EngineType fetched. Reading  each device and add it to the geotabextracteddevices list.
            log.write(Logger.LogLevel.Debug, String.Join("", new string[] { "\"EngineType\" fetched, Reading list..." }));
            foreach (EngineType geotabEngineType in geotabEngineTypes)
            {
                /*Console.WriteLine(
                    "ID=" + geotabEngineType.Id.ToString() +
                    "\tName=" + geotabEngineType.Name
                    );*/
                if (geotabEngineType.Id != null)
                {
                    if (geotabEngineType.Id.ToString() != "")
                    {
                        extractedEngineTypeFields.Add(new EngineTypeField(
                            geotabEngineType.Id.ToString(),
                            geotabEngineType.Name
                            ));
                    }
                    else
                    {
                        log.write(Logger.LogLevel.Warning, "geotabEngineType.Id is empty. We won't be able to export it. Skipping...");
                    }
                }
                else
                {
                    log.write(Logger.LogLevel.Warning, "geotabEngineType.Id is null. We won't be able to export it. Skipping...");
                }
            }
            log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "Reading done. Number of EngineType read = ", geotabEngineTypes.Count.ToString(), ". Number of valid EngineType = ", extractedEngineTypeFields.Count.ToString() }));
            //Returning the list of "valid" EngineType found.
            return extractedEngineTypeFields;
        }

        /// <summary>
        /// Fetch ExceptionEvent based on ExceptionEventVersion provided.
        /// </summary>
        /// <param name="exceptionEventVersion"></param>
        /// <returns>Returns a list of ExceptionEventFields object.</returns>
        public List<ExceptionEventField> GetExceptionEvent(ref long exceptionEventVersion)
        {
            Logger log = Logger.getInstance();
            List<ExceptionEventField> exceptionEvents = new List<ExceptionEventField>();

            //Fetching ExceptionEvent
            log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "Fetching ExceptionEvents from version ", exceptionEventVersion.ToString(), "." }));
            FeedResult<ExceptionEvent> feedExceptionEventData = null;
            try
            {
                feedExceptionEventData = MakeFeedCall<ExceptionEvent>(exceptionEventVersion);
                log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "ExceptionEvents fetched up to version = ", feedExceptionEventData.ToVersion.ToString(), ". Reading ExceptionEvents..." }));
            }
            catch (Exception e)
            {
                log.write(Logger.LogLevel.Critical, String.Join("", new String[] { "Error with MakeFeedCall fonction.", e.StackTrace }));
            }

            //Add the ExceptionEvent to the ExceptionEvent list
            if (feedExceptionEventData != null)
            {
                foreach (ExceptionEvent ee in feedExceptionEventData.Data)
                {
                    if (ee.Id != null)
                    {
                        if (ee.Device != null)
                        {
                            if (ee.Device.Id != null)
                            {
                                exceptionEvents.Add(new ExceptionEventField(
                                    ee.Id.ToString(),
                                    ee.Device.Id.ToString(),
                                    ee.Driver.Id.ToString(),
                                    ee.Rule.Id.ToString(),
                                    ee.ActiveFrom,
                                    ee.ActiveTo,
                                    ee.Distance,
                                    ee.Duration,
                                    ee.Version));
                                /*Console.WriteLine(
                                    "ID= " + ee.Id +
                                    "\tDevice.Id.ToString()= " + ee.Device.Id.ToString() +
                                    "\tDriver.Id.ToString()= " + ee.Driver.Id.ToString() +
                                    "\tRule.Id.ToString()= " + ee.Rule.Id.ToString() +
                                    "\tActiveFrom= " + ee.ActiveFrom +
                                    "\tActiveTo= " + ee.ActiveTo +
                                    "\tDistance= " + ee.Distance +
                                    "\tDuration= " + ee.Duration +
                                    "\tVersion= " + ee.Version);

                                Console.ReadKey();*/
                            }
                            else
                            {
                                log.write(Logger.LogLevel.Warning, "feedLExceptionEvent.Device.Id is null. We won't be able to export it. Skipping...");
                            }
                        }
                        else
                        {
                            log.write(Logger.LogLevel.Warning, "feedExceptionEvent.Device is null. We won't be able to export it. Skipping...");
                        }
                    }
                    else
                    {
                        log.write(Logger.LogLevel.Warning, "feedExceptionEvent.Id is null. We won't be able to export it. Skipping...");
                    }
                }
                log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "Reading done. Number of ExceptionEvents read = ", feedExceptionEventData.Data.Count.ToString(), ". Number of ExceptionEvents Extracted = ", exceptionEvents.Count.ToString(), "." }));

                //ref Version update
                exceptionEventVersion = feedExceptionEventData.ToVersion == null ? 0 : feedExceptionEventData.ToVersion.Value;
            }
            //Returning the list
            return exceptionEvents;
        }

        /// <summary>
        /// Get all FailureMode from Geotab server/database.
        /// </summary>
        /// <returns>Returns the complete list as FailureModeField.</returns>
        public List<FailureModeField> GetFailureModes()
        {
            Logger log = Logger.getInstance();

            //Fetching FailureMode...
            log.write(Logger.LogLevel.Debug, String.Join("", new string[] { "Fetching Entity type \"FailureMode\"..." }));
            List<FailureMode> geotabFailureModes = api.Call<List<FailureMode>>("Get", typeof(FailureMode));
            List<FailureModeField> extractedFailureModeFields = new List<FailureModeField>();

            //FailureMode list fetched. Reading  each FailureMode and add it to the extractedFailureModeFields list.
            log.write(Logger.LogLevel.Debug, String.Join("", new string[] { "\"FailureMode\" fetched, Reading list..." }));
            foreach (FailureMode geotabFailureMode in geotabFailureModes)
            {
                /*Console.WriteLine(
                    "ID=" + geotabEngineType.Id.ToString() +
                    "\tName=" + geotabEngineType.Name
                    );*/
                if (geotabFailureMode.Id != null)
                {
                    if (geotabFailureMode.Id.ToString() != "")
                    {
                        extractedFailureModeFields.Add(new FailureModeField(
                            geotabFailureMode.Id.ToString(),
                            geotabFailureMode.Source != null ? geotabFailureMode.Source.Id.ToString() : null,
                            geotabFailureMode.Code,
                            geotabFailureMode.Name
                            ));
                    }
                    else
                    {
                        log.write(Logger.LogLevel.Warning, "geotabFailureMode.Id is empty. We won't be able to export it. Skipping...");
                    }
                }
                else
                {
                    log.write(Logger.LogLevel.Warning, "geotabFailureMode.Id is null. We won't be able to export it. Skipping...");
                }
            }
            log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "Reading done. Number of FailureMode read = ", geotabFailureModes.Count.ToString(), ". Number of valid FailureMode = ", extractedFailureModeFields.Count.ToString() }));
            //Returning the list of "valid" FailureMode found.
            return extractedFailureModeFields;
        }


        /// <summary>
        /// Fetch faultdata based on faultdataVersion provided.
        /// </summary>
        /// <param name="faultdataVersion"></param>
        /// <returns>Returns a list of StatusDataFields object.</returns>
        public List<FaultDataField> GetFaultData(ref long faultdataVersion)
        {
            Logger log = Logger.getInstance();
            List<FaultDataField> faultstatusdata = new List<FaultDataField>();

            //Fetching FaultData
            log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "Fetching FaultData from version ", faultdataVersion.ToString(), "." }));
            FeedResult<FaultData> feedFaultDataData = null;
            try
            {
                feedFaultDataData = MakeFeedCall<FaultData>(faultdataVersion);
                log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "FaultData fetched up to version = ", feedFaultDataData.ToVersion.ToString(), ". Reading FaultData..." }));
            }
            catch (Exception e)
            {
                log.write(Logger.LogLevel.Critical, String.Join("", new String[] { "Error with MakeFeedCall fonction.", e.StackTrace }));
            }

            //Add the logrecords to the logrecords list
            if (feedFaultDataData != null)
            {
                foreach (FaultData fd in feedFaultDataData.Data)
                {
                    if (fd.Id != null)
                    {
                        if (fd.Device != null)
                        {
                            if (fd.Device.Id != null)
                            {
                                /*Console.WriteLine(fd.Id.ToString());
                                Console.WriteLine(fd.Device.Id.ToString());
                                Console.WriteLine(fd.Diagnostic.Id.ToString());
                                Console.WriteLine(fd.Controller.Id.ToString());
                                Console.WriteLine(fd.FailureMode.Id.ToString());
                                Console.WriteLine(fd.FlashCode != null ? fd.FlashCode.Id.ToString() : null);
                                Console.WriteLine(fd.DismissUser.Id.ToString());
                                Console.WriteLine(fd.DateTime);
                                Console.WriteLine(fd.DismissDateTime);
                                Console.WriteLine(fd.Count);
                                Console.WriteLine(fd.FaultState.Value.ToString());
                                Console.WriteLine(fd.FaultLampState != null ? fd.FaultLampState.Value.ToString(): null);
                                Console.WriteLine(fd.AmberWarningLamp);
                                Console.WriteLine(fd.MalfunctionLamp);
                                Console.WriteLine(fd.ProtectWarningLamp);
                                Console.WriteLine(fd.RedStopLamp);*/
                                faultstatusdata.Add(new FaultDataField(
                                    fd.Id.ToString(),
                                    fd.Device.Id.ToString(),
                                    fd.Diagnostic.Id.ToString(),
                                    fd.Controller.Id.ToString(),
                                    fd.FailureMode.Id.ToString(),
                                    fd.FlashCode != null ? fd.FlashCode.Id.ToString() : null,
                                    fd.DismissUser.Id.ToString(),
                                    fd.DateTime,
                                    fd.DismissDateTime,
                                    fd.Count,
                                    fd.FaultState.Value.ToString(),
                                    fd.FaultLampState != null ? fd.FaultLampState.Value.ToString() : null,
                                    fd.AmberWarningLamp,
                                    fd.MalfunctionLamp,
                                    fd.ProtectWarningLamp,
                                    fd.RedStopLamp));
                            }
                            else
                            {
                                log.write(Logger.LogLevel.Warning, "feedFaultData.Device.Id is null. We won't be able to export it. Skipping...");
                            }
                        }
                        else
                        {
                            log.write(Logger.LogLevel.Warning, "feedFaultData.Device is null. We won't be able to export it. Skipping...");
                        }
                    }
                    else
                    {
                        log.write(Logger.LogLevel.Warning, "feedFaultData.Id is null. We won't be able to export it. Skipping...");
                    }
                }
                log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "Reading done. Number of FaultData read = ", feedFaultDataData.Data.Count.ToString(), ". Number of FaultData Extracted = ", faultstatusdata.Count.ToString(), "." }));

                //ref Version update
                faultdataVersion = feedFaultDataData.ToVersion == null ? 0 : feedFaultDataData.ToVersion.Value;
            }
            //Returning the list
            return faultstatusdata;
        }

        /// <summary>
        /// Get all FlashCode from Geotab server/database.
        /// </summary>
        /// <returns>Returns the complete list as FlashCodeField.</returns>
        public List<FlashCodeField> GetFlashCodes()
        {
            Logger log = Logger.getInstance();

            //Fetching FlashCode...
            log.write(Logger.LogLevel.Debug, String.Join("", new string[] { "Fetching Entity type \"FlashCode\"..." }));
            List<FlashCode> geotabFlashCodes = api.Call<List<FlashCode>>("Get", typeof(FlashCode));
            List<FlashCodeField> extractedFlashCodeFields = new List<FlashCodeField>();

            //FailureMode list fetched. Reading  each FailureMode and add it to the extractedFailureModeFields list.
            log.write(Logger.LogLevel.Debug, String.Join("", new string[] { "\"FlashCode\" fetched, Reading list..." }));
            foreach (FlashCode geotabFlashCode in geotabFlashCodes)
            {
                /*Console.WriteLine(
                    "ID=" + geotabFlashCode.Id.ToString() +
                    "\tName=" + geotabFlashCode.Name
                    );*/
                if (geotabFlashCode.Id != null)
                {
                    if (geotabFlashCode.Id.ToString() != "")
                    {
                        extractedFlashCodeFields.Add(new FlashCodeField(
                            geotabFlashCode.Id.ToString(),
                            geotabFlashCode.Diagnostic.Id.ToString(),
                            geotabFlashCode.FailureMode.Id.ToString(),
                            geotabFlashCode.Name,
                            geotabFlashCode.FlashCodeIndex,
                            geotabFlashCode.HelpUrl,
                            geotabFlashCode.PageReference,
                            geotabFlashCode.PriorityLevel
                            ));
                    }
                    else
                    {
                        log.write(Logger.LogLevel.Warning, "geotabFlashCode.Id is empty. We won't be able to export it. Skipping...");
                    }
                }
                else
                {
                    log.write(Logger.LogLevel.Warning, "geotabFlashCode.Id is null. We won't be able to export it. Skipping...");
                }
            }
            log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "Reading done. Number of FlashCodes read = ", geotabFlashCodes.Count.ToString(), ". Number of valid FlashCodes = ", extractedFlashCodeFields.Count.ToString() }));
            //Returning the list of "valid" FlashCode found.
            return extractedFlashCodeFields;
        }

        /// <summary>
        /// Fetch FuelTaxDetails based on fuelTaxDetailVersion provided.
        /// </summary>
        /// <param name="fuelTaxDetailVersion"></param>
        /// <returns>Returns a list of LogRecordsFields object.</returns>
        public List<FuelTaxDetailField> GetFuelTaxDetails(ref long fuelTaxDetailVersion)
        {
            Logger log = Logger.getInstance();
            List<FuelTaxDetailField> fuelTaxDetails = new List<FuelTaxDetailField>();

            //Fetching FuelTaxDetails
            log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "Fetching FuelTaxDetails from version ", fuelTaxDetailVersion.ToString(), "." }));
            FeedResult<FuelTaxDetail> feedFuelTaxDetailData = null;
            try
            {
                feedFuelTaxDetailData = MakeFeedCall<FuelTaxDetail>(fuelTaxDetailVersion);
                log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "FuelTaxDetails fetched up to version = ", feedFuelTaxDetailData.ToVersion.ToString(), ". Reading FuelTaxDetails..." }));
            }
            catch (Exception e)
            {
                log.write(Logger.LogLevel.Critical, String.Join("", new String[] { "Error with MakeFeedCall fonction.", e.StackTrace }));
            }

            //Add the logrecords to the logrecords list
            if (feedFuelTaxDetailData != null)
            {
                foreach (FuelTaxDetail ftd in feedFuelTaxDetailData.Data)
                {
                    if (ftd.Id != null)
                    {
                        if (ftd.Device != null)
                        {
                            if (ftd.Device.Id != null)
                            {
                                fuelTaxDetails.Add(new FuelTaxDetailField(
                                    ftd.Id.ToString(),
                                    ftd.Device.Id.ToString(),
                                    ftd.Driver.Id.ToString(),
                                    ftd.Authority,
                                    ftd.IsClusterOdometer,
                                    ftd.Jurisdiction,
                                    ftd.TollRoad,
                                    ftd.EnterGpsOdometer,
                                    ftd.EnterOdometer,
                                    ftd.EnterLatitude,
                                    ftd.EnterLongitude,
                                    ftd.EnterTime,
                                    ftd.ExitGpsOdometer,
                                    ftd.ExitOdometer,
                                    ftd.ExitLatitude,
                                    ftd.ExitLongitude,
                                    ftd.ExitTime));
                                Console.WriteLine(
                                    "ID= " + ftd.Id +
                                    "\tDeviceId= " + ftd.Device.Id +
                                    "\tDriverId= " + ftd.Driver.Id +
                                    "\tAuthority= " + ftd.Authority +
                                    "\tIsClusterOdometer= " + ftd.IsClusterOdometer +
                                    "\tJurisdiction= " + ftd.Jurisdiction +
                                    "\tTollRoad= " + ftd.TollRoad +
                                    "\tEnterGpsOdometer= " + ftd.EnterGpsOdometer +
                                    "\tEnterOdometer= " + ftd.EnterOdometer +
                                    "\tEnterLatitude= " + ftd.EnterLatitude +
                                    "\tEnterLongitude= " + ftd.EnterLongitude +
                                    "\tEnterTime= " + ftd.EnterTime +
                                    "\tExitGpsOdometer= " + ftd.ExitGpsOdometer +
                                    "\tExitOdometer= " + ftd.ExitOdometer +
                                    "\tExitLatitude= " + ftd.ExitLatitude +
                                    "\tExitLongitude= " + ftd.ExitLongitude +
                                    "\tExitTime= " + ftd.ExitTime);
                                Console.ReadKey();
                            }
                            else
                            {
                                log.write(Logger.LogLevel.Warning, "feedFuelTaxDetail.Device.Id is null. We won't be able to export it. Skipping...");
                            }
                        }
                        else
                        {
                            log.write(Logger.LogLevel.Warning, "feedFuelTaxDetail.Device is null. We won't be able to export it. Skipping...");
                        }
                    }
                    else
                    {
                        log.write(Logger.LogLevel.Warning, "feedFuelTaxDetail.Id is null. We won't be able to export it. Skipping...");
                    }
                }
                log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "Reading done. Number of FuelTaxDetails read = ", feedFuelTaxDetailData.Data.Count.ToString(), ". Number of FuelTaxDetails Extracted = ", fuelTaxDetails.Count.ToString(), "." }));

                //ref Version update
                fuelTaxDetailVersion = feedFuelTaxDetailData.ToVersion == null ? 0 : feedFuelTaxDetailData.ToVersion.Value;
            }
            //Returning the list
            return fuelTaxDetails;
        }

        /// <summary>
        /// Fetch logrecords based on logRecordsVersion provided.
        /// </summary>
        /// <param name="logRecordVersion"></param>
        /// <returns>Returns a list of LogRecordsFields object.</returns>
        public List<LogRecordField> GetLogRecords(ref long logRecordVersion)
        {
            Logger log = Logger.getInstance();
            List<LogRecordField> logrecords = new List<LogRecordField>();

            //Fetching LogRecords
            log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "Fetching LogRecords from version ", logRecordVersion.ToString(), "." }));
            FeedResult<LogRecord> feedLogRecordData = null;
            try
            {
                feedLogRecordData = MakeFeedCall<LogRecord>(logRecordVersion);
                log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "LogRecords fetched up to version = ", feedLogRecordData.ToVersion.ToString(), ". Reading LogRecords..." }));
            }
            catch (Exception e)
            {
                log.write(Logger.LogLevel.Critical, String.Join("", new String[] { "Error with MakeFeedCall fonction.", e.StackTrace }));
            }

            //Add the logrecords to the logrecords list
            if (feedLogRecordData != null)
            {
                foreach (LogRecord lr in feedLogRecordData.Data)
                {
                    if (lr.Id != null)
                    {
                        if (lr.Device != null)
                        {
                            if (lr.Device.Id != null)
                            {
                                logrecords.Add(new LogRecordField(
                                    lr.Id.ToString(),
                                    lr.Device.Id.ToString(),
                                    lr.DateTime,
                                    lr.Speed,
                                    lr.Latitude,
                                    lr.Longitude,
                                    lr.IsLastActive,
                                    lr.Version));
                                /*Console.WriteLine(
                                    "ID= " + lr.Id +
                                    "\tDeviceId= " + lr.Device.Id +
                                    "\tDateTime= " + lr.DateTime.ToString() +
                                    "\tSpeed= " + lr.Speed +
                                    "\tLatitude= " + lr.Latitude +
                                    "\tLongitude= " + lr.Longitude +
                                    "\tIsLastActive= " + lr.IsLastActive +
                                    "\tVersion= " + lr.Version);

                                //Console.ReadKey();*/
                            }
                            else
                            {
                                log.write(Logger.LogLevel.Warning, "feedLogRecord.Device.Id is null. We won't be able to export it. Skipping...");
                            }
                        }
                        else
                        {
                            log.write(Logger.LogLevel.Warning, "feedLogRecord.Device is null. We won't be able to export it. Skipping...");
                        }
                    }
                    else
                    {
                        log.write(Logger.LogLevel.Warning, "feedLogRecord.Id is null. We won't be able to export it. Skipping...");
                    }
                }
                log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "Reading done. Number of LogRecords read = ", feedLogRecordData.Data.Count.ToString(), ". Number of LogRecords Extracted = ", logrecords.Count.ToString(), "." }));

                //ref Version update
                logRecordVersion = feedLogRecordData.ToVersion == null ? 0 : feedLogRecordData.ToVersion.Value;
            }
            //Returning the list
            return logrecords;
        }

        /// <summary>
        /// Get all Rules from Geotab server/database.
        /// </summary>
        /// <returns>Returns the complete list as RuleField.</returns>
        public List<RuleField> GetRules()
        {
            Logger log = Logger.getInstance();

            //Fetching Rules...
            log.write(Logger.LogLevel.Debug, "Fetching Rules...");
            List<Rule> geotabrules = api.Call<List<Rule>>("Get", typeof(Rule));

            //Rules fetched. Reading  each Rule and add it to the extractedRuleFields list.
            log.write(Logger.LogLevel.Debug, "Rules fetched. Reading Rules...");
            List<RuleField> extractedRuleFields = new List<RuleField>();
            foreach (Rule geotabrule in geotabrules)
            {
                //Test the ID to make sure we dont have null or empty value.
                if (geotabrule.Id != null)
                {
                    if (geotabrule.Id.ToString() != "")
                    {
                        //we probably have a valid id. Add the device to the list.
                        extractedRuleFields.Add(new RuleField(
                            geotabrule.Id.ToString(),
                            geotabrule.Condition.Id.ToString(),
                            geotabrule.ActiveFrom,
                            geotabrule.ActiveTo,
                            geotabrule.BaseType.Value.ToString(),
                            geotabrule.Color.ToString(),
                            geotabrule.Comment,
                            //geotabrule.Groups,
                            geotabrule.Name,
                            geotabrule.Version));
                    }
                    else
                    {
                        log.write(Logger.LogLevel.Warning, "geotabrule.Id is empty. We won't be able to export it. Skipping...");
                    }
                }
                else
                {
                    log.write(Logger.LogLevel.Warning, "geotabrule.Id is null. We won't be able to export it. Skipping...");
                }
            }
            log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "Reading done. Number of Rules read = ", extractedRuleFields.Count.ToString(), ". Number of valid Rules = ", extractedRuleFields.Count.ToString() }));
            //Returning the list of "valid" Rules found.
            return extractedRuleFields;
        }

        /// <summary>
        /// Fetch Trips based on tripVersion provided.
        /// </summary>
        /// <param name="tripVersion"></param>
        /// <returns>Returns a list of TripFields object.</returns>
        public List<TripField> GetTrips(ref long tripVersion)
        {
            Logger log = Logger.getInstance();
            List<TripField> trips = new List<TripField>();

            //Fetching LogRecords
            log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "Fetching Trips from version ", tripVersion.ToString(), "." }));
            FeedResult<Trip> feedTripData = null;
            try
            {
                feedTripData = MakeFeedCall<Trip>(tripVersion);
                log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "Trips fetched up to version = ", feedTripData.ToVersion.ToString(), ". Reading Trips..." }));
            }
            catch (Exception e)
            {
                log.write(Logger.LogLevel.Critical, String.Join("", new String[] { "Error with MakeFeedCall fonction.", e.StackTrace }));
            }

            //Add the Trip to the Trip list
            if (feedTripData != null)
            {
                foreach (Trip trip in feedTripData.Data)
                {
                    if (trip.Id != null)
                    {
                        if (trip.Device != null)
                        {
                            if (trip.Device.Id != null)
                            {
                                trips.Add(new TripField(
                                    trip.Id.ToString(),
                                    trip.Device.Id.ToString(),
                                    trip.Driver.Id.ToString(),
                                    trip.DateTime,
                                    trip.AfterHoursDistance,
                                    trip.AfterHoursDrivingDuration,
                                    trip.AfterHoursEnd,
                                    trip.AfterHoursStart,
                                    trip.AfterHoursStopDuration,
                                    trip.AverageSpeed,
                                    trip.IdlingDuration,
                                    trip.MaximumSpeed,
                                    trip.NextTripStart,
                                    trip.SpeedRange1,
                                    trip.SpeedRange1Duration,
                                    trip.SpeedRange2,
                                    trip.SpeedRange2Duration,
                                    trip.SpeedRange3,
                                    trip.SpeedRange3Duration,
                                    trip.Start,
                                    trip.Stop,
                                    trip.StopDuration,
                                    trip.StopPoint.ToString(),
                                    trip.WorkDistance,
                                    trip.WorkDrivingDuration,
                                    trip.WorkStopDuration,
                                    trip.Version));
                                /*Console.WriteLine(
                                    "ID= " + trip.Id +
                                    "\tDeviceId= " + trip.Device.Id +
                                    "\tDateTime= " + trip.DateTime +
                                    "\tAfterHoursDistance= " + trip.AfterHoursDistance +
                                    "\tAfterHoursDrivingDuration= " + trip.AfterHoursDrivingDuration +
                                    "\tAfterHoursEnd= " + trip.AfterHoursEnd +
                                    "\tAfterHoursStart= " + trip.AfterHoursStart +
                                    "\tAfterHoursStopDuration= " + trip.AfterHoursStopDuration +
                                    "\tAverageSpeed= " + trip.AverageSpeed +
                                    "\tIdlingDuration= " + trip.IdlingDuration +
                                    "\tMaximumSpeed= " + trip.MaximumSpeed +
                                    "\tNextTripStart= " + trip.NextTripStart +
                                    "\tSpeedRange1= " + trip.SpeedRange1 +
                                    "\tSpeedRange1Duration= " + trip.SpeedRange1Duration +
                                    "\tSpeedRange2= " + trip.SpeedRange2 +
                                    "\tSpeedRange2Duration= " + trip.SpeedRange2Duration +
                                    "\tSpeedRange3= " + trip.SpeedRange3 +
                                    "\tSpeedRange3Duration= " + trip.SpeedRange3Duration +
                                    "\tStart= " + trip.Start +
                                    "\tStop= " + trip.Stop +
                                    "\tStopDuration= " + trip.StopDuration +
                                    "\tStopPoint.ToString()= " + trip.StopPoint.ToString() +
                                    "\tWorkDistance= " + trip.WorkDistance +
                                    "\tWorkDrivingDuration= " + trip.WorkDrivingDuration +
                                    "\tWorkStopDuration= " + trip.WorkStopDuration +
                                    "\tVersion= " + trip.Version);

                                Console.ReadKey();*/
                            }
                            else
                            {
                                log.write(Logger.LogLevel.Warning, "trip.Device.Id is null. We won't be able to export it. Skipping...");
                            }
                        }
                        else
                        {
                            log.write(Logger.LogLevel.Warning, "trip.Device is null. We won't be able to export it. Skipping...");
                        }
                    }
                    else
                    {
                        log.write(Logger.LogLevel.Warning, "trip.Id is null. We won't be able to export it. Skipping...");
                    }
                }
                log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "Reading done. Number of Trips read = ", feedTripData.Data.Count.ToString(), ". Number of Trips Extracted = ", trips.Count.ToString(), "." }));

                //ref Version update
                tripVersion = feedTripData.ToVersion == null ? 0 : feedTripData.ToVersion.Value;
            }
            //Returning the list
            return trips;
        }

        /// <summary>
        /// Get all UnitOfMeasure from Geotab server/database.
        /// </summary>
        /// <returns>Returns the complete list as UnitOfMeasureField.</returns>
        public List<UnitOfMeasureField> GetUnitOfMeasures()
        {
            Logger log = Logger.getInstance();

            //Fetching UnitOfMeasures...
            log.write(Logger.LogLevel.Debug, String.Join("", new string[] { "Fetching Entity type \"UnitOfMeasure\"..." }));
            List<UnitOfMeasure> geotabUnitOfMeasures = api.Call<List<UnitOfMeasure>>("Get", typeof(UnitOfMeasure));
            List<UnitOfMeasureField> extractedUnitOfMeasureFields = new List<UnitOfMeasureField>();

            //UnitOfMeasure fetched. Reading  each device and add it to the extractedUnitOfMeasureFields list.
            log.write(Logger.LogLevel.Debug, String.Join("", new string[] { "\"UnitOfMeasure\" fetched, Reading list..." }));
            foreach (UnitOfMeasure geotabUnitOfMeasure in geotabUnitOfMeasures)
            {
                /*Console.WriteLine(
                    "ID=" + geotabUnitOfMeasure.Id.ToString() +
                    "\tName=" + geotabUnitOfMeasure.Name
                    );*/
                if (geotabUnitOfMeasure.Id != null)
                {
                    if (geotabUnitOfMeasure.Id.ToString() != "")
                    {
                        extractedUnitOfMeasureFields.Add(new UnitOfMeasureField(
                            geotabUnitOfMeasure.Id.ToString(),
                            geotabUnitOfMeasure.Name
                            ));
                    }
                    else
                    {
                        log.write(Logger.LogLevel.Warning, "geotabUnitOfMeasure.Id is empty. We won't be able to export it. Skipping...");
                    }
                }
                else
                {
                    log.write(Logger.LogLevel.Warning, "geotabUnitOfMeasure.Id is null. We won't be able to export it. Skipping...");
                }
            }
            log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "Reading done. Number of Devices read = ", geotabUnitOfMeasures.Count.ToString(), ". Number of valid Device = ", extractedUnitOfMeasureFields.Count.ToString() }));
            //Returning the list of "valid" UnitOfMeasure found.
            return extractedUnitOfMeasureFields;
        }

        /// <summary>
        /// Get all EngineType from Geotab server/database.
        /// </summary>
        /// <returns>Returns the complete list as EngineTypeField.</returns>
        public List<UserField> GetUsers()
        {
            Logger log = Logger.getInstance();

            //Fetching Users...
            log.write(Logger.LogLevel.Debug, String.Join("", new string[] { "Fetching Entity type \"User\"..." }));
            List<User> geotabUsers = api.Call<List<User>>("Get", typeof(User));
            List<UserField> extractedUserFields = new List<UserField>();

            //Users fetched. Reading  each device and add it to the extractedUserFields list.
            log.write(Logger.LogLevel.Debug, String.Join("", new string[] { "\"User\" fetched, Reading list..." }));
            foreach (User geotabUser in geotabUsers)
            {
                /*Console.WriteLine(
                    "ID=" + geotabUser.Id.ToString() +
                    "\tName=" + geotabUser.Name
                    );*/
                if (geotabUser.Id != null)
                {
                    if (geotabUser.Id.ToString() != "")
                    {
                        extractedUserFields.Add(new UserField(
                            geotabUser.Id.ToString(),
                            geotabUser.AcceptedEULA,
                            geotabUser.ActiveDashboardReports,
                            geotabUser.ActiveFrom,
                            geotabUser.ActiveTo,
                            geotabUser.AvailableDashboardReports,
                            geotabUser.CannedResponseOptions,
                            geotabUser.ChangePassword,
                            geotabUser.Comment,
                            //geotabUser.CompanyGroups,
                            geotabUser.DateFormat,
                            geotabUser.DefaultGoogleMapStyle.Value.ToString(),
                            geotabUser.DefaultHereMapStyle.Value.ToString(),
                            geotabUser.DefaultMapEngine,
                            geotabUser.DefaultOpenStreetMapStyle.Value.ToString(),
                            geotabUser.DefaultPage,
                            geotabUser.Designation,
                            geotabUser.EmployeeNo,
                            geotabUser.FirstDayOfWeek.Value.ToString(),
                            geotabUser.FirstName,
                            geotabUser.FuelEconomyUnit.Value.ToString(),
                            geotabUser.HosRuleSet.Value.ToString(),
                            geotabUser.IsEULAAccepted,
                            geotabUser.IsLabsEnabled,
                            geotabUser.IsMetric,
                            geotabUser.IsNewsEnabled,
                            geotabUser.IsPersonalConveyanceEnabled,
                            geotabUser.IsYardMoveEnabled,
                            geotabUser.Language.ToString(),
                            geotabUser.LastName,
                            //geotabUser.MapViews,
                            geotabUser.MenuCollapsedNotified,
                            geotabUser.Name,
                            geotabUser.Password,
                            //geotabUser.PrivateUserGroups,
                            //geotabUser.ReportGroups,
                            //geotabUser.SecurityGroups,
                            geotabUser.ShowClickOnceWarning,
                            geotabUser.TimeZoneId,
                            geotabUser.ZoneDisplayMode.Value.ToString()
                            ));
                    }
                    else
                    {
                        log.write(Logger.LogLevel.Warning, "geotabUser.Id is empty. We won't be able to export it. Skipping...");
                    }
                }
                else
                {
                    log.write(Logger.LogLevel.Warning, "geotabUser.Id is null. We won't be able to export it. Skipping...");
                }
            }
            log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "Reading done. Number of User read = ", geotabUsers.Count.ToString(), ". Number of valid User = ", extractedUserFields.Count.ToString() }));
            //Returning the list of "valid" Users found.
            return extractedUserFields;
        }

        /// <summary>
        /// Get all Source from Geotab server/database.
        /// </summary>
        /// <returns>Returns the complete list as SourceField.</returns>
        public List<SourceField> GetSources()
        {
            Logger log = Logger.getInstance();

            //Fetching Sources...
            log.write(Logger.LogLevel.Debug, String.Join("", new string[] { "Fetching Entity type \"Source\"..." }));
            List<Source> geotabSources = api.Call<List<Source>>("Get", typeof(Source));
            List<SourceField> extractedSourceFields = new List<SourceField>();

            //Source fetched. Reading  each device and add it to the extractedSourceFields list.
            log.write(Logger.LogLevel.Debug, String.Join("", new string[] { "\"Source\" fetched, Reading list..." }));
            foreach (Source geotabSource in geotabSources)
            {
                /*Console.WriteLine(
                    "ID=" + geotabSource.Id.ToString() +
                    "\tName=" + geotabSource.Name
                    );*/
                if (geotabSource.Id != null)
                {
                    if (geotabSource.Id.ToString() != "")
                    {
                        extractedSourceFields.Add(new SourceField(
                            geotabSource.Id.ToString(),
                            geotabSource.Name
                            ));
                    }
                    else
                    {
                        log.write(Logger.LogLevel.Warning, "geotabSource.Id is empty. We won't be able to export it. Skipping...");
                    }
                }
                else
                {
                    log.write(Logger.LogLevel.Warning, "geotabSource.Id is null. We won't be able to export it. Skipping...");
                }
            }
            log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "Reading done. Number of Source read = ", geotabSources.Count.ToString(), ". Number of valid Source = ", extractedSourceFields.Count.ToString() }));
            //Returning the list of "valid" Source found.
            return extractedSourceFields;
        }


        /// <summary>
        /// Fetch statusdata based on statusdataVersion provided.
        /// </summary>
        /// <param name="statusdataVersion"></param>
        /// <returns>Returns a list of StatusDataFields object.</returns>
        public List<StatusDataField> GetStatusData(ref long statusdataVersion)
        {
            Logger log = Logger.getInstance();
            List<StatusDataField> statusdata = new List<StatusDataField>();

            //Fetching StatusData
            log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "Fetching StatusData from version ", statusdataVersion.ToString(), "." }));
            FeedResult<StatusData> feedStatusDataData = null;
            try
            {
                feedStatusDataData = MakeFeedCall<StatusData>(statusdataVersion);
                log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "StatusData fetched up to version = ", feedStatusDataData.ToVersion.ToString(), ". Reading StatusData..." }));
            }
            catch (Exception e)
            {
                log.write(Logger.LogLevel.Critical, String.Join("", new String[] { "Error with MakeFeedCall fonction.", e.StackTrace }));
            }

            //Add the logrecords to the logrecords list
            if (feedStatusDataData != null)
            {
                foreach (StatusData sd in feedStatusDataData.Data)
                {
                    if (sd.Id != null)
                    {
                        if (sd.Device != null)
                        {
                            if (sd.Device.Id != null)
                            {
                                statusdata.Add(new StatusDataField(
                                    sd.Id.ToString(),
                                    sd.Device.Id.ToString(),
                                    sd.Diagnostic.Id.ToString(),
                                    sd.DateTime,
                                    sd.Data,
                                    sd.Version));
                            }
                            else
                            {
                                log.write(Logger.LogLevel.Warning, "feedStatusData.Device.Id is null. We won't be able to export it. Skipping...");
                            }
                        }
                        else
                        {
                            log.write(Logger.LogLevel.Warning, "feedStatusData.Device is null. We won't be able to export it. Skipping...");
                        }
                    }
                    else
                    {
                        log.write(Logger.LogLevel.Warning, "feedStatusData.Id is null. We won't be able to export it. Skipping...");
                    }
                }
                log.write(Logger.LogLevel.Debug, String.Join("", new String[] { "Reading done. Number of StatusData read = ", feedStatusDataData.Data.Count.ToString(), ". Number of StatusData Extracted = ", statusdata.Count.ToString(), "." }));

                //ref Version update
                statusdataVersion = feedStatusDataData.ToVersion == null ? -1 : feedStatusDataData.ToVersion.Value;
            }
            //Returning the list
            return statusdata;
        }

        /// <summary>
        /// This fonction was copy-pasted from the Geotab .Net examples. Used to make the API call for all GetFeed fonction. Note: it seems there is a maximum of 50000 objects fetched in a single call.
        /// </summary>
        /// <typeparam name="T">Type of object to get using the GetFeed fonction.</typeparam>
        /// <param name="fromVersion">Version of object to get value from. Use 0 to get all values from the start. I think null value returns the latest version (need checking if want to use).</param>
        /// <returns>Returns the new fromVersion to use on the next call.</returns>
        FeedResult<T> MakeFeedCall<T>(long? fromVersion) where T : Entity => api.Call<FeedResult<T>>("GetFeed", typeof(T), new { fromVersion });
    }
}
