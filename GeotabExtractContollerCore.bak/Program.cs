﻿using System;

namespace GeotabExtractContoller
{
    class Program
    {
        static void Main(string[] args)
        {
            //Running Debug
            if (Environment.UserInteractive)
            {
                GeotabExtractWorker serviceDebug = new GeotabExtractWorker();

                serviceDebug.TestStartupAndStop(new string[2]);
            }
            //Running Release
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                    new GeotabExtractWorker()
                };
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
