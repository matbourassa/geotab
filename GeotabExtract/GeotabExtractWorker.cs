﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.ServiceProcess;
using System.Threading;

using GeotabExtractLib.EntityFields;
using GeotabExtractLib.Utils;
using GeotabConnectorCore.Connector.GeotabConnector;
using GeotabExtract.Connector.DB;
using System.Security.Authentication;
using System.Net.Http;

namespace GeotabExtract.Worker
{
    public partial class GeotabExtractWorker : ServiceBase
    {     

        System.Timers.Timer timer;

        private int timerIntervalDefault = 300; //Default to 5 minutes

        private int worker_min_logrecord_days_to_keep;
        private int worker_min_odometer_days_to_keep;
        private int worker_min_alertextract_days_to_keep;


        private int timerInterval = 10; //Default to 10 seconds

        public int worker_sleeptime;

        private string geotab_server;
        private string geotab_db;
        private string geotab_username;
        private string geotab_password;

        private string mysql_server;
        private string mysql_db;
        private string mysql_username;
        private string mysql_password;

        private string hfsql_connectionstring;

        private readonly Logger _logger;

        /// <summary>
        /// Constructor for the worker. Initializing most of the app's parameters.
        /// </summary>
        public GeotabExtractWorker()
        {

            InitializeComponent();

            //Geotab settings
            geotab_server = ConfigurationManager.AppSettings.Get("geotab_server");
            geotab_db = ConfigurationManager.AppSettings.Get("geotab_db");
            geotab_username = ConfigurationManager.AppSettings.Get("geotab_username");
            geotab_password = ConfigurationManager.AppSettings.Get("geotab_password");

            //MySQL settings
            mysql_server = ConfigurationManager.AppSettings.Get("mysql_server");
            mysql_db = ConfigurationManager.AppSettings.Get("mysql_db");
            mysql_username = ConfigurationManager.AppSettings.Get("mysql_username");
            mysql_password = ConfigurationManager.AppSettings.Get("mysql_password");

            //HFSQL settings
            hfsql_connectionstring = ConfigurationManager.AppSettings.Get("hfsql_connectionstring");

            _logger = Logger.getInstance();

        }

        /// <summary>
        /// Method used to enable debugging into the windows service without breaking Service pattern
        /// </summary>
        /// <param name="args"></param>
        internal void TestStartupAndStop(string[] args)
        {
            //Used for debugging
            this.OnStart(args);
            Console.ReadLine();
            this.OnStop();
        }



        /// <summary>
        /// Need to be updated. Used when starting as service. Should not include the worker's job in the start. Service is currently hanging.
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            _logger.write(Logger.LogLevel.Information, "#################################");
            _logger.write(Logger.LogLevel.Information, "#GeotabExtractor worker starting.");
            _logger.write(Logger.LogLevel.Information, "#################################");


            Int32.TryParse(ConfigurationManager.AppSettings["worker_sleeptime"], out timerInterval);
            if(timerInterval == 0)
            {
                timerInterval = timerIntervalDefault;
                _logger.write(Logger.LogLevel.Critical, string.Format("Configuration timer invalid, using default value of {0} seconds", timerIntervalDefault));
            }

            //Can log here with an if statement to warn if value couldn't be fetched.
            Int32.TryParse(ConfigurationManager.AppSettings.Get("worker_min_logrecord_days_to_keep"),out worker_min_logrecord_days_to_keep);
            Int32.TryParse(ConfigurationManager.AppSettings.Get("worker_min_odometer_days_to_keep"),out worker_min_odometer_days_to_keep);
            Int32.TryParse(ConfigurationManager.AppSettings.Get("worker_min_alertextract_days_to_keep"), out worker_min_alertextract_days_to_keep);


            // Set up a timer to trigger every X minutes.
            timer = new System.Timers.Timer();
            timer.AutoReset = false;
            
            //Debug testing
            if (Environment.UserInteractive)
                timer.Interval = 1000; //1 millisecond
            else
                timer.Interval = timerInterval * 1000; //X seconds, timer is in millis

            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.GetGeotabData);
            timer.Start();
            /*
            try
            {
                this.GetGeotabData(null, null);
            }
            catch (Exception e) { }*/


            _logger.write(Logger.LogLevel.Debug, string.Format(" Loop done. Sleeping for {0} seconds", (timer.Interval/1000).ToString()));

        }

        protected override void OnStop()
        {
            base.OnStop();
            _logger.write(Logger.LogLevel.Information, "GeotabExtractor exited peacefully.");
        }



        public void GetGeotabData(object sender, System.Timers.ElapsedEventArgs args)
        {
            _logger.write(Logger.LogLevel.Debug, "Loop starting.");

            //Init GeotabConnector
            GeotabConnector gc = null;
            try
            {
                gc = GeotabConnector.getInstance(geotab_server, geotab_db, geotab_username, geotab_password);
            }
            catch (AuthenticationException ex)
            {
                // Display Authentication Exceptions
                _logger.write(Logger.LogLevel.Critical, string.Format("Error while Creating the GeotabConnector, Authentication Exceptions: {0}", ex.Message));
            }
            catch (Exception ex)
            {
                _logger.write(Logger.LogLevel.Critical, string.Format("Error while Creating the GeotabConnector: {0}", ex.Message ));
                return;
            }

            //Init MySQLConnector
            MySQLConnector mc = null;
            try
            {
                mc = MySQLConnector.GetInstance(mysql_server, mysql_db, mysql_username, mysql_password, geotab_db);
            }
            catch (WrongDatabaseException wdex)
            {
                _logger.write(Logger.LogLevel.Critical, wdex.Message);
                return;
            }
            catch (Exception ex)
            {
                _logger.write(Logger.LogLevel.Critical, string.Format("Error while Creating the MySQLConnector: {0}", ex.Message ));
                return;
            }

            //Init MySQLConnector
            HFSQLConnector hc = null;
            try
            {
                hc = HFSQLConnector.GetInstance(hfsql_connectionstring);
            }
            catch (Exception ex)
            {
                _logger.write(Logger.LogLevel.Critical, string.Format("Error while Creating the HFSQLConnector: {0}", ex.Message));
                return;
            }

            if (gc != null && mc != null && hc != null)
            {
                try
                {/*
                    UpdateDevices(mc, gc);

                    UpdateGo7Devices(mc, gc);
                  
                    FetchDutyStatusLog(mc, gc, hc);
                    
                    FetchOdomoterValuesFromStatusData(mc, gc, hc);
                    
                    UpdateRules(mc, gc);

                    UpdateExceptionEvent(mc, gc);
                    
                    FetchLogRecords(mc, gc);*/

                    FetchFuelTaxDetails(mc, gc);

                    //#############################
                    //Update rules
                    //#############################
                    //UpdateRules(mc, gc);


                    //UpdateAlertExtract(mc, gc, hc);


                    //Update list in table
                    //mc.updateAlertExtractRuleNameAndGPS(lstUncompleteAlertExtract);

                    //#############################
                    //clear old data
                    //#############################
                    //mc.DeleteOdometersOlderThan(DateTime.Now.Subtract(TimeSpan.FromDays(worker_min_odometer_days_to_keep)));
                    //mc.DeleteAlertExtractOlderThan(DateTime.Now.Subtract(TimeSpan.FromDays(worker_min_alertextract_days_to_keep)));
                }
                catch (HttpRequestException hre) {
                    _logger.write(Logger.LogLevel.Critical, String.Format("HttpRequestException. Lost connectivity to geotab. Stopping the current process. Message: {0}", hre.Message));
                }
                catch (Exception e)
                {
                    _logger.write(Logger.LogLevel.Critical, String.Format("An unknown exception occured. Stopping the current process. Message: {0}", e.Message));
                }



            }
            else
            {
                _logger.write(Logger.LogLevel.Critical, "Error, one of the connector is Null.");
                return;
            }
            _logger.write(Logger.LogLevel.Debug, "End of data extraction loop.");
            timer.Start();
        }


        private void UpdateAlertExtract(MySQLConnector mc, GeotabConnector gc, HFSQLConnector hc)
        {
            List<AlertExtractField> lstUncompleteAlertExtract = mc.GetUncompleteAlertExtract();
            int processedAlertExtractCount = 0;

            foreach (AlertExtractField aef in lstUncompleteAlertExtract)
            {
                if (++processedAlertExtractCount % 500 == 0)
                {
                    _logger.write(Logger.LogLevel.Debug, String.Format("{0} Alarms processed.", processedAlertExtractCount));
                }
                /*DateTime tempDT = new DateTime(2017, 10, 17, 21, 0, 0);
                DateTime tempDT2 = new DateTime(2017, 10, 18, 0, 0, 0);
                if (aef.start_date_time < tempDT || aef.start_date_time > tempDT2)
                {
                    continue;
                }*/
                //Update VIN number
                if (aef.device_id != null && aef.vin == null)
                {
                    aef.vin = mc.GetVIN(aef.device_id);
                }

                //Update Rule Name
                if (aef.rule_name == null || aef.rule_name.Equals(""))
                {
                    aef.rule_name = mc.GetRuleName(aef.rule_id);
                }


                //Update Start GPS value
                if (aef.start_logrecord_id == null || aef.start_logrecord_id.Equals(""))
                {
                    if (aef.start_date_time != null)
                    {

                        _logger.write(9, string.Format("Setting GPS Starting value for AlertExtractField id={0}", aef.id.ToString()));
                        LogRecordField tmpSLR = mc.FindLogRecordFieldByDateTime(aef.device_id, aef.start_date_time.Value);
                        try
                        {
                            aef.setGPSStart(tmpSLR);
                        }
                        catch (Exception e)
                        {
                            _logger.write(Logger.LogLevel.Critical, string.Format("Unable to set the GPSStart value for AlertExtract id={0}. Error message is={1}", aef.id.ToString(), e.Message));
                        }
                    }
                    else
                    {
                        _logger.write(Logger.LogLevel.Critical, string.Format("AlertExtract of id=\"{0}\" has no start_date_time value", aef.id.ToString()));
                    }
                }
                //Update End GPS value
                if (aef.end_logrecord_id == null || aef.end_logrecord_id.Equals(""))
                {
                    if (aef.end_date_time != null)
                    {

                        _logger.write(9, string.Format("Setting GPS Ending value for AlertExtractField id={0}", aef.id.ToString()));
                        LogRecordField tmpELR = mc.FindLogRecordFieldByDateTime(aef.device_id, aef.end_date_time.Value);
                        try
                        {
                            aef.setGPSEnd(tmpELR);
                        }
                        catch (Exception e)
                        {
                            _logger.write(Logger.LogLevel.Critical, string.Format("Unable to set the GPSEnd value for AlertExtract id={0}. Error message is={1}", aef.id.ToString(), e.Message));
                        }
                    }
                    else
                    {
                        _logger.write(Logger.LogLevel.Warning, string.Format("AlertExtract of id=\"{0}\" has no end_date_time value", aef.id.ToString()));
                    }
                }
                _logger.write(9, string.Format("Inserting/Updating AlertExtract in HFSQL id={0}", aef.id.ToString()));
                hc.InsertAlertExtract(aef);
                _logger.write(9, string.Format("Updating entry in MySQL id={0}", aef.id.ToString()));
                mc.UpdateAlertExtractRuleNameAndGPS(aef);
                _logger.write(9, string.Format("Done with entry id={0}", aef.id.ToString()));
            }
            /*if (hc.InsertAlertExtract(lstUncompleteAlertExtract))
            {
                mc.UpdateAlertExtractRuleNameAndGPS(lstUncompleteAlertExtract);
            }*/

            //Delete old logrecords
            //mc.DeleteLogRecordsOlderThan(DateTime.Now.Subtract(TimeSpan.FromDays(worker_min_logrecord_days_to_keep)));
        }

        private void FetchDutyStatusLog(MySQLConnector mc, GeotabConnector gc, HFSQLConnector hc)
        {
            string tripTable = "dutystatuslog";
            long curFeedDutyStatusLogVersion = mc.GetFeedVersion(tripTable);
            long tmpDutyStatusLogVersion = -50000;
            while (tmpDutyStatusLogVersion != curFeedDutyStatusLogVersion && tmpDutyStatusLogVersion <= curFeedDutyStatusLogVersion - 49000 && curFeedDutyStatusLogVersion >= 0)
            {

                tmpDutyStatusLogVersion = curFeedDutyStatusLogVersion;
                //Fetch Feed object from Geotab
                List<DutyStatusLogField> gtObjectList = gc.GetDutyStatusLogs(ref curFeedDutyStatusLogVersion);
                
                if (mc.UpdateDutyStatusLogs(gtObjectList))
                {
                    _logger.write(Logger.LogLevel.Information, "Done updating MySQL DB.");
                    if (curFeedDutyStatusLogVersion > 0)
                    {
                        mc.UpdateFeedVersion(tripTable, curFeedDutyStatusLogVersion);
                    }
                    else
                    {
                        _logger.write(Logger.LogLevel.Critical, string.Format("\"{0}\" version is null. Will not update the DB with a null value. 0 should be used instead.", tripTable));
                    }
                    /*if (hc.InsertDutyStatusLogs(gtObjectList))
                    {
                        _logger.write(Logger.LogLevel.Information, "Done updating HFSQL DB. Updating feedversion...");
                        //update version only if no error detected while updating the list.
                        if (curFeedDutyStatusLogVersion > 0)
                        {
                            mc.UpdateFeedVersion(tripTable, curFeedDutyStatusLogVersion);
                        }
                        else
                        {
                            _logger.write(Logger.LogLevel.Critical, string.Format("\"{0}\" version is null. Will not update the DB with a null value. 0 should be used instead.", tripTable));
                        }
                    }*/
                }
            }
        }

        private void FetchFuelTaxDetails(MySQLConnector mc, GeotabConnector gc)
        {
            string fueltaxdetailTable = "fueltaxdetail";

            long curFeedFuelTaxDetailVersion = mc.GetFeedVersion(fueltaxdetailTable);
            long tmpFuelTaxDetailVersion = -50000;
            while (tmpFuelTaxDetailVersion != curFeedFuelTaxDetailVersion && tmpFuelTaxDetailVersion <= curFeedFuelTaxDetailVersion - 49000 && curFeedFuelTaxDetailVersion >= 0)
            {

                tmpFuelTaxDetailVersion = curFeedFuelTaxDetailVersion;
                //Fetch Feed object from Geotab
                List<FuelTaxDetailField> gtObjectList = gc.GetFuelTaxDetails(ref curFeedFuelTaxDetailVersion);
                /*
                if (mc.UpdateFuelTaxDetails(gtObjectList))
                {
                    _logger.write(Logger.LogLevel.Information, "Done updating MySQL DB. Updating feedversion...");
                    //update version only if no error detected while updating the list.
                    if (curFeedFuelTaxDetailVersion > 0)
                    {
                        mc.UpdateFeedVersion(fueltaxdetailTable, curFeedFuelTaxDetailVersion);
                    }
                    else
                    {
                        _logger.write(Logger.LogLevel.Critical, string.Format("\"{0}\" version is null. Will not update the DB with a null value. 0 should be used instead.", fueltaxdetailTable));
                    }
                }*/
            }

        }

        private void FetchLogRecords(MySQLConnector mc, GeotabConnector gc)
        {
            string logrecordTable = "logrecord";

            long curFeedLogRecordVersion = mc.GetFeedVersion(logrecordTable);
            long tmpLogRecordVersion = -50000;
            while (tmpLogRecordVersion != curFeedLogRecordVersion && tmpLogRecordVersion <= curFeedLogRecordVersion - 49000 && curFeedLogRecordVersion >= 0)
            {

                tmpLogRecordVersion = curFeedLogRecordVersion;
                //Fetch Feed object from Geotab
                List<LogRecordField> gtObjectList = gc.GetLogRecords(ref curFeedLogRecordVersion);

                if (mc.UpdateLogRecords(gtObjectList))
                {
                    _logger.write(Logger.LogLevel.Information, "Done updating MySQL DB. Updating feedversion...");
                    //update version only if no error detected while updating the list.
                    if (curFeedLogRecordVersion > 0)
                    {
                        mc.UpdateFeedVersion(logrecordTable, curFeedLogRecordVersion);
                    }
                    else
                    {
                        _logger.write(Logger.LogLevel.Critical, string.Format("\"{0}\" version is null. Will not update the DB with a null value. 0 should be used instead.", logrecordTable));
                    }
                }
            }

        }

        private void FetchTrip(MySQLConnector mc, GeotabConnector gc, HFSQLConnector hc) {
            string tripTable = "trip";
            long curFeedTripVersion = mc.GetFeedVersion(tripTable);
            long tmpTripVersion = -50000;
            while (tmpTripVersion != curFeedTripVersion && tmpTripVersion <= curFeedTripVersion - 49000 && curFeedTripVersion >= 0)
            {

                tmpTripVersion = curFeedTripVersion;
                //Fetch Feed object from Geotab
                List<TripField> gtObjectList = gc.GetTrips(ref curFeedTripVersion);
                
                if (mc.UpdateTrips(gtObjectList))
                {
                    _logger.write(Logger.LogLevel.Information, "Done updating MySQL DB.");
                   // if (hc.InsertTrips(gtObjectList))
                   // {
                   //    _logger.write(Logger.LogLevel.Information, "Done updating HFSQL DB. Updating feedversion...");
                        //update version only if no error detected while updating the list.
                        if (curFeedTripVersion > 0)
                        {
                            mc.UpdateFeedVersion(tripTable, curFeedTripVersion);
                        }
                        else
                        {
                            _logger.write(Logger.LogLevel.Critical, string.Format("\"{0}\" version is null. Will not update the DB with a null value. 0 should be used instead.", tripTable));
                        }
                    //}
                }
            }
        }

        private void UpdateDevices(MySQLConnector mc, GeotabConnector gc)
        {
            List<DeviceField> curDevices = gc.GetDevices();
            //If error, need to break
            //Update device in the DB.
            if (!mc.UpdateDevices(curDevices))
            {
                _logger.write(Logger.LogLevel.Critical, "Error while updating Devices. Exiting loop.");
                return;
            }
        }
        private void UpdateRules(MySQLConnector mc, GeotabConnector gc)
        {
            List<RuleField> curRules = gc.GetRules();

            //Update Rules in the DB.
            if (!mc.UpdateRules(curRules))
            {
                _logger.write(Logger.LogLevel.Warning, "A problem occured while updating the Rules.");
                return;
            }
        }

        private void UpdateGo7Devices(MySQLConnector mc, GeotabConnector gc)
        {
            List<Go7Field> curGo7Devices = gc.GetGo7();
            //If error, need to break
            //Update device in the DB.
            if (!mc.UpdateGo7(curGo7Devices))
            {
                _logger.write(Logger.LogLevel.Critical, "Error while updating Go7 devices. Exiting loop.");
                return;
            }
        }



        /// <summary>
        /// Analyse a StatusData list and return the Odometer values.
        /// </summary>
        /// <param name="lstStatusData">List of StatusData to extract odometer values from.</param>
        /// <returns>Returns a list of Odometer values</returns>
        private List<OdometerField> extractOdometer(List<StatusDataField> lstStatusData)
        {
            List<OdometerField> lstOdometer = new List<OdometerField>();
            foreach (StatusDataField sdf in lstStatusData)
            {
                if (sdf.diagnostic_id.Equals("DiagnosticRawOdometerId"))
                {
                    lstOdometer.Add(new OdometerField(0, sdf.id, sdf.device_id, null, sdf.date_time, sdf.data));
                }
            }
            return lstOdometer;
        }

        /// <summary>
        /// Create an AlertExtractField list from an ExceptionEvent list.
        /// </summary>
        /// <param name="lstExceptionEvent">List of ExceptionEvent</param>
        /// <returns>List of AlertExtract</returns>
        private List<AlertExtractField> createAlertExtractFromExceptionEvent(List<ExceptionEventField> lstExceptionEvent)
        {

            List<AlertExtractField> lstAlertExtract = new List<AlertExtractField>();
            foreach (ExceptionEventField eef in lstExceptionEvent)
            {
                lstAlertExtract.Add(new AlertExtractField(null, eef.device_id, null, eef.id, eef.rule_id, null, eef.distance, eef.activeFrom, eef.activeTo, null, null, null, null, null, null, null, null));
            }
            return lstAlertExtract;
        }



        /// <summary>
        /// Creates alert extract from Exceptions
        /// </summary>
        /// <param name="mc"></param>
        private void UpdateExceptionEvent(MySQLConnector mc, GeotabConnector gc)
        {
            string exceptionEventTable = "exceptionevent";
            long curFeedExceptionEventVersion = mc.GetFeedVersion(exceptionEventTable);
            long tmpExceptionEventVersion = -50000;
            while (tmpExceptionEventVersion != curFeedExceptionEventVersion && tmpExceptionEventVersion <= curFeedExceptionEventVersion - 49000 && curFeedExceptionEventVersion >= 0)
            {

                tmpExceptionEventVersion = curFeedExceptionEventVersion;
                //Fetch Feed object from Geotab
                List<ExceptionEventField> lstExceptionEvent = gc.GetExceptionEvent(ref curFeedExceptionEventVersion);


                //List<AlertExtractField> lstAlertExtract = createAlertExtractFromExceptionEvent(lstExceptionEvent);


                //if (mc.InsertAlertExtract(lstAlertExtract))
                if (mc.UpdateExceptionEvent(lstExceptionEvent))
                    {
                    _logger.write(Logger.LogLevel.Information, string.Join("", new string[] { "Done updating MySQL DB. Updating feedversion..." }));
                    //update version only if no error detected while updating the list.
                    if (curFeedExceptionEventVersion > 0)
                    {
                        mc.UpdateFeedVersion(exceptionEventTable, curFeedExceptionEventVersion);
                    }
                    else
                    {
                        _logger.write(Logger.LogLevel.Critical, string.Format("\"{0}\" version is null. Will not update the DB with a null value. 0 should be used instead.", exceptionEventTable));
                    }
                }
            }
        }

      
        private void FetchOdomoterValuesFromStatusData(MySQLConnector mc, GeotabConnector gc, HFSQLConnector hc)
        {
            string statusdataTable = "statusdata";
            long curFeedStatusDataVersion = mc.GetFeedVersion(statusdataTable);
            long tmpStatusDataVersion = -50000;
            while (tmpStatusDataVersion != curFeedStatusDataVersion && tmpStatusDataVersion <= curFeedStatusDataVersion - 49000 && curFeedStatusDataVersion >= 0)
            {
                tmpStatusDataVersion = curFeedStatusDataVersion;
                //Fetch Feed object from Geotab
                List<StatusDataField> gtObjectList = gc.GetStatusData(ref curFeedStatusDataVersion);

                //Extract Odometer values
                _logger.write(Logger.LogLevel.Information, "Extracting Odometers values from StatusData.");
                List<OdometerField> lstOdometer = extractOdometer(gtObjectList);

                _logger.write(Logger.LogLevel.Information, "Updating Odometer with VIN.");
                mc.UpdateOdometerVINs(ref lstOdometer);

                _logger.write(Logger.LogLevel.Information, String.Format("Done extracting {0} Odometer values.", lstOdometer.Count));

                //if (lstOdometer.Count>0 && mc.InsertOdometer(lstOdometer))
                //{
                    if (lstOdometer.Count > 0 && hc.InsertOdometer(lstOdometer))
                    {
                        _logger.write(Logger.LogLevel.Information, "Done updating MySQL DB. Updating feedversion...");
                        //update version only if no error detected while updating the list.
                        if (curFeedStatusDataVersion > 0)
                        {
                            mc.UpdateFeedVersion(statusdataTable, curFeedStatusDataVersion);
                        }
                        else
                        {
                            _logger.write(Logger.LogLevel.Critical, string.Format("\"{0}\" version is null. Will not update the DB with a null value. 0 should be used instead.", statusdataTable));
                        }
                    }
                //}                
            }
        }

    }
}

