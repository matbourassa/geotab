﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using GeotabExtractLib.Utils;
using GeotabExtractLib.EntityFields;

namespace GeotabExtract.Connector.DB
{
    class HFSQLConnector
    {
        private static HFSQLConnector instance;
        private OdbcConnection _connection;

        private HFSQLConnector(string connectionString)
        {
            _connection = new OdbcConnection(connectionString);
        }
        public static HFSQLConnector GetInstance(string connectionString)
        {
            if (instance == null)
            {
                instance = new HFSQLConnector(connectionString);
            }
            return instance;
        }

        public bool InsertOdometer(List<OdometerField> lstOdometer)
        {
            Logger log = Logger.getInstance();
            bool isSqlException = false;

            if (lstOdometer != null)
            {
                log.write(Logger.LogLevel.Debug, "Inserting Odometer values in HFSQL DB.");

                try
                {
                    _connection.Open();

                    int itemcount = 0;
                    foreach (OdometerField odometer in lstOdometer)
                    {
                        if (++itemcount % 1000 == 0)
                        {
                            log.write(Logger.LogLevel.Debug, String.Format("{0} items processed.", itemcount.ToString()));
                        }
                        OdbcCommand command = new OdbcCommand();
                        command.Connection = _connection;
                        command.CommandText = "INSERT INTO T_Odometre(device_id,vin,date_time,value) values(?,?,?,?)";
                        command.Parameters.Add(new OdbcParameter("@deviceid", odometer.device_id));
                        command.Parameters.Add(new OdbcParameter("@vin", odometer.vin));
                        command.Parameters.Add(new OdbcParameter("@date_time", odometer.date_time));
                        //command.Parameters.Add(new OdbcParameter("@date_time", "2017-01-01 00:00:00:00"));
                        command.Parameters.Add(new OdbcParameter("@value", odometer.value));

                        command.ExecuteNonQuery();
                    }
                }
                catch (Exception e)
                {
                    log.write(Logger.LogLevel.Critical, string.Format("HFSQL problem while inserting odometer values. Message is: {0}", e.Message));
                    isSqlException = true;
                }
                finally
                {
                    _connection.Close();
                }
            }
            else
            {
                log.write(Logger.LogLevel.Debug, "No odometer to update.");
            }
            return !isSqlException;
        }

        public bool InsertAlertExtract(AlertExtractField alertExtract)
        {
            List<AlertExtractField> lstAlertExtract = new List<AlertExtractField>();
            lstAlertExtract.Add(alertExtract);
            return InsertAlertExtract(lstAlertExtract);
        }

        public bool InsertAlertExtract(List<AlertExtractField> lstAlertExtract)
        {
            Logger log = Logger.getInstance();
            bool isSqlException = false;

            if (lstAlertExtract != null)
            {
                log.write(9, string.Join("", new string[] { "Inserting AlertExtract in HFSQL DB." }));
                try
                {
                    _connection.Open();

                    int itemcount = 0;
                    foreach (AlertExtractField alertExtract in lstAlertExtract)
                    {
                        if (++itemcount % 1000 == 0)
                        {
                            log.write(Logger.LogLevel.Debug, String.Format("{0} items processed.", itemcount.ToString()));
                        }
                        //Insert the AlertExtract in the DB
                        OdbcCommand command = new OdbcCommand();
                        command.Connection = _connection;
                        command.CommandText =
@"insert into T_Alerte(
    exceptionevent_id,
    device_id,
    vin,
    rule_id,
    rule_name,
    distance,
    start_date_time,
    end_date_time,
    start_logrecord_id,
    start_timediff,
    start_latitude,
    start_longitude,
    end_logrecord_id,
    end_timediff,
    end_latitude,
    end_longitude
    ) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                        command.Parameters.Add(new OdbcParameter("@exceptionevent_id", alertExtract.exceptionevent_id));
                        command.Parameters.Add(new OdbcParameter("@deviceid", alertExtract.device_id));
                        command.Parameters.Add(new OdbcParameter("@vin", alertExtract.vin));
                        command.Parameters.Add(new OdbcParameter("@rule_id", alertExtract.rule_id));
                        command.Parameters.Add(new OdbcParameter("@rule_name", alertExtract.rule_name));
                        command.Parameters.Add(new OdbcParameter("@distance", alertExtract.distance));
                        command.Parameters.Add(new OdbcParameter("@start_date_time", alertExtract.start_date_time));
                        command.Parameters.Add(new OdbcParameter("@end_date_time", alertExtract.end_date_time));
                        command.Parameters.Add(new OdbcParameter("@start_logrecord_id", alertExtract.start_logrecord_id));
                        command.Parameters.Add(new OdbcParameter("@start_timediff", alertExtract.start_timediff));
                        command.Parameters.Add(new OdbcParameter("@start_latitude", alertExtract.start_latitude));
                        command.Parameters.Add(new OdbcParameter("@start_longitude", alertExtract.start_longitude));
                        command.Parameters.Add(new OdbcParameter("@end_logrecord_id", alertExtract.end_logrecord_id));
                        command.Parameters.Add(new OdbcParameter("@end_timediff", alertExtract.end_timediff));
                        command.Parameters.Add(new OdbcParameter("@end_latitude", alertExtract.end_latitude));
                        command.Parameters.Add(new OdbcParameter("@end_longitude", alertExtract.end_longitude));

                        command.ExecuteNonQuery();
                    }
                    log.write(9, string.Format("Done inserting {0} AlertExtract.", lstAlertExtract.Count.ToString()));
                }
                catch (Exception e)
                {
                    log.write(Logger.LogLevel.Critical, string.Format("HFSQL problem. Message is: {0}", e.Message));
                    isSqlException = true;
                }
                finally
                {
                    _connection.Close();
                }
            }
            else
            {
                log.write(Logger.LogLevel.Warning, "No AlertExtract to insert.");
            }

            return !isSqlException;
        }

        public bool InsertDutyStatusLogs(List<DutyStatusLogField> lstDutyStatusLogs)
        {
            Logger log = Logger.getInstance();
            bool isSqlException = false;

            if (lstDutyStatusLogs != null)
            {
                log.write(Logger.LogLevel.Debug, "Inserting DutyStatusLog values in HFSQL DB.");

                try
                {
                    _connection.Open();

                    int itemcount = 0;
                    foreach (DutyStatusLogField dutyStatusLog in lstDutyStatusLogs)
                    {
                        if (++itemcount % 500 == 0)
                        {
                            log.write(Logger.LogLevel.Debug, String.Format("{0} items processed.", itemcount.ToString()));
                        }
                        //Find if the DutyStatusLog Exists
                        OdbcCommand selectCommand = new OdbcCommand();
                        selectCommand.Connection = _connection;
                        selectCommand.CommandText = "SELECT COUNT(id) FROM T_DutyStatusLog WHERE id=?";
                        selectCommand.Parameters.Add(new OdbcParameter("@id", dutyStatusLog.id));
                        string dscount = selectCommand.ExecuteScalar().ToString();

                        if (dscount.Equals("0"))
                        {
                            //Insert a new DutyStatusLog
                            OdbcCommand insertCommand = new OdbcCommand();
                            insertCommand.Connection = _connection;
                            insertCommand.CommandText = "INSERT INTO T_DutyStatusLog(id,device_id,driver_id,parentid,state,status,datetime,editdatetime,location,origin,verifydatetime,version) values(?,?,?,?,?,?,?,?,?,?,?,?)";
                            insertCommand.Parameters.Add(new OdbcParameter("@id", dutyStatusLog.id));
                            insertCommand.Parameters.Add(new OdbcParameter("@device_id", dutyStatusLog.device_id));
                            insertCommand.Parameters.Add(new OdbcParameter("@driver_id", dutyStatusLog.driver_id));
                            insertCommand.Parameters.Add(new OdbcParameter("@parentid", dutyStatusLog.parentid));
                            insertCommand.Parameters.Add(new OdbcParameter("@state", dutyStatusLog.state));
                            insertCommand.Parameters.Add(new OdbcParameter("@status", dutyStatusLog.status));
                            insertCommand.Parameters.Add(new OdbcParameter("@datetime", dutyStatusLog.datetime));
                            insertCommand.Parameters.Add(new OdbcParameter("@editdatetime", dutyStatusLog.editdatetime));
                            insertCommand.Parameters.Add(new OdbcParameter("@location", dutyStatusLog.location));
                            insertCommand.Parameters.Add(new OdbcParameter("@origin", dutyStatusLog.origin));
                            insertCommand.Parameters.Add(new OdbcParameter("@verifydatetime", dutyStatusLog.verifydatetime));
                            insertCommand.Parameters.Add(new OdbcParameter("@version", dutyStatusLog.version));
                            insertCommand.ExecuteNonQuery();
                        }
                        else
                        {
                            //Update existing DutyStatusLog
                            OdbcCommand updateCommand = new OdbcCommand();
                            updateCommand.Connection = _connection;
                            updateCommand.CommandText = "UPDATE T_DutyStatusLog SET device_id=?,driver_id=?,parentid=?,state=?,status=?,datetime=?,editdatetime=?,location=?,origin=?,verifydatetime=?,version=? WHERE id=?";
                            updateCommand.Parameters.Add(new OdbcParameter("@device_id", dutyStatusLog.device_id));
                            updateCommand.Parameters.Add(new OdbcParameter("@driver_id", dutyStatusLog.driver_id));
                            updateCommand.Parameters.Add(new OdbcParameter("@parentid", dutyStatusLog.parentid));
                            updateCommand.Parameters.Add(new OdbcParameter("@state", dutyStatusLog.state));
                            updateCommand.Parameters.Add(new OdbcParameter("@status", dutyStatusLog.status));
                            updateCommand.Parameters.Add(new OdbcParameter("@datetime", dutyStatusLog.datetime));
                            updateCommand.Parameters.Add(new OdbcParameter("@editdatetime", dutyStatusLog.editdatetime));
                            updateCommand.Parameters.Add(new OdbcParameter("@location", dutyStatusLog.location));
                            updateCommand.Parameters.Add(new OdbcParameter("@origin", dutyStatusLog.origin));
                            updateCommand.Parameters.Add(new OdbcParameter("@verifydatetime", dutyStatusLog.verifydatetime));
                            updateCommand.Parameters.Add(new OdbcParameter("@version", dutyStatusLog.version));
                            updateCommand.Parameters.Add(new OdbcParameter("@id", dutyStatusLog.id));
                            updateCommand.ExecuteNonQuery();
                        }

                    }
                }
                catch (Exception e)
                {
                    log.write(Logger.LogLevel.Critical, string.Format("HFSQL problem while inserting DutyStatusLog values. Message is: {0}", e.Message));
                    isSqlException = true;
                }
                finally
                {
                    _connection.Close();
                }
            }
            else
            {
                log.write(Logger.LogLevel.Debug, "No DutyStatusLog to update.");
            }
            return !isSqlException;
        }

        public bool InsertTrips(List<TripField> lstTrips)
        {
            Logger log = Logger.getInstance();
            bool isSqlException = false;

            if (lstTrips != null)
            {
                log.write(Logger.LogLevel.Debug, "Inserting Trip values in HFSQL DB.");

                try
                {
                    _connection.Open();

                    int itemcount = 0;
                    foreach (TripField trip in lstTrips)
                    {
                        if (++itemcount % 500 == 0)
                        {
                            log.write(Logger.LogLevel.Debug, String.Format("{0} items processed.", itemcount.ToString()));
                        }
                        OdbcCommand command = new OdbcCommand();
                        command.Connection = _connection;
                        command.CommandText = "INSERT INTO T_Trip(id,device_id,driver_id,datetime,afterhoursdistance,afterhoursdrivingduration,afterhoursend,afterhoursstart,afterhoursstopduration,workdistance,workdrivingduration,workstopduration) values(?,?,?,?,?,?,?,?,?,?,?,?)";
                        command.Parameters.Add(new OdbcParameter("@id", trip.id));
                        command.Parameters.Add(new OdbcParameter("@device_id", trip.device_id));
                        command.Parameters.Add(new OdbcParameter("@driver_id", trip.driver_id));
                        command.Parameters.Add(new OdbcParameter("@datetime", trip.datetime));
                        command.Parameters.Add(new OdbcParameter("@afterhoursdistance", trip.afterhoursdistance));
                        command.Parameters.Add(new OdbcParameter("@afterhoursdrivingduration", trip.afterhoursdrivingduration));
                        command.Parameters.Add(new OdbcParameter("@afterhoursend", trip.afterhoursend));
                        command.Parameters.Add(new OdbcParameter("@afterhoursstart", trip.afterhoursstart));
                        command.Parameters.Add(new OdbcParameter("@afterhoursstopduration", trip.afterhoursstopduration));
                        command.Parameters.Add(new OdbcParameter("@workdistance", trip.workdistance));
                        command.Parameters.Add(new OdbcParameter("@workdrivingduration", trip.workdrivingduration));
                        command.Parameters.Add(new OdbcParameter("@workstopduration", trip.workstopduration));

                        command.ExecuteNonQuery();
                    }
                }
                catch (Exception e)
                {
                    log.write(Logger.LogLevel.Critical, string.Format("HFSQL problem while inserting odometer values. Message is: {0}", e.Message));
                    isSqlException = true;
                }
                finally
                {
                    _connection.Close();
                }
            }
            else
            {
                log.write(Logger.LogLevel.Debug, "No odometer to update.");
            }
            return !isSqlException;
        }
    }
}
