﻿using System;

namespace GeotabExtract.Connector.DB
{
    public class WrongDatabaseException : Exception
    {
        public WrongDatabaseException()
        {
        }
        public WrongDatabaseException(string message)
        : base(message)
        {
        }
    }
}