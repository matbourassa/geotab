﻿using GeotabExtract.Worker;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace GeotabExtract
{
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            //Running Debug
            if (Environment.UserInteractive)
            {
                GeotabExtractWorker serviceDebug = new GeotabExtractWorker();

                serviceDebug.TestStartupAndStop(new string[2]);
            }
            //Running Release
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                    new GeotabExtractWorker()
                };
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
